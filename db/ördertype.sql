create database if not exists `umsesb`;

USE `umsesb`;


DROP TABLE IF EXISTS `ordertype`;
CREATE TABLE ordertype ( 
	`code` varchar(255)  NOT NULL,
	`description`     varchar(255),
    `notes`     varchar(255),
    CONSTRAINT pk_code PRIMARY KEY ( code )
 );
 INSERT INTO ordertype(
code,
description,
notes)
VALUES
(
'ZPM1', 'RESPONSE', 'Issues arising (usually notified by the public)'
);
 INSERT INTO ordertype(
code,
description,
notes)
VALUES
(
'ZPM2', 'COMPLIANCE (ZPM2)', ''
);
 INSERT INTO ordertype(
code,
description,
notes)
VALUES
(
'ZPM3', 'PLANNED', 'Planned jobs, including both regular and one-off.'
);
 INSERT INTO ordertype(
code,
description,
notes)
VALUES
(
'ZPM4', 'CAPEX', 'Minor Capital Work Orders'
);