create database if not exists `umsesb`;

USE `umsesb`;

CREATE TABLE `message` (
  `id` CHAR(40) NOT NULL, 
  `esbid`CHAR(40) DEFAULT NULL,
  `sourcesys` varchar(45) DEFAULT NULL,
  `messagetype` varchar(45) DEFAULT NULL,
  `actiontype` varchar(45) DEFAULT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payload` LONGTEXT DEFAULT NULL,
  `sapid` varchar(45) DEFAULT NULL,
   `operationid` varchar(255) DEFAULT NULL,
   `valid` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`id` )
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DELIMITER #
CREATE TRIGGER insert_guid 
BEFORE INSERT ON message
FOR EACH  ROW 
BEGIN 
    SET NEW.id = UUID(); 
END;
#
DELIMITER ;


CREATE TABLE messageresponse ( 
   	esbid   CHAR(40) DEFAULT NULL ,
	datecreated          timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP ,
	targetsys           varchar(255)    ,
	messagetype          varchar(255)    ,
	actiontype          varchar(255)    ,
	result          LONGTEXT DEFAULT NULL,
	status         varchar(255)    ,
	techoneId          varchar(255)  DEFAULT NULL,
	INDEX messageresponseid_ind (esbid))ENGINE=InnoDB  DEFAULT CHARSET=utf8;