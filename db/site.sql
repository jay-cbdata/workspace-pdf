create database if not exists `umsesb`;

USE `umsesb`;

CREATE TABLE `site` (
   `site` integer  NOT NULL,
	`localboarddescription`     varchar(255),
    `sitedescription`     varchar(255),
    CONSTRAINT pk_site PRIMARY KEY ( site )
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10465, 'Te Arai Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10466, 'Atiu Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10472, 'Wenderholm Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10474, 'Mahurangi Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10475, 'Pakiri Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10476, 'Scandrett Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10478, 'Te Muri Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10480, 'Tawharanui Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10484, 'Muriwai Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 10485, 'Waitakere West Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 10488, 'Waitakere South Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 10489, 'Waitakere North Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 10490, 'Waitakere Central Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10491, 'Te Rau Puriri Regional Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 10494, 'Muriwai Golf Club'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 10498, 'Western Sector'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 11201, 'Huapai Recreation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 11203, 'Lake Tomarata Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 11204, 'Leigh Domain');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 11205, 'Riverhead War Memorial Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 11206, 'Mill Lane, 15');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 11207, 'Kumeu Utility Reserve & Public Toilets'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney',
11208,
'Puhoi Pioneer'' s Memorial Park Domain ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11210,' Shoesmith Domain Recreation Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11211,' Wellsford Centennial Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11212,' Te Arai Forestry South ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11213,' Rautawhiri Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11215,' Helensville War Memorial Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11216,' Mahurangi East Community Centre Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11218,' Wellsford Community Centre Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11219,' Ahuroa Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11220,' Coatesville Settlers Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11221,' Glasgow Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11222,' Bourne Dean Recreation Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11223,' Kaukapakapa Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11224,' Kourawhero Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11225,' Leigh Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11226,' Point Wells Community Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11227,' Riverhead War Memorial Pavilion Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11228,' Leigh Road,
307 ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11229,' Sinclair Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11232,' Algies Bay Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11233,' Parry Kauri Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11234,' Kumeu Community Centre ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11235,' Port Albert Recreation Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11236,' Pakiri Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11237,' Broadlands Drive,
57 ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11240,' Ahuroa Fire Station Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11241,' Kaukapakapa Fire Station Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11242,' Muriwai Fire Station Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11243,' Shelly Beach Fire Station Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11245,' Muriwai Surf Club Site ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11246,' Shelly Beach Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11247,' Warkworth Service Centre & Library Grou.');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11248,' 45 Oraha Road ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11250,' 290 Mahurangi East Road ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11256,' Church Hill,
8 ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11257,' 71 Muriwai Road ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11259,' Huapai Service Centre
And Kumeu Library ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11261,' Tapora Recreation Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11262,' Naumai Domain Recreation Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11266,' Waimauku Memorial Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11267,' Wainui Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11268,' Warkworth Masonic Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11269,' Warkworth Town Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11270,' Whangaripo Valley Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11271,' Whangateau Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11272,' Martins Bay Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11274,' Whangateau Holiday Park Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11276,' Leigh Library
and Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11278,' 118 Rodney Street,
Wellsford ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11284,' 35 Mill Road Helensville ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11285,' Sandspit Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11289,' Hoteo Recreational Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11290,' Wellsford War Memorial Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11291,' Goodall Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11292,' Coatesville Recreation Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11294,' Helensville River Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11297,' Tauhoa Road Land ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11298,' Harry James Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11299,' Birds Beach Recreation Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11301,' Omaha Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11302,' Manuhiri Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11304,' South Head Hall Grounds ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11306,' 80 Great North Road Warkworth ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11323,' Parakai Recreation Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11324,' 27 Commerical Road Helensville ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Rodney ',11326,' Green Road,
Dairy Flat ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11330,' Township Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11335,' Parrs Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11337,' Piha Domain ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11339,' Prospect Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11340,' Ramlea Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11341,' Ranui Domain ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11342,' Riverpark Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11343,' Roby Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11344,' Royal Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11345,' Sandys Parade ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11348,' Woodglen Road Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11349,' Westview Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11350,' Swanson Station Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11351,' Waitakere Quarry ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11353,' Hoani Waititi Marae ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11355,' Seymour Road 26 ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11359,' 23 Henderson Valley Rd ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11361,' Woodlands Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11363,' 2 - 4 Waipareira Ave ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11364,' Cranwell Esplanade ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11365,' Falls Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11367,' Fairdene Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11368,' Durham Green ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11369,' Titirangi Beach ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11370,' Waiatarua Reserve - Waiatarua ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11371,' French Bay Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11372,' Waitakere War Memorial Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11373,' Trig Reserve ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11374,' Triangle Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11375,' Te Rangi Hiroa / Birdwood Winery ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Henderson - Massey ',11376,' Te Pai Park ');
/* INSERT QUERY */INSERT INTO site(localboarddescription,Site
,sitedescription
) VALUES( ' Waitakere Ranges ',11377,' Owen''s Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11378, 'Te Henga Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11379, 'Titirangi War Memorial'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11380, 'Fred Taylor Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11381, 'Glen Norman Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11382, 'Gloria Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11383, 'Kingdale Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11384, 'Kaumatua Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11385, 'Kaurilands Domain'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11386, 'Karamatura Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11387, 'Jack Colvin Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11388, 'Jack Pringle Sports Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11389, 'Kaikoura Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11391, 'Henderson Valley Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11393, 'Henderson Park - Henderson'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11394, 'Corbans Estate'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11395, 'Corban Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11396, 'Grassmere Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11399, 'Lincoln Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11401, 'Taipari Strand'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11403, '8 Ratanui Street-Third Party'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11406, '51 Keeling Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11408, 'Kowhai Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11410, 'Central One'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11419, 'Levy Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11420, 'Starling Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11422, 'Singer Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11423, 'Divich Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11424, 'Swanson Stream Esplanade Reser'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11425, 'Cranwell Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11426, 'Coletta Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11427, 'Chapman Strand'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11428, 'Trusts Arena'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11429, 'Corban Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11431, 'Bruce Mclaren Memorial Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11432, 'Awaroa Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11433, 'Covil Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11434, 'Les Waygood Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11435, 'Te Atatu Peninsula Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11436, 'Lloyd Morgan Lions Club Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11437, 'Harbourview - Orangihina'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11438, 'Marinich Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11444, 'Jack Pringle Village Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11445, 'Laingholm Hall Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11446, 'Ranui Centre'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11447, '10 Western Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11449, 'Rahui Kahika Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11450, 'Lopdell Hall And House'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11453, '35 Arapito Road (Shabolt House)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11454, 'Massey Leisure Centre & Library'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11456, '399 Don Buck Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11458, 'Solid Waste Transfer Stn'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11459, 'Waitakere Lib Cpark Bldg & Cab'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11460, 'Tui Glen Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11461, 'Civil Defence Western Region Headquarter'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11462, 'Wilsher Village'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11464, 'Waitakere Civic Centre'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11466, 'Animal Welfare Centre'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11467, 'Glen Eden Library'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11468, '39 - 41 Glenmall Place'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11469, 'Harold Moody Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11471, 'Ceramco Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11475, '98 Birdwood Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11476, 'Makora Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11477, 'Sturges West Community House'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11478, 'Fire Station Bethells'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11479, 'Neville Power Memorial Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11480, 'Massey Domain'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11481, 'Oratia Hall Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 11483, 'Glen Eden Railway Stn'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11485, 'Opanuku Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11486, 'Moire Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11488, 'McLeod Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 11489, 'Matipo Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription
)
VALUES
(
'Rodney', 13002, 'Matakana Rural Volunteer Fire Station'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 20029, '46 Armitage Road Wellsford'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20070, '6 Ratanui Street'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 20087, '184 Hungry Creek Road - Puhoi'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20089, 'Wicks'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20110, 'Streetscape - Henderson-Massey
Rodney');

INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney',20119, 'Streetscape - Rodney');


INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Waitakere Ranges',
20121, 'Streetscape - Waitakere Ranges'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20495, 'Kellys Bridge Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20499, 'Hibernia Stream Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20502, 'Lockington Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20509, 'Kelvin Strand'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20513, 'Kemp Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20517, 'Kervil Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20521, 'Kitewaho Plant Reserve - 2'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20524, 'Kitewaho Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20532, 'Robert Knox Memorial Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20537, 'Konini Pt Reserve - 1'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20540, 'Konini Pt Reserve - 2'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20544, 'Kotinga Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20552, '45 Kauri Point Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20555, 'Kaurimu Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20560, 'Kitewaho Plant Reserve - 1'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20564, 'Laingholm Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20572, 'Keegan Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20576, 'Kayle Glen Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20580, 'Kawaka Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20584, 'Kawa Glade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20589, 'Henderson Valley Scenic'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20593, 'Holmes Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20597, 'Hepburn Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20601, 'Hughes Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20605, 'Huia Domain'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20609, 'Huruhuru Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20613, 'Huia Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20617, 'Inaka Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20621, 'Jaemont Strand'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20625, 'John Webster Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20636, 'Martin Jugum Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20640, 'Kauri Plant Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20643, 'Karekare Plant Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20647, 'Karekare Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20651, 'Karekare Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20655, 'Karekare Valley Scenic'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20659, 'Kamara Road Common'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20671, 'Kauri Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20675, 'Katrina Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20679, 'Kamara Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20683, 'Henderson Valley Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20687, 'Laings Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20691, 'Landow Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20698, 'Maywood Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20702, 'Mccormick Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20706, 'Mcclintock Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20710, 'Mceldowney Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20716, 'Mckinley Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20720, 'Mcleod Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20724, 'Meadow Glade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20740, 'Millbrook Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20744, 'Millbrook Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20748, 'Midgley Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20752, 'Miha Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20755, 'Milan Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20759, 'Minnehaha Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20775, 'Mona Vale Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20785, 'Mahoe Plantation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20789, 'Marsh Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20794, 'Marian Roberts Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20798, 'Lopdell Plantation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20802, 'Laingholm Drive Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20806, 'Lake Wainamu Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20810, 'Little Huia Beach'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20814, 'Lilburn Crescent Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20818, 'Lincoln Road Plant Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20822, 'Lookout Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20826, 'Little Muddy Creek'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20832, 'Lone Kauri Glade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20835, 'Lone Kauri Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20841, 'Kensington Gardens'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20844, 'Landing Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20848, 'Lowtherhurst Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20857, 'Lydford Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20866, 'Maher Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20870, 'Mahoe Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20876, 'Manutewhau Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20883, 'Marlene Glade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20886, 'Lone Tree Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20890, 'Mountain Road Esplanade Reserv'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20892, 'Hindmarsh Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20896, 'Hilda Griffin Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20900, 'Wickstead Strand'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20908, 'Wirihana Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20923, 'Withers Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20928, 'Waimoko Glen Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20932, 'Woodford Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20936, 'Railside Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20940, 'Bill Haresnape Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20945, 'Shah Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20949, 'Olive Grove'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20952, 'Ranui Station Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20957, 'Piha Road Edge Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20963, 'Soldiers Memorial Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20967, 'Edgelea Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 20970, 'Catherine Mall'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20977, 'Driving Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 20985, 'Duck Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21003, 'Valron Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21009, 'Vale Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21013, 'Victory Glade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21017, 'Vintage Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21021, 'Virgo Common'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21025, 'Vitasovich Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21028, 'Vodanovich Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21036, 'Waikomiti Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21040, 'Waimanu Bay Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21044, 'Christine Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21048, 'Waima Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21055, 'Warner Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21059, 'Warner Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21063, 'Wood Bay Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21067, 'Wood Bay Way'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21071, 'Waima Crescent Boylan Terrace'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21076, 'Wekatahi Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21079, 'Welsh Hills Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21089, 'Western Park - Laingholm'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21093, 'Wakeling Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21104, 'Davies Bay Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21108, 'Glucina Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21112, 'Glen Close Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21116, 'Glendene Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21120, 'Greenberry Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21124, 'Glen Eden Picnic Ground'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21128, 'Laurieston Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21144, 'Halyard Common'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21148, 'Hamblyn Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21151, 'Handley Plantation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21159, 'Harvest Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21163, 'Library Lane/Glen Eden Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21167, 'Hart Domain'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21179, 'Helena Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21187, 'Herrings Cove'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21191, 'Gill Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21195, 'Glenesk Road Plant Reserve - 2'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21199, 'Glenesk Road Plant Reserve - 1'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21203, 'George Herring Common'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21212, 'Lavelle Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21216, 'Elvira Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21220, 'Emerald Valley Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21226, 'Epping Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21230, 'Epping Plant Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21234, 'Fawcett Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21237, 'Fawcett Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21245, 'Featherstone Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21248, 'Danica Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21252, 'Flanshaw Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21255, 'Foster Ave Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21261, 'Foster Bay Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21265, 'Foster Hill Lane'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21269, 'Forest Hill Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21272, 'Foster Strand'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21277, 'French Bay Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21281, 'Gallony Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21285, 'Garden Road Plant Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21291, 'Garden Road Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21295, 'Flaunty Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21298, 'Mountain Reserve - Henderson Valley'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21302, 'Marama Plantation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21305, 'Marine Parade Plantation Reser'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21309, 'Paremuka Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21313, 'Burtons Drive Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21321, 'Xena Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21329, 'Pleasant Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21336, 'Westgate Drive Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21346, 'Lincoln Park Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21350, 'Landing Road Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21354, 'Waituna Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21364, 'Kaurimu Stream Reserve - Central'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21376, 'Tawini Road Bush Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21380, 'Opanuku Road Bush Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21385, 'Sw Storage Basin Res - 119 Glendale Rd'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21391, 'Sw Storage Basin Res - 12 Kora Ave'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21394, 'Sw Storage Basin Res - 10 Welcome Pl'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21399, 'Momotu Stream Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21411, 'Sturges Park & Ride'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21415, 'Kay Road Bale Fill'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21423, 'Laingholm Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21427, 'Lake Panorama'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21431, 'Mili Way South Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21435, 'Kohu Plantation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21439, 'Kaurimu Stream Reserve - South'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21443, 'Cochran Stream Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21451, 'Tracey Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21455, 'Sw Storage Basin Res - 97B Pooks Rd'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21463, 'Cochran Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21467, 'Harbourview Corner'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21471, 'Cornwallis Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21475, 'Paremuka Wetland'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21478, 'Westglen Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21482, 'Riverglade Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21486, 'Sunnyvale Park & Ride'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21494, 'Cartmel Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21499, 'Scenic Drive Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21503, 'Oratia Drive Accessway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21507, 'Pioneer Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21511, 'Sw Storage Basin Res - Lucienne Dr'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21514, 'Sw Storage Basin Res - Tony Street'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21616, 'Sw Storage Basin Res - 31 Blueridge Cl'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21653, 'Sw Storage Basin Res - 37 View Ridge Dr'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21704, 'Ferngrove Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21708, 'South Piha Plantation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21712, 'Espalier Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21716, 'Pahi Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21724, 'Pareira Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21728, 'Pareira Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21732, 'Paturoa Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21735, 'Paturoa Way'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21739, 'Penfold Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21743, 'Pendrell Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21747, 'Piha South Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21751, 'Piha Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21754, 'Plumer Domain'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21766, 'Pooks Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21781, 'Raelene Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21792, 'Gus Nola Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21800, 'Palomino Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21804, 'Otitori Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21809, 'Otitori Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21813, 'Osman Street Common'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21817, 'Murillo Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21821, 'Neweys Corner'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21826, 'Nicolas Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21837, 'North Piha Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21841, 'Manutewhau Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21845, 'Melia Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21849, 'North Piha Strand'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21855, 'Raroa Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21860, 'Napuka Road Plantation'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21864, 'Odlin Corner'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21869, 'Okewa Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21873, 'Okewa Reserve & Beach'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21877, 'Onedin Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21881, 'Provence Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21885, 'Opanuku Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21890, 'Opanuku Marginal Strip Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21896, 'Opanuku Stream Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21900, 'Opou Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21905, 'Oratia Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21918, 'Realm Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21926, 'Seibel Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21930, 'Woodfern Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21935, 'Woodside Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21943, 'Kaurimu Stream Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21947, '537 West Coast Road Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21951, 'Woodside Glen'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21956, 'Waitemata Corner'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21962, 'Blueridge Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21966, 'Seibel Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21970, 'York Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21975, 'Paremuka Lakeside'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21983, 'Woodbank Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21987, 'Corban Estate Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 21991, 'Howard Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 21998, 'Swanson Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22002, 'Sun Place Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22006, 'Mt Atkinson Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22010, 'Foothills Lane Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22014, 'Zita Maria Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22018, 'Urlich Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22022, 'Seaview Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22040, 'Rena Place Rec Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22045, 'Renata Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22048, 'Reynella Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22052, 'Rhinevale Close'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22056, 'Rimu Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22060, 'Roberts Field'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22064, 'Roberts Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22068, 'Royal Heights Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22072, 'Seaview Road Plant Reserve - 1'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22076, 'Rotary Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22088, 'Ruru Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22092, 'Rush Creek Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22106, 'Sarajevo Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22114, 'Scenic Drive North Plant'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22118, 'Seaview Road Plant Reserve - 2'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22121, 'Rayner Road/Sylvan Glade Res'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22127, 'Upland Glade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22131, 'San Bernadino Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22134, 'William Fraser Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22137, 'Dune Walkway');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22140, 'Kewai Street Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22143, 'Kokopu Street Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22146, 'Omaha South Quarry Track'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22148, 'Pukemateko Reserve Omaha South'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22151, 'Rahui Te Kiri Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22154, 'Tuna Place Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22157, 'Opahi Bay Beach Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22160, 'McElroy Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22162, 'Point Wells Community Centre'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22165, 'Point Wells Foreshore Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22172, 'Waimanu Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22175, 'Port Albert Wharf Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22178, 'Claytons Landing'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22179, 'Puhoi Close');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22182, 'Puhoi Esplanade Land'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22186, 'Beach Street Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22188, 'Green Point Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22191, 'Kanuka Reserve (Sandspit)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22193, 'Whangateau Harbour Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22201, 'Rita Way_Excelsior Way_Lagoon Way'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22204, 'Omaha Beach Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22207, 'Harbour View Road Coastal Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22210, 'Leigh Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22213, 'Leigh Harbour Cove Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22216, 'Cumberland Street Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22219, 'Leigh Village Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22222, 'Leigh Wharf Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22225, 'Omaha Blk Access Road Espl Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22228, 'Wonderview Rd/Cotterell St Espl'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22231, 'Albert Dennis Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22234, 'Matakana Diamond Jubilee Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22237, 'Sandspit Reserve - Rodney');
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney',
22240, 'Matakana Wharf Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22242, 'Riverglade Lane A/Way & Espl Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22245, 'Rosemount Rd Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22248, 'Sharpe Road Matakana Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22252, 'Kendale Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22255, 'Matheson Bay Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22258, 'Spray Crescent Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22264, 'Excelsior Way Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22267, 'Ida Way - Rita Way Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22270, 'Omaha Beach Boat Launching & Wharf'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22273, 'Rainbows End Reserve - Rodney');
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney',
22276, 'South Cove Wharf'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22279, 'Sandspit Road - Brick Bay Drive'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22287, 'Kowhai Park');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22290, 'Langridge Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22293, 'Lucy Moore Memorial Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22296, 'Valerie Close Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22299, 'View Road Bush Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22302, 'Warkworth River Bank-Town W/Way'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22308, 'Warkworth Showgrounds'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22313, 'Warkworth Town Hall Grounds'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22316, 'Whitaker Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22322, 'Currys Bush Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22328, 'Watson Place Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22330, 'Wellsford Depot Grounds'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22332, 'Wellsford South Entrance Reserve Layby'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22334, 'Wellsford Town Toilets & Library'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22336, 'Whangaripo Valley Hall'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22338, 'Worthington Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22339, 'Whangaripo Valley Rd Espl Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22341, 'Ashton Road - Leigh Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22343, 'Hauiti Street Council Sections'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22345, 'Glenmore Drive Industry Screening'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22347, 'Falls Rd River Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22349, 'Elizabeth Street Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22351, 'Amanda Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22354, 'Ariki Reserve');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22356, 'Fidelis Avenue Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22358, 'Piccadilly Circus Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22359, 'Snells Beach (Sunrise Boulevard)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22360, 'Sunburst Reserve & Tamatea Espl'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22361, 'Sunrise Boulevard & Dalton Rd'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22362, 'Woodlands Avenue Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22364, 'Mangakura Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22365, 'Silver Hill Rd Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22367, 'Te Hana Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22368, 'Waimanu Rd Te Hana'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22369, 'Ti Point Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22370, 'Ti Point Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22371, 'Tapu Bush Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22372, 'Opango Creek Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22373, 'Cement Works');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22374, 'Church Hill Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22375, 'Slipper Lake Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22376, 'Big Omaha Wharf Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22378, 'Kaipara Flats Rd Recreation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22391, 'Audrey Luckens - Rautawhiri Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22392, 'Bridge Street - Awaroa Stream Espl'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22393, 'Cabeleigh Drive Pond Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22395, 'Cabeleigh Reserves'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22396, 'Creek Lane');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22397, 'Green Lane Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22398, 'Helensville Civic Centre Grounds'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22399, 'Helensville River Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22414, 'Kaipara Crescent Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22427, 'Morrison Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22428, 'Makiri Rd - Mcleod St Closed Landfill'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22429, 'Rata Reserve');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22430, 'Omana Ave Stormwater Pond'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22432, 'Mosquito Bay');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22433, 'Taupaki Esplanade Reserve No'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22435, 'Old North Road Waikoukou Val Esp'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22436, 'Buttercup Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22437, 'Freshfield Road-Rosella Grove Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22438, 'Rosella Grove Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22439, 'Sarah Todd Rsve & W/Way To Buttercup'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22440, 'Waimauku War Memorial Hall'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22441, 'Thomas Grace Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22442, 'Highfield Garden & The Glade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22443, 'Mariner Grv, Algies Bay Espl Rsve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22444, 'Willjames Ave Espl & Recreation'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22445, 'Baddeleys Beach Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22446, 'Brick Bay Drive - Puriri Place Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22447, 'Buckleton Beach Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22448, 'Campbells Beach Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22449, 'Pigeon Place A/Way, Campbells Beach'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22450, 'Jamieson Bay Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22451, 'Omana Ave Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22452, 'Riverhead Triangle Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22454, 'Riverhead Historic Mill Espl Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22455, 'West Park');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22456, 'Huapai Railway Land Adj To Huapai Rsve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22457, 'Merlot Heights'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22458, 'Pinotage Espl Rsve, Adj Council'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22460, 'Station Rd Corner Sh16'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22461, 'Sunny Crescent - Merlot Heights Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22462, 'Taylor Rd Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22463, 'Kaukapakapa Hall & Library Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22464, 'Kaukapakapa Plantation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22465, 'Mill Rd Esplanade Reserve Helensville'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22466, 'Kaukapakapa Sh16 #887 Esplanade Res'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22468, 'Makarau Bridge Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22469, 'Omeru Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22470, 'Hamilton Rd Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22471, 'Muriwai Beach Playground (DOC)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22473, 'Springs Road Wharf & Boat Ramp'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22474, 'Te Moau Rsv & River Espl, Parakai Av'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22475, 'Edward Jonkers Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22476, 'Murray Jones Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22477, 'Huapai Riverbank,Service Centre,'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22478, 'Lews Bay');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22479, 'Whangateau Tip Landfill'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22480, 'Stables Landing Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22481, 'Crosby Reserve - West Harbour'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22483, 'Carter Road Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22486, 'Cyclarama Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22487, 'Dalmatia Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22488, 'Daffodil Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22490, 'Dawnhaven Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22491, 'Daytona Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22492, 'Daytona Strand'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22495, 'Don Buck Corner'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22496, 'Don Buck Prim Rec Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22497, 'Douglas Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22498, 'Sherrybrooke Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22499, 'Shona Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22500, 'Sherwood Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22501, 'Shays Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22502, 'Cron Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22503, 'Henderson Creek Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22504, 'Crows Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22505, 'Colwill Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22506, 'Bush Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22507, 'Buckingham Common'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22508, 'Buisson Glade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22509, 'Cascade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22510, 'Catherine Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22511, 'Chilcott Brae'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22513, 'Chorley Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22514, 'Clarence Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22515, 'Claude Able Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22516, 'Spargo Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22517, 'Cellarmans Corner'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22519, 'Claverdon Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22520, 'Clayburn Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22523, 'The Concourse Strand'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22526, 'Coroglen Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22527, 'Corran Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22528, 'Claridge Street Common'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22530, 'Springbank Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22531, 'Starforth Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22532, 'Tane Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22533, 'Taumatarea Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22534, 'Tasman View Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22535, 'Tawari Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22536, 'Tawa Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22537, 'Tatyana Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22539, 'Titirangi Bush Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22540, 'Tirimoana Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22541, 'Tinopai Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22542, 'Tiroroa Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22543, 'Titirangi Way Pl Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22544, 'Tanekaha Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22549, 'Trading Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22551, 'Tui Glen Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22552, 'Swanson Oaks'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22553, 'Te Atatu South Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22554, 'Takaranga Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22556, 'Tane Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22557, 'South Titirangi Rec Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22558, 'South Titirangi Pl Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22559, 'South Titirangi Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22560, 'Semillon Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22561, 'Paremuka Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22563, 'St Margarets Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22564, 'Sunline Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22565, 'Sunline Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22566, 'Sunhill Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22567, 'Spinnaker Strand'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22568, 'Sunvue Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22569, 'Swanson Stream Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22570, 'Swanson Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22571, 'Serwayne Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22572, 'Sylvan Glade Plantation Reserv'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22573, 'Sylvan Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22574, 'Taitapu Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22575, 'Tainui Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22576, 'Takahe Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22577, 'Tamariki Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22578, 'Tangiwai Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22579, 'Swan Arch Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22580, 'Bush Road Plantation'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22583, 'Whisper Cove');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22584, 'Omaha Estuary Causeway Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22605, 'Dawson Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22609, 'Manuka Grove Stormwater Pond'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22610, 'Dormer Rd Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22611, 'Waikoukou Valley Esplanade Reserves'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22614, 'Woodcocks Kawaka Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22616, 'Vera Reserve Baddeleys Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22617, 'Muriwai Village Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22622, 'Solan Drive Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22628, 'Golf Course Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 22629, 'Sh16 Opp 965');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22634, 'Astelia Grandis Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22637, 'Barrys Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22639, 'Bridge Avenue Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22640, 'Bendalls Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22642, 'Bethells Road Esplanade Reserv'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22644, 'Awhiorangi Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22645, 'Birdwood Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22648, 'Big Muddy Creek Landing'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22651, 'Bosun Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22652, 'Bosun Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22653, 'Border Road Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22655, 'Brandon Walk'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22656, 'Bishop Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22657, 'Tuscany Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22659, 'Arapito Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22660, 'Kings Farm (Wainui)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22662, 'Upper Waiwera Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22663, 'Lawrie Road Landfill'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 22664, 'Kourawhero Hall'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22669, 'Armour Bay Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22670, 'Atarua Way'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22672, 'Alan Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22673, 'Alex Jenkins Memorial'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22675, 'Annison Green'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22676, 'Arama Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 22678, 'Armada Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22680, 'Arapito Foreshore Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22681, 'Arapito Plantation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 22682, 'Atkinson / Norman Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23022, 'Kopupaka Stream Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23023, 'Tihema Stream Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23027, '493 State Highway 16 - Kumeu'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23030, '1440 Sandspit Road - Sandspit'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23044, 'Mahurangi West Hall'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23049, 'Riverleigh Drive Pond Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23064, 'Massey Matters Office'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23143, 'PT Mblk ML 1002, Mangakura Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23163, '19 Weza Lane, Kumeu'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23171, 'Glendene Res House 82 Hepburn Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23223, 'Glorit Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23224, 'Helensville Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23225, 'Hoteo North Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23236, 'Clearview Heights, Ranui'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23239, 'Wellsford Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23242, '29 Falls Road, Te Henga'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23244, '40 Glendale Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23250, 'Kaipara Flats Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23252, 'Kaukapakapa Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23255, 'Puhoi Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23256, 'Warkworth Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23257, 'Te Kapa Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23259, 'Wainui Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23264, '431 Swanson Road, Ranui'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23265, '5 Trading Place, Henderson'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23270, '393 Main Road, Waiuku'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23285, 'Tauhoa Landing Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23290, '391 Main Road, Huapai'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23291, 'Pt Allot 282 SO 904, Green Rd Dairy Flat'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23296, 'Exhibition Drive, Titirangi'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23312, 'Worker Road Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23328, '41 Wilsher Crescent'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23339, 'Scotts Landing Wharf'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23345, 'PT LOT 1DP 114083, 265 West Coast Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23357, 'Kumeu Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23358, 'Tapora Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23360, '4/222 Edmonton Road, Te Atatu South'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23367, 'Babich Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23373, '449 Huia Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23403, 'Glen Eden Depot'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23486, '1580 State Highway 1, Wellsford'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23489, '342 West Coast Road, Glen Eden'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23490, '62 Edmonton Road, Henderson'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23494, 'Parks - Waitakere Ranges'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23499, 'Parkhurst Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23510, 'Sec 3 SO 70011, Run Road, Wharehine'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23515, 'Alexander Recreation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23516, 'Whangateau Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23521, '1/2 Woodglen Rd, Glen Eden'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23523, '2/22 Woodglen Rd, Glen Eden'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23524, '18 Weza Lane, Kumeu'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23528, 'Helensville A & P'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23540, '113 Sturges Road Accessway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23605, 'Kelly Thompson Memorial Recreational Res'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23607, 'Alnwick Street Stormwater Pond'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23608, 'John Andrew Drive Stormwater Pond'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23617, 'Omaha South Quarry Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23622, 'West Harbour Esplanade - West'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23668, '4 Township Road - Rural Fire Station'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23669, 'Karekare Rural Fire Station'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23675, 'Waiwhauwhau-Tram Valley Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23698, 'Hoteo North Domain'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23711, 'Sw Storage Drainage Basin Res 24 Sari Pl'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23743, '3308 South Head Road, South Head'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23753, 'Sw Storage Basin Res - 48 Westgate Drive'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23765, '55A Alnwick Street, Warkworth'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23768, 'Sw Storage Basin Res - 12 Blueridge Cl'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23769, 'Sw Storage Basin Res - 44 Greenberry Dr'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23772, 'Lot22 DP201990 Riverglade Lane,Matakana'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23774, '594 Swanson Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23777, 'Dairy Flat Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23785, '341-347 Henderson Valley Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23799, 'Sw Storage Basin Res - 12 Mantra Rd'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23800, 'Te Whau Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23812, 'Sesquicentennial Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23816, 'Woodcocks Road,Warkworth.Lot701-DP447445'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23817, 'Woodcocks Road,Warkworth.Lot132-DP447445'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23820, 'Rotary Grove (Warkworth)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 23821, 'Albro Lane');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23827, 'Woodcocks Road,Warkworth.Lot604 DP447445'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23828, '63 Flanshaw Road,Te Atatu South'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23829, 'John Andrew Dr, Warkworth-Lot 8 DP410072'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23831, '12 Waimauku Station Road Waimauku'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23837, 'Mahurangi West Road,Puhoi-Alt207 SO51660'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23844, '200A West Coast Road, Glen Eden'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23860, 'Kaipara Coast Highway-Allot 262 SO 44479'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23861, 'Horseshoe Bush Road -Sec 1 SO 68946'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23864, 'Wellsford Valley Rd-Allot N155 SO 18175'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23868, 'Fred Taylor Drive, Whenuapai (SO 449087)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23877, 'Ranui Library-431 Swanson Road, Ranui'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23880, 'Lot 5 DP119681 State Highway 1,Warkworth'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23884, 'Kiwitahi Road, Paehoka'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23886, 'Allot 217 SO 36169,Tahekeroa Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23888, 'Sec1 SO 46840, Waiteitei Rd, Waiteitei'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23889, 'Sec 3 SO 419103, Weranui Rd. Up Waiwera'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23901, '220-240 Shaw Road, Titirangi'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23904, 'Pt Allot 104 Matakana Road, Matakana'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23907, 'Allot 339 SO40091 Kapanui St, Warkworth'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23908, 'Sec 1 SO 327676 Springs Road, Parakai'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23909, 'Sec 9 SO 25051 Taiapa Road, Motutara'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23910, 'Se 9 SO336295, Kaipara View Rd, Kiwitahi'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23911, 'Sec 1 SO 62295 Wharehine, Tauhoa'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23912, 'SO 43985, Wellsford Valley Road, Te Hana'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23913, 'Sec36 SO45744, Pakiri Block Rd, Tomarata'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 23914, 'Sec 1 SO 67266, Pakiri Block Rd, Pakiri'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23925, '29 Te Ahuahu Road, Piha'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 23933, '74 Oreil Ave./Ministry of Edu./3rd Party'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 23962, 'Sw Storage Basin Res - 28 Anna Ln'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24030, 'Komakoriki Cemetery'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24043, 'Prawn Farm');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24044, 'Lyttle Lane');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24045, 'Kumeu River');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24046, 'Hampton Mews');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24047, 'Robinia Place Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24048, '118 Percy Street'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24050, 'Trusts Esplanade Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24061, '370 Triangle Road, Henderson'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24070, 'Longburn Road adj. to 26, Henderson'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24071, 'Rathgar Road adj. to 169, Henderson'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24080, '1/16 Sarona Avenue, Glen Eden'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24081, 'Lax Reserve');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24094, 'Sw Storage Basin Res - 88-90 Pooks Rd'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24096, '895 State Highway 16 Waimauku'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24097, '315A Glengarry Road, Glen Eden'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24112, 'Okahukura Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24114, 'Omaha Estuary');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24115, 'Omaha Golf Course Bush'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24116, 'Omaha River Point Wells'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24117, 'Onehunga Stream Old Kaipara Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24118, 'Ourauwhare River Kaipara Coast Highway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24119, 'Pipitiwai Creek Helensville'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24134, 'Shop2, Glenmall Pl.,Glen Eden-3 rd Party'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24142, 'Mahurangi River Alnwick Street'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24146, '170 Ridge Road Scotts Landing Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24148, '430 Sunnyside Road Coatesville'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24149, 'Anderson Road Matakana'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24150, 'Ararimu Stream'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24151, 'Awaroa Stream Helensville'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24152, 'Baddeleys Creek'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24153, 'Batten Street Scout Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24154, 'Birdsall Stream Whangateau'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24155, 'Brick Bay Drive'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24156, 'Brigham Creek Esplanade Coatesville'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24158, 'Constable Road Muriwai'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24159, 'Cowan Bay Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24160, 'Crabb Fields Lane Riverhead'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24161, 'Duck Creek Warkworth'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24163, 'Fairy Hill Road Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24165, 'Forestry Road Waitoki'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24167, 'Goldsworthy Bay'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24168, 'Goodall Road');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24169, 'Grange Street');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24170, 'Greenwood Road Pakiri'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24172, 'Hamilton Road Snells Beach'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24173, 'Haururu Stream Monowai Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24174, 'Hepburn Creek Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24175, 'Hoteo North Recreation Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24176, 'Hoteo River Wayby Valley'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24177, 'Kaipara Coast Highway Kakanui'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24178, 'Kaipara Flats Railway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24179, 'Kaipara River Helensville'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24180, 'Kaipara River Parakai'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24181, 'Kaipara River Waimauku'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24182, 'Kaukapakapa River'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24183, 'Kent Terrace Esplanade Riverhead'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24184, 'Kourawhero Stream Hall Lane'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24185, 'Kraak Road corner Dome Valley'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24186, 'Kumeu River Taupaki'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24188, 'Lloyd Road Esplanade Riverhead'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24190, 'Higham Road Waionek'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24191, 'South Head Road Mairetahi'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24192, 'Mahurangi East Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24193, 'Mahurangi River Kowhai View'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24194, 'Mahurangi River Sandspit Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24195, 'Mahurangi West Road Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24196, 'Makarau River');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24197, 'Matakana River Esplanades'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24198, 'McElroy Scenic Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24199, 'Mildred Amy Kerr-Taylor Recreation'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24200, 'Mohenui Stream Coatesville'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24201, 'North Cove Kawau Island'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24210, 'Pipitiwai Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24211, 'Puhoi River');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24212, 'Puhoi River Wenderholm Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24213, 'Pukapuka Road Esplanades'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24214, 'Rangitopuni Stream Dairy Flat'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24215, 'Rangitopuni Stream Coatesville'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24216, 'Rangitopuni Stream Riverhead'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24218, 'Run Road Esplanades'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24219, 'Short Road Esplanade Riverhead'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24220, 'South Head Road Haranui'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24221, 'South Head Road Parkhurst'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24222, 'South Head Road Waioneke'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24224, 'Takapau Reserve South Head Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24225, 'Takatu Road Esplanades'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24226, 'Tamahunga Stream Leigh Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24227, 'Te Hihi Creek South Head Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24228, 'Te Pahi Stream Naumai'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24229, 'Ti Point Road Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24232, 'Totara Road Esplanade Leigh'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24233, 'Upper Waiwera Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24234, 'Vivian Bay Kawau Island'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24236, 'Waireia River Wharehine Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24237, 'Waiteitei Stream Tomarata Valley Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24238, 'Waitoki Stream Colgan Lane'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24239, 'Waitoki Stream Kahikatea Flat Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24240, 'Waitoki Stream Pebble Brook Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24242, 'Wautaiti Stream Riverhead'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24246, 'Weiti Stream Pine Valley Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24247, 'Wellsford Valley Road'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24248, 'West Coast Road Komokoriki Hill'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24249, 'Whangateau Recreation Reserve Islands'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24251, 'Jordan Road');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24255, 'Snells Beach Esplanade'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24261, 'Christopher Lane Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24262, 'Evelyn Street Stormwater Pond'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24263, 'Wright Road Redvale'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24264, 'Blue Gum Drive'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24267, 'Waikahikatea Stream Kaukapakapa'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24273, 'Windy Ridge (Provisional)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24280, 'Sw Storage Basin Res - 2 Mt Lebanon Ln'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24303, 'Anamata Stream Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24304, 'McLeods Farm');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24315, 'Sunshine Boulevard Reserve (Provisional)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24316, 'Sunshine Boulevard Accessway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24320, 'Horseshoe Bay Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24333, 'Sw Storage Basin Res - 26 Westgate Dr'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24363, 'Lot 207 DP465999, Aurora Ave, Snells Bea'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24367, 'Ariki Drive Walkways'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24369, 'Darroch Shipyard Bridge Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24370, 'Te Pumanawa Square'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24375, '641D West Coast Rd Esp Res (Provisional)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24381, 'Sw Storage Basin Res - Babich Rd'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24383, 'Clinton Road-Baddeleys Beach Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24392, 'Sw Drainage Basin Res - Tiriwa Dr'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24401, 'Atarua Way Reserve (Provisional)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24431, 'Sw Storage Basin Res - 15 Seymour Rd'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24432, 'Bethells Road Esplanade Reserve B (Prov)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24433, 'Jonkers Road Recreation Reserve (Prov)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24434, 'Taumata Scenic Reserve (Provisional)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24435, 'Waitakere Quarry Scenic Reserve (Prov)'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24443, 'Puhoi Fire Station'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24444, 'Puhoi Band Rotunda'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
('Rodney', 24452, 'Weza Lane');
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24460, 'Kumeu Lions Garden Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24461, 'Mokihi Reserve - Provisional'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24462, 'Karaka Esplanade Reserve - Provisional'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Waitakere Ranges', 24471, 'Waikumete Esplanade Reserve - Prov'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24480, '3 Harrison Street, Wellsford'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24481, 'Lake Ototoa Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24520, 'Nirmal Place Utility Reserve'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24551, 'Success-Dungarvon-Dornie Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24552, 'Blue Bell-Thistle-Day Dawn Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24553, 'Day Dawn-Blue Bell-Darroch Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24554, 'Jane Gifford-Meiklejohn Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24560, 'Dungarvon-Blue Bell Walkway'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Henderson-Massey', 24610, 'Kopupaka Park'
);
/* INSERT QUERY */
INSERT INTO site(
localboarddescription,
site,
sitedescription




)
VALUES
(
'Rodney', 24614, 'Jamie Lane Reserve'
);
