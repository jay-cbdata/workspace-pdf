package com.ums.b2b.esb.entity;

/**
 * A fault representing a failure of a system to handle a message, even though
 * that message may have been successfully received. It is expected that the
 * caller will retry the message. An example is where a message was received but
 * a critical back-end system was unavailable.
 * 
 * @author Sankalp
 */
@SuppressWarnings("serial")
public class UnprocessedMessageFault extends AbstractRemoteFault implements java.io.Serializable {

	public UnprocessedMessageFault() {
	}

	public UnprocessedMessageFault(java.lang.String faultCode, java.lang.String faultText,
			java.lang.String faultReference) {
		super(faultCode, faultText, faultReference);
	}

	private java.lang.Object __equalsCalc = null;

	@SuppressWarnings("unused")
	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof UnprocessedMessageFault))
			return false;
		UnprocessedMessageFault other = (UnprocessedMessageFault) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj);
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		__hashCodeCalc = false;
		return _hashCode;
	}
}