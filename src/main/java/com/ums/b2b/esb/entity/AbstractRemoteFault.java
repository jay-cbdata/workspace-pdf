package com.ums.b2b.esb.entity;

/**
 * The base object for faults originating in a remote system that are propagated
 * back to the caller.
 * 
 * @author Sankalp
 */
@SuppressWarnings("serial")
public class AbstractRemoteFault implements java.io.Serializable {

	/*
	 * A locale-independent code that can be mapped to the nature or cause of
	 * the fault by the party generating this fault.
	 */
	private java.lang.String faultCode;

	/* An informational message describing the fault. */
	private java.lang.String faultText;

	/*
	 * An optional reference to the instance of the fault that can be used by
	 * the party generating the fault to locate the instance of the fault.
	 */
	private java.lang.String faultReference;

	public AbstractRemoteFault() {
		super();
	}

	public AbstractRemoteFault(String faultCode, String faultText, String faultReference) {
		super();
		this.faultCode = faultCode;
		this.faultText = faultText;
		this.faultReference = faultReference;
	}

	/**
	 * Gets the faultCode value for this AbstractRemoteFault.
	 * 
	 * @return faultCode * A locale-independent code that can be mapped to the
	 *         nature or cause of the fault by the party generating this fault.
	 */
	public java.lang.String getFaultCode() {
		return faultCode;
	}

	/**
	 * Sets the faultCode value for this AbstractRemoteFault.
	 * 
	 * @param faultCode
	 *            * A locale-independent code that can be mapped to the nature
	 *            or cause of the fault by the party generating this fault.
	 */
	public void setFaultCode(java.lang.String faultCode) {
		this.faultCode = faultCode;
	}

	/**
	 * Gets the faultText value for this AbstractRemoteFault.
	 * 
	 * @return faultText * An informational message describing the fault.
	 */
	public java.lang.String getFaultText() {
		return faultText;
	}

	/**
	 * Sets the faultText value for this AbstractRemoteFault.
	 * 
	 * @param faultText
	 *            * An informational message describing the fault.
	 */
	public void setFaultText(java.lang.String faultText) {
		this.faultText = faultText;
	}

	/**
	 * Gets the faultReference value for this AbstractRemoteFault.
	 * 
	 * @return faultReference * An optional reference to the instance of the
	 *         fault that can be used by the party generating the fault to
	 *         locate the instance of the fault.
	 */
	public java.lang.String getFaultReference() {
		return faultReference;
	}

	/**
	 * Sets the faultReference value for this AbstractRemoteFault.
	 * 
	 * @param faultReference
	 *            * An optional reference to the instance of the fault that can
	 *            be used by the party generating the fault to locate the
	 *            instance of the fault.
	 */
	public void setFaultReference(java.lang.String faultReference) {
		this.faultReference = faultReference;
	}

	private java.lang.Object __equalsCalc = null;

	@SuppressWarnings("unused")
	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof AbstractRemoteFault))
			return false;
		AbstractRemoteFault other = (AbstractRemoteFault) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.faultCode == null && other.getFaultCode() == null)
						|| (this.faultCode != null && this.faultCode.equals(other.getFaultCode())))
				&& ((this.faultText == null && other.getFaultText() == null)
						|| (this.faultText != null && this.faultText.equals(other.getFaultText())))
				&& ((this.faultReference == null && other.getFaultReference() == null)
						|| (this.faultReference != null && this.faultReference.equals(other.getFaultReference())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getFaultCode() != null) {
			_hashCode += getFaultCode().hashCode();
		}
		if (getFaultText() != null) {
			_hashCode += getFaultText().hashCode();
		}
		if (getFaultReference() != null) {
			_hashCode += getFaultReference().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

}
