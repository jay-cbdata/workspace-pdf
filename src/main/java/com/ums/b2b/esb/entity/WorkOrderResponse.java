package com.ums.b2b.esb.entity;

public class WorkOrderResponse {

	private String ExternalReference;
	private String TemplateWorkOrderNumber;
	private String WorkSystemName;
	private boolean IsTemplate;
	private String Source;
	private String LevelType;
	private String status;
	private String ApprovalStatus;
	private String Stage;
	private String Details;
	private String orderId;
	private String supplierId;
	private String supplierOrderId;
	private String Type;
	
	public String getExternalReference() {
		return ExternalReference;
	}
	public void setExternalReference(String externalReference) {
		ExternalReference = externalReference;
	}
	public String getTemplateWorkOrderNumber() {
		return TemplateWorkOrderNumber;
	}
	public void setTemplateWorkOrderNumber(String templateWorkOrderNumber) {
		TemplateWorkOrderNumber = templateWorkOrderNumber;
	}
	public String getWorkSystemName() {
		return WorkSystemName;
	}
	public void setWorkSystemName(String workSystemName) {
		WorkSystemName = workSystemName;
	}
	public boolean isIsTemplate() {
		return IsTemplate;
	}
	public void setIsTemplate(boolean isTemplate) {
		IsTemplate = isTemplate;
	}
	public String getSource() {
		return Source;
	}
	public void setSource(String source) {
		Source = source;
	}
	public String getLevelType() {
		return LevelType;
	}
	public void setLevelType(String levelType) {
		LevelType = levelType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getApprovalStatus() {
		return ApprovalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		ApprovalStatus = approvalStatus;
	}
	public String getStage() {
		return Stage;
	}
	public void setStage(String stage) {
		Stage = stage;
	}
	public String getDetails() {
		return Details;
	}
	public void setDetails(String details) {
		Details = details;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the supplierId
	 */
	public String getSupplierId() {
		return supplierId;
	}
	/**
	 * @param supplierId the supplierId to set
	 */
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	/**
	 * @return the supplierOrderId
	 */
	public String getSupplierOrderId() {
		return supplierOrderId;
	}
	/**
	 * @param supplierOrderId the supplierOrderId to set
	 */
	public void setSupplierOrderId(String supplierOrderId) {
		this.supplierOrderId = supplierOrderId;
	}
	

}
