package com.ums.b2b.esb.entity;

/**
 * OperationService.java
 *
 * @author Sankalp
 */
@SuppressWarnings("serial")
public class Service implements java.io.Serializable {

	private String serviceId;
	private String plannedCost;
	private String uom;
	private String serviceQuantity;
	/* estimated cost in detail */
	private EstimateCostDetails estCostDetail;

	public Service() {
		super();
	}

	public Service(String serviceId, String plannedCost, String uom, String serviceQuantity,
			EstimateCostDetails estCostDetail) {
		super();
		this.serviceId = serviceId;
		this.plannedCost = plannedCost;
		this.uom = uom;
		this.serviceQuantity = serviceQuantity;
		this.estCostDetail = estCostDetail;
	}

	/**
	 * Gets the serviceId value for this OperationService.
	 * 
	 * @return serviceId
	 */
	public java.lang.String getServiceId() {
		return serviceId;
	}

	/**
	 * Sets the serviceId value for this OperationService.
	 * 
	 * @param serviceId
	 */
	public void setServiceId(java.lang.String serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * Gets the serviceQuantity value for this OperationService.
	 * 
	 * @return serviceQuantity
	 */
	public java.lang.String getServiceQuantity() {
		return serviceQuantity;
	}

	/**
	 * Sets the serviceQuantity value for this OperationService.
	 * 
	 * @param serviceQuantity
	 */
	public void setServiceQuantity(java.lang.String serviceQuantity) {
		this.serviceQuantity = serviceQuantity;
	}

	/**
	 * Gets the uom value for this OperationService.
	 * 
	 * @return uom
	 */
	public java.lang.String getUom() {
		return uom;
	}

	/**
	 * Sets the uom value for this OperationService.
	 * 
	 * @param uom
	 */
	public void setUom(java.lang.String uom) {
		this.uom = uom;
	}

	/**
	 * Gets the plannedCost value for this OperationService.
	 * 
	 * @return plannedCost
	 */
	public java.lang.String getPlannedCost() {
		return plannedCost;
	}

	/**
	 * Sets the plannedCost value for this OperationService.
	 * 
	 * @param plannedCost
	 */
	public void setPlannedCost(java.lang.String plannedCost) {
		this.plannedCost = plannedCost;
	}

	/**
	 * Gets the estimatedCostDetail value for this OperationService.
	 * 
	 * @return estimatedCostDetail * estimated cost in detail
	 */
	public EstimateCostDetails getEstCostDetail() {
		return estCostDetail;
	}

	/**
	 * Sets the estimatedCostDetail value for this OperationService.
	 * 
	 * @param estimatedCostDetail
	 *            * estimated cost in detail
	 */
	public void setEstCostDetail(EstimateCostDetails estCostDetail) {
		this.estCostDetail = estCostDetail;
	}

	private java.lang.Object __equalsCalc = null;

	@SuppressWarnings("unused")
	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Service))
			return false;
		Service other = (Service) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.serviceId == null && other.getServiceId() == null)
						|| (this.serviceId != null && this.serviceId.equals(other.getServiceId())))
				&& ((this.serviceQuantity == null && other.getServiceQuantity() == null)
						|| (this.serviceQuantity != null && this.serviceQuantity.equals(other.getServiceQuantity())))
				&& ((this.uom == null && other.getUom() == null)
						|| (this.uom != null && this.uom.equals(other.getUom())))
				&& ((this.plannedCost == null && other.getPlannedCost() == null)
						|| (this.plannedCost != null && this.plannedCost.equals(other.getPlannedCost())))
				&& ((this.estCostDetail == null && other.getEstCostDetail() == null)
						|| (this.estCostDetail != null && this.estCostDetail.equals(other.getEstCostDetail())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getServiceId() != null) {
			_hashCode += getServiceId().hashCode();
		}
		if (getServiceQuantity() != null) {
			_hashCode += getServiceQuantity().hashCode();
		}
		if (getUom() != null) {
			_hashCode += getUom().hashCode();
		}
		if (getPlannedCost() != null) {
			_hashCode += getPlannedCost().hashCode();
		}
		if (getEstCostDetail() != null) {
			_hashCode += getEstCostDetail().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}
}