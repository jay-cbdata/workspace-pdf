package com.ums.b2b.esb.entity;
/**
 * Code.java
 *
 * @author Sankalp
 */

@SuppressWarnings("serial")
public class Code  implements java.io.Serializable  {

	 private java.lang.String code;

	    private java.lang.String codeGroup;

	    public Code() {
	    }

	    public Code(
	           java.lang.String code,
	           java.lang.String codeGroup) {
	           this.code = code;
	           this.codeGroup = codeGroup;
	    }


	    /**
	     * Gets the code value for this Code.
	     * 
	     * @return code
	     */
	    public java.lang.String getCode() {
	        return code;
	    }


	    /**
	     * Sets the code value for this Code.
	     * 
	     * @param code
	     */
	    public void setCode(java.lang.String code) {
	        this.code = code;
	    }


	    /**
	     * Gets the codeGroup value for this Code.
	     * 
	     * @return codeGroup
	     */
	    public java.lang.String getCodeGroup() {
	        return codeGroup;
	    }


	    /**
	     * Sets the codeGroup value for this Code.
	     * 
	     * @param codeGroup
	     */
	    public void setCodeGroup(java.lang.String codeGroup) {
	        this.codeGroup = codeGroup;
	    }

	    private java.lang.Object __equalsCalc = null;
	    @SuppressWarnings("unused")
		public synchronized boolean equals(java.lang.Object obj) {
	        if (!(obj instanceof Code)) return false;
	        Code other = (Code) obj;
	        if (obj == null) return false;
	        if (this == obj) return true;
	        if (__equalsCalc != null) {
	            return (__equalsCalc == obj);
	        }
	        __equalsCalc = obj;
	        boolean _equals;
	        _equals = true && 
	            ((this.code==null && other.getCode()==null) || 
	             (this.code!=null &&
	              this.code.equals(other.getCode()))) &&
	            ((this.codeGroup==null && other.getCodeGroup()==null) || 
	             (this.codeGroup!=null &&
	              this.codeGroup.equals(other.getCodeGroup())));
	        __equalsCalc = null;
	        return _equals;
	    }

	    private boolean __hashCodeCalc = false;
	    public synchronized int hashCode() {
	        if (__hashCodeCalc) {
	            return 0;
	        }
	        __hashCodeCalc = true;
	        int _hashCode = 1;
	        if (getCode() != null) {
	            _hashCode += getCode().hashCode();
	        }
	        if (getCodeGroup() != null) {
	            _hashCode += getCodeGroup().hashCode();
	        }
	        __hashCodeCalc = false;
	        return _hashCode;
	    }
}