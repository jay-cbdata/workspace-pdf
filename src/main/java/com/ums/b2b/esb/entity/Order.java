/**
 * Order.java
 *
 * @author Sankalp
 */

package com.ums.b2b.esb.entity;

@SuppressWarnings("serial")
public class Order  implements java.io.Serializable {
    /* work order identifier in AKLC system */
    private java.lang.String orderId;

    private java.lang.String parentOrder;

    /* object to refer the duplicate orderId */
    private java.lang.String duplicateOf;

    /* work order type (reactive or planned) */
    private java.lang.String orderType;

    /* text description of the work, as entered by the customer services */
    private java.lang.String workDescription;

    /* work order status */
    private java.lang.String status;

    /* priority of the work order */
    private java.lang.String priority;

    /* date work started */
    private java.util.Calendar startDate;

    /* date work finished */
    private java.util.Calendar finishDate;

    /* maintenance activity type */
    private java.lang.String activityType;

    /* defines the group to which an order belongs */
    private java.lang.String plannerGroup;

    private java.lang.String sequenceId;

    private java.lang.String mainWorkCentre;

    /* closest location of the asset */
    private java.lang.String address;

    /* functional location identifier */
    private java.lang.String locationId;

    /* asset equipment identifier text Id */
    private java.lang.String equipmentId;

    /* date and time when work order was approved */
    private java.util.Calendar releaseDate;

    /* date and time when the work order was last updated */
    private java.util.Calendar lastUpdated;

    /* required start time of the work order. date time by which the
     * job should be started (per SLA) */
    private java.util.Calendar requiredStart;

    /* required finish time of the work order. date time by which
     * job should be finished (per SLA) */
    private java.util.Calendar requiredFinish;

    /* stormwater's purchase order for the contract */
    private java.lang.String purchaseOrderId;

    /* item number of the purchase order */
    private java.lang.String purchaseOrderItemId;

    /* estimated total costs of the work order */
    private java.lang.String estimatedCosts;

    /* X coordinate (Mercator) for job */
    private java.lang.String loggedXCoordinate;

    /* Y coordinate (Mercator) for job */
    private java.lang.String loggedYCoordinate;

    private java.lang.String actualXCoordinate;

    private java.lang.String actualYCoordinate;

    /* suppliers internal work order number */
    private java.lang.String supplierOrderId;

    /* supplier AKLC allocated identifier (vendor number) */
    private java.lang.String supplierId;

    private  Summary summary;

    public Order() {
    }

    public Order(
           java.lang.String orderId,
           java.lang.String parentOrder,
           java.lang.String duplicateOf,
           java.lang.String orderType,
           java.lang.String workDescription,
           java.lang.String status,
           java.lang.String priority,
           java.util.Calendar startDate,
           java.util.Calendar finishDate,
           java.lang.String activityType,
           java.lang.String plannerGroup,
           java.lang.String sequenceId,
           java.lang.String mainWorkCentre,
           java.lang.String address,
           java.lang.String locationId,
           java.lang.String equipmentId,
           java.util.Calendar releaseDate,
           java.util.Calendar lastUpdated,
           java.util.Calendar requiredStart,
           java.util.Calendar requiredFinish,
           java.lang.String purchaseOrderId,
           java.lang.String purchaseOrderItemId,
           java.lang.String estimatedCosts,
           java.lang.String loggedXCoordinate,
           java.lang.String loggedYCoordinate,
           java.lang.String actualXCoordinate,
           java.lang.String actualYCoordinate,
           java.lang.String supplierOrderId,
           java.lang.String supplierId,
            Summary summary) {
           this.orderId = orderId;
           this.parentOrder = parentOrder;
           this.duplicateOf = duplicateOf;
           this.orderType = orderType;
           this.workDescription = workDescription;
           this.status = status;
           this.priority = priority;
           this.startDate = startDate;
           this.finishDate = finishDate;
           this.activityType = activityType;
           this.plannerGroup = plannerGroup;
           this.sequenceId = sequenceId;
           this.mainWorkCentre = mainWorkCentre;
           this.address = address;
           this.locationId = locationId;
           this.equipmentId = equipmentId;
           this.releaseDate = releaseDate;
           this.lastUpdated = lastUpdated;
           this.requiredStart = requiredStart;
           this.requiredFinish = requiredFinish;
           this.purchaseOrderId = purchaseOrderId;
           this.purchaseOrderItemId = purchaseOrderItemId;
           this.estimatedCosts = estimatedCosts;
           this.loggedXCoordinate = loggedXCoordinate;
           this.loggedYCoordinate = loggedYCoordinate;
           this.actualXCoordinate = actualXCoordinate;
           this.actualYCoordinate = actualYCoordinate;
           this.supplierOrderId = supplierOrderId;
           this.supplierId = supplierId;
           this.summary = summary;
    }


    /**
     * Gets the orderId value for this Order.
     * 
     * @return orderId   * work order identifier in AKLC system
     */
    public java.lang.String getOrderId() {
        return orderId;
    }


    /**
     * Sets the orderId value for this Order.
     * 
     * @param orderId   * work order identifier in AKLC system
     */
    public void setOrderId(java.lang.String orderId) {
        this.orderId = orderId;
    }


    /**
     * Gets the parentOrder value for this Order.
     * 
     * @return parentOrder
     */
    public java.lang.String getParentOrder() {
        return parentOrder;
    }


    /**
     * Sets the parentOrder value for this Order.
     * 
     * @param parentOrder
     */
    public void setParentOrder(java.lang.String parentOrder) {
        this.parentOrder = parentOrder;
    }


    /**
     * Gets the duplicateOf value for this Order.
     * 
     * @return duplicateOf   * object to refer the duplicate orderId
     */
    public java.lang.String getDuplicateOf() {
        return duplicateOf;
    }


    /**
     * Sets the duplicateOf value for this Order.
     * 
     * @param duplicateOf   * object to refer the duplicate orderId
     */
    public void setDuplicateOf(java.lang.String duplicateOf) {
        this.duplicateOf = duplicateOf;
    }


    /**
     * Gets the orderType value for this Order.
     * 
     * @return orderType   * work order type (reactive or planned)
     */
    public java.lang.String getOrderType() {
        return orderType;
    }


    /**
     * Sets the orderType value for this Order.
     * 
     * @param orderType   * work order type (reactive or planned)
     */
    public void setOrderType(java.lang.String orderType) {
        this.orderType = orderType;
    }


    /**
     * Gets the workDescription value for this Order.
     * 
     * @return workDescription   * text description of the work, as entered by the customer services
     */
    public java.lang.String getWorkDescription() {
        return workDescription;
    }


    /**
     * Sets the workDescription value for this Order.
     * 
     * @param workDescription   * text description of the work, as entered by the customer services
     */
    public void setWorkDescription(java.lang.String workDescription) {
        this.workDescription = workDescription;
    }


    /**
     * Gets the status value for this Order.
     * 
     * @return status   * work order status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Order.
     * 
     * @param status   * work order status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the priority value for this Order.
     * 
     * @return priority   * priority of the work order
     */
    public java.lang.String getPriority() {
        return priority;
    }


    /**
     * Sets the priority value for this Order.
     * 
     * @param priority   * priority of the work order
     */
    public void setPriority(java.lang.String priority) {
        this.priority = priority;
    }


    /**
     * Gets the startDate value for this Order.
     * 
     * @return startDate   * date work started
     */
    public java.util.Calendar getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this Order.
     * 
     * @param startDate   * date work started
     */
    public void setStartDate(java.util.Calendar startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the finishDate value for this Order.
     * 
     * @return finishDate   * date work finished
     */
    public java.util.Calendar getFinishDate() {
        return finishDate;
    }


    /**
     * Sets the finishDate value for this Order.
     * 
     * @param finishDate   * date work finished
     */
    public void setFinishDate(java.util.Calendar finishDate) {
        this.finishDate = finishDate;
    }


    /**
     * Gets the activityType value for this Order.
     * 
     * @return activityType   * maintenance activity type
     */
    public java.lang.String getActivityType() {
        return activityType;
    }


    /**
     * Sets the activityType value for this Order.
     * 
     * @param activityType   * maintenance activity type
     */
    public void setActivityType(java.lang.String activityType) {
        this.activityType = activityType;
    }


    /**
     * Gets the plannerGroup value for this Order.
     * 
     * @return plannerGroup   * defines the group to which an order belongs
     */
    public java.lang.String getPlannerGroup() {
        return plannerGroup;
    }


    /**
     * Sets the plannerGroup value for this Order.
     * 
     * @param plannerGroup   * defines the group to which an order belongs
     */
    public void setPlannerGroup(java.lang.String plannerGroup) {
        this.plannerGroup = plannerGroup;
    }


    /**
     * Gets the sequenceId value for this Order.
     * 
     * @return sequenceId
     */
    public java.lang.String getSequenceId() {
        return sequenceId;
    }


    /**
     * Sets the sequenceId value for this Order.
     * 
     * @param sequenceId
     */
    public void setSequenceId(java.lang.String sequenceId) {
        this.sequenceId = sequenceId;
    }


    /**
     * Gets the mainWorkCentre value for this Order.
     * 
     * @return mainWorkCentre
     */
    public java.lang.String getMainWorkCentre() {
        return mainWorkCentre;
    }


    /**
     * Sets the mainWorkCentre value for this Order.
     * 
     * @param mainWorkCentre
     */
    public void setMainWorkCentre(java.lang.String mainWorkCentre) {
        this.mainWorkCentre = mainWorkCentre;
    }


    /**
     * Gets the address value for this Order.
     * 
     * @return address   * closest location of the asset
     */
    public java.lang.String getAddress() {
        return address;
    }


    /**
     * Sets the address value for this Order.
     * 
     * @param address   * closest location of the asset
     */
    public void setAddress(java.lang.String address) {
        this.address = address;
    }


    /**
     * Gets the locationId value for this Order.
     * 
     * @return locationId   * functional location identifier
     */
    public java.lang.String getLocationId() {
        return locationId;
    }


    /**
     * Sets the locationId value for this Order.
     * 
     * @param locationId   * functional location identifier
     */
    public void setLocationId(java.lang.String locationId) {
        this.locationId = locationId;
    }


    /**
     * Gets the equipmentId value for this Order.
     * 
     * @return equipmentId   * asset equipment identifier text Id
     */
    public java.lang.String getEquipmentId() {
        return equipmentId;
    }


    /**
     * Sets the equipmentId value for this Order.
     * 
     * @param equipmentId   * asset equipment identifier text Id
     */
    public void setEquipmentId(java.lang.String equipmentId) {
        this.equipmentId = equipmentId;
    }


    /**
     * Gets the releaseDate value for this Order.
     * 
     * @return releaseDate   * date and time when work order was approved
     */
    public java.util.Calendar getReleaseDate() {
        return releaseDate;
    }


    /**
     * Sets the releaseDate value for this Order.
     * 
     * @param releaseDate   * date and time when work order was approved
     */
    public void setReleaseDate(java.util.Calendar releaseDate) {
        this.releaseDate = releaseDate;
    }


    /**
     * Gets the lastUpdated value for this Order.
     * 
     * @return lastUpdated   * date and time when the work order was last updated
     */
    public java.util.Calendar getLastUpdated() {
        return lastUpdated;
    }


    /**
     * Sets the lastUpdated value for this Order.
     * 
     * @param lastUpdated   * date and time when the work order was last updated
     */
    public void setLastUpdated(java.util.Calendar lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    /**
     * Gets the requiredStart value for this Order.
     * 
     * @return requiredStart   * required start time of the work order. date time by which the
     * job should be started (per SLA)
     */
    public java.util.Calendar getRequiredStart() {
        return requiredStart;
    }


    /**
     * Sets the requiredStart value for this Order.
     * 
     * @param requiredStart   * required start time of the work order. date time by which the
     * job should be started (per SLA)
     */
    public void setRequiredStart(java.util.Calendar requiredStart) {
        this.requiredStart = requiredStart;
    }


    /**
     * Gets the requiredFinish value for this Order.
     * 
     * @return requiredFinish   * required finish time of the work order. date time by which
     * job should be finished (per SLA)
     */
    public java.util.Calendar getRequiredFinish() {
        return requiredFinish;
    }


    /**
     * Sets the requiredFinish value for this Order.
     * 
     * @param requiredFinish   * required finish time of the work order. date time by which
     * job should be finished (per SLA)
     */
    public void setRequiredFinish(java.util.Calendar requiredFinish) {
        this.requiredFinish = requiredFinish;
    }


    /**
     * Gets the purchaseOrderId value for this Order.
     * 
     * @return purchaseOrderId   * stormwater's purchase order for the contract
     */
    public java.lang.String getPurchaseOrderId() {
        return purchaseOrderId;
    }


    /**
     * Sets the purchaseOrderId value for this Order.
     * 
     * @param purchaseOrderId   * stormwater's purchase order for the contract
     */
    public void setPurchaseOrderId(java.lang.String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }


    /**
     * Gets the purchaseOrderItemId value for this Order.
     * 
     * @return purchaseOrderItemId   * item number of the purchase order
     */
    public java.lang.String getPurchaseOrderItemId() {
        return purchaseOrderItemId;
    }


    /**
     * Sets the purchaseOrderItemId value for this Order.
     * 
     * @param purchaseOrderItemId   * item number of the purchase order
     */
    public void setPurchaseOrderItemId(java.lang.String purchaseOrderItemId) {
        this.purchaseOrderItemId = purchaseOrderItemId;
    }


    /**
     * Gets the estimatedCosts value for this Order.
     * 
     * @return estimatedCosts   * estimated total costs of the work order
     */
    public java.lang.String getEstimatedCosts() {
        return estimatedCosts;
    }


    /**
     * Sets the estimatedCosts value for this Order.
     * 
     * @param estimatedCosts   * estimated total costs of the work order
     */
    public void setEstimatedCosts(java.lang.String estimatedCosts) {
        this.estimatedCosts = estimatedCosts;
    }


    /**
     * Gets the loggedXCoordinate value for this Order.
     * 
     * @return loggedXCoordinate   * X coordinate (Mercator) for job
     */
    public java.lang.String getLoggedXCoordinate() {
        return loggedXCoordinate;
    }


    /**
     * Sets the loggedXCoordinate value for this Order.
     * 
     * @param loggedXCoordinate   * X coordinate (Mercator) for job
     */
    public void setLoggedXCoordinate(java.lang.String loggedXCoordinate) {
        this.loggedXCoordinate = loggedXCoordinate;
    }


    /**
     * Gets the loggedYCoordinate value for this Order.
     * 
     * @return loggedYCoordinate   * Y coordinate (Mercator) for job
     */
    public java.lang.String getLoggedYCoordinate() {
        return loggedYCoordinate;
    }


    /**
     * Sets the loggedYCoordinate value for this Order.
     * 
     * @param loggedYCoordinate   * Y coordinate (Mercator) for job
     */
    public void setLoggedYCoordinate(java.lang.String loggedYCoordinate) {
        this.loggedYCoordinate = loggedYCoordinate;
    }


    /**
     * Gets the actualXCoordinate value for this Order.
     * 
     * @return actualXCoordinate
     */
    public java.lang.String getActualXCoordinate() {
        return actualXCoordinate;
    }


    /**
     * Sets the actualXCoordinate value for this Order.
     * 
     * @param actualXCoordinate
     */
    public void setActualXCoordinate(java.lang.String actualXCoordinate) {
        this.actualXCoordinate = actualXCoordinate;
    }


    /**
     * Gets the actualYCoordinate value for this Order.
     * 
     * @return actualYCoordinate
     */
    public java.lang.String getActualYCoordinate() {
        return actualYCoordinate;
    }


    /**
     * Sets the actualYCoordinate value for this Order.
     * 
     * @param actualYCoordinate
     */
    public void setActualYCoordinate(java.lang.String actualYCoordinate) {
        this.actualYCoordinate = actualYCoordinate;
    }


    /**
     * Gets the supplierOrderId value for this Order.
     * 
     * @return supplierOrderId   * suppliers internal work order number
     */
    public java.lang.String getSupplierOrderId() {
        return supplierOrderId;
    }


    /**
     * Sets the supplierOrderId value for this Order.
     * 
     * @param supplierOrderId   * suppliers internal work order number
     */
    public void setSupplierOrderId(java.lang.String supplierOrderId) {
        this.supplierOrderId = supplierOrderId;
    }


    /**
     * Gets the supplierId value for this Order.
     * 
     * @return supplierId   * supplier AKLC allocated identifier (vendor number)
     */
    public java.lang.String getSupplierId() {
        return supplierId;
    }


    /**
     * Sets the supplierId value for this Order.
     * 
     * @param supplierId   * supplier AKLC allocated identifier (vendor number)
     */
    public void setSupplierId(java.lang.String supplierId) {
        this.supplierId = supplierId;
    }


    /**
     * Gets the summary value for this Order.
     * 
     * @return summary
     */
    public  Summary getSummary() {
        return summary;
    }


    /**
     * Sets the summary value for this Order.
     * 
     * @param summary
     */
    public void setSummary( Summary summary) {
        this.summary = summary;
    }

    private java.lang.Object __equalsCalc = null;
    @SuppressWarnings("unused")
	public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Order)) return false;
        Order other = (Order) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.orderId==null && other.getOrderId()==null) || 
             (this.orderId!=null &&
              this.orderId.equals(other.getOrderId()))) &&
            ((this.parentOrder==null && other.getParentOrder()==null) || 
             (this.parentOrder!=null &&
              this.parentOrder.equals(other.getParentOrder()))) &&
            ((this.duplicateOf==null && other.getDuplicateOf()==null) || 
             (this.duplicateOf!=null &&
              this.duplicateOf.equals(other.getDuplicateOf()))) &&
            ((this.orderType==null && other.getOrderType()==null) || 
             (this.orderType!=null &&
              this.orderType.equals(other.getOrderType()))) &&
            ((this.workDescription==null && other.getWorkDescription()==null) || 
             (this.workDescription!=null &&
              this.workDescription.equals(other.getWorkDescription()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.priority==null && other.getPriority()==null) || 
             (this.priority!=null &&
              this.priority.equals(other.getPriority()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.finishDate==null && other.getFinishDate()==null) || 
             (this.finishDate!=null &&
              this.finishDate.equals(other.getFinishDate()))) &&
            ((this.activityType==null && other.getActivityType()==null) || 
             (this.activityType!=null &&
              this.activityType.equals(other.getActivityType()))) &&
            ((this.plannerGroup==null && other.getPlannerGroup()==null) || 
             (this.plannerGroup!=null &&
              this.plannerGroup.equals(other.getPlannerGroup()))) &&
            ((this.sequenceId==null && other.getSequenceId()==null) || 
             (this.sequenceId!=null &&
              this.sequenceId.equals(other.getSequenceId()))) &&
            ((this.mainWorkCentre==null && other.getMainWorkCentre()==null) || 
             (this.mainWorkCentre!=null &&
              this.mainWorkCentre.equals(other.getMainWorkCentre()))) &&
            ((this.address==null && other.getAddress()==null) || 
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.locationId==null && other.getLocationId()==null) || 
             (this.locationId!=null &&
              this.locationId.equals(other.getLocationId()))) &&
            ((this.equipmentId==null && other.getEquipmentId()==null) || 
             (this.equipmentId!=null &&
              this.equipmentId.equals(other.getEquipmentId()))) &&
            ((this.releaseDate==null && other.getReleaseDate()==null) || 
             (this.releaseDate!=null &&
              this.releaseDate.equals(other.getReleaseDate()))) &&
            ((this.lastUpdated==null && other.getLastUpdated()==null) || 
             (this.lastUpdated!=null &&
              this.lastUpdated.equals(other.getLastUpdated()))) &&
            ((this.requiredStart==null && other.getRequiredStart()==null) || 
             (this.requiredStart!=null &&
              this.requiredStart.equals(other.getRequiredStart()))) &&
            ((this.requiredFinish==null && other.getRequiredFinish()==null) || 
             (this.requiredFinish!=null &&
              this.requiredFinish.equals(other.getRequiredFinish()))) &&
            ((this.purchaseOrderId==null && other.getPurchaseOrderId()==null) || 
             (this.purchaseOrderId!=null &&
              this.purchaseOrderId.equals(other.getPurchaseOrderId()))) &&
            ((this.purchaseOrderItemId==null && other.getPurchaseOrderItemId()==null) || 
             (this.purchaseOrderItemId!=null &&
              this.purchaseOrderItemId.equals(other.getPurchaseOrderItemId()))) &&
            ((this.estimatedCosts==null && other.getEstimatedCosts()==null) || 
             (this.estimatedCosts!=null &&
              this.estimatedCosts.equals(other.getEstimatedCosts()))) &&
            ((this.loggedXCoordinate==null && other.getLoggedXCoordinate()==null) || 
             (this.loggedXCoordinate!=null &&
              this.loggedXCoordinate.equals(other.getLoggedXCoordinate()))) &&
            ((this.loggedYCoordinate==null && other.getLoggedYCoordinate()==null) || 
             (this.loggedYCoordinate!=null &&
              this.loggedYCoordinate.equals(other.getLoggedYCoordinate()))) &&
            ((this.actualXCoordinate==null && other.getActualXCoordinate()==null) || 
             (this.actualXCoordinate!=null &&
              this.actualXCoordinate.equals(other.getActualXCoordinate()))) &&
            ((this.actualYCoordinate==null && other.getActualYCoordinate()==null) || 
             (this.actualYCoordinate!=null &&
              this.actualYCoordinate.equals(other.getActualYCoordinate()))) &&
            ((this.supplierOrderId==null && other.getSupplierOrderId()==null) || 
             (this.supplierOrderId!=null &&
              this.supplierOrderId.equals(other.getSupplierOrderId()))) &&
            ((this.supplierId==null && other.getSupplierId()==null) || 
             (this.supplierId!=null &&
              this.supplierId.equals(other.getSupplierId()))) &&
            ((this.summary==null && other.getSummary()==null) || 
             (this.summary!=null &&
              this.summary.equals(other.getSummary())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOrderId() != null) {
            _hashCode += getOrderId().hashCode();
        }
        if (getParentOrder() != null) {
            _hashCode += getParentOrder().hashCode();
        }
        if (getDuplicateOf() != null) {
            _hashCode += getDuplicateOf().hashCode();
        }
        if (getOrderType() != null) {
            _hashCode += getOrderType().hashCode();
        }
        if (getWorkDescription() != null) {
            _hashCode += getWorkDescription().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getPriority() != null) {
            _hashCode += getPriority().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getFinishDate() != null) {
            _hashCode += getFinishDate().hashCode();
        }
        if (getActivityType() != null) {
            _hashCode += getActivityType().hashCode();
        }
        if (getPlannerGroup() != null) {
            _hashCode += getPlannerGroup().hashCode();
        }
        if (getSequenceId() != null) {
            _hashCode += getSequenceId().hashCode();
        }
        if (getMainWorkCentre() != null) {
            _hashCode += getMainWorkCentre().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getLocationId() != null) {
            _hashCode += getLocationId().hashCode();
        }
        if (getEquipmentId() != null) {
            _hashCode += getEquipmentId().hashCode();
        }
        if (getReleaseDate() != null) {
            _hashCode += getReleaseDate().hashCode();
        }
        if (getLastUpdated() != null) {
            _hashCode += getLastUpdated().hashCode();
        }
        if (getRequiredStart() != null) {
            _hashCode += getRequiredStart().hashCode();
        }
        if (getRequiredFinish() != null) {
            _hashCode += getRequiredFinish().hashCode();
        }
        if (getPurchaseOrderId() != null) {
            _hashCode += getPurchaseOrderId().hashCode();
        }
        if (getPurchaseOrderItemId() != null) {
            _hashCode += getPurchaseOrderItemId().hashCode();
        }
        if (getEstimatedCosts() != null) {
            _hashCode += getEstimatedCosts().hashCode();
        }
        if (getLoggedXCoordinate() != null) {
            _hashCode += getLoggedXCoordinate().hashCode();
        }
        if (getLoggedYCoordinate() != null) {
            _hashCode += getLoggedYCoordinate().hashCode();
        }
        if (getActualXCoordinate() != null) {
            _hashCode += getActualXCoordinate().hashCode();
        }
        if (getActualYCoordinate() != null) {
            _hashCode += getActualYCoordinate().hashCode();
        }
        if (getSupplierOrderId() != null) {
            _hashCode += getSupplierOrderId().hashCode();
        }
        if (getSupplierId() != null) {
            _hashCode += getSupplierId().hashCode();
        }
        if (getSummary() != null) {
            _hashCode += getSummary().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }
  
}
