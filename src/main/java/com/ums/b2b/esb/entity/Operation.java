package com.ums.b2b.esb.entity;

public class Operation {

	private String operationId;
	private String workCentre;
	private String estimatedCosts;
	private String noAsset;
	private String assetNotFound;
	private String status;
	private String activitySubType;
	private String Description;
	private String requiredStart;
	private String requiredFinish;
	private String requestedStart;
	private String requestedFinish;
	private String dateChangeReason;
	private String actualStart;
	private String actualFinish;
	private String locationId;
	private String equipmentId;
	private String actualXCoordinate;
	private String actualYCoordinate;
	private Service service;
    private Summary summary;
    
	public String getOperationId() {
		return operationId;
	}
	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}
	public String getWorkCentre() {
		return workCentre;
	}
	public void setWorkCentre(String workCentre) {
		this.workCentre = workCentre;
	}
	public String getEstimatedCosts() {
		return estimatedCosts;
	}
	public void setEstimatedCosts(String estimatedCosts) {
		this.estimatedCosts = estimatedCosts;
	}
	public String getNoAsset() {
		return noAsset;
	}
	public void setNoAsset(String noAsset) {
		this.noAsset = noAsset;
	}
	public String getAssetNotFound() {
		return assetNotFound;
	}
	public void setAssetNotFound(String assetNotFound) {
		this.assetNotFound = assetNotFound;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getActivitySubType() {
		return activitySubType;
	}
	public void setActivitySubType(String activitySubType) {
		this.activitySubType = activitySubType;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getRequiredStart() {
		return requiredStart;
	}
	public void setRequiredStart(String requiredStart) {
		this.requiredStart = requiredStart;
	}
	public String getRequiredFinish() {
		return requiredFinish;
	}
	public void setRequiredFinish(String requiredFinish) {
		this.requiredFinish = requiredFinish;
	}
	public String getRequestedStart() {
		return requestedStart;
	}
	public void setRequestedStart(String requestedStart) {
		this.requestedStart = requestedStart;
	}
	public String getRequestedFinish() {
		return requestedFinish;
	}
	public void setRequestedFinish(String requestedFinish) {
		this.requestedFinish = requestedFinish;
	}
	public String getDateChangeReason() {
		return dateChangeReason;
	}
	public void setDateChangeReason(String dateChangeReason) {
		this.dateChangeReason = dateChangeReason;
	}
	public String getActualStart() {
		return actualStart;
	}
	public void setActualStart(String actualStart) {
		this.actualStart = actualStart;
	}
	public String getActualFinish() {
		return actualFinish;
	}
	public void setActualFinish(String actualFinish) {
		this.actualFinish = actualFinish;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getEquipmentId() {
		return equipmentId;
	}
	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}
	public String getActualXCoordinate() {
		return actualXCoordinate;
	}
	public void setActualXCoordinate(String actualXCoordinate) {
		this.actualXCoordinate = actualXCoordinate;
	}
	public String getActualYCoordinate() {
		return actualYCoordinate;
	}
	public void setActualYCoordinate(String actualYCoordinate) {
		this.actualYCoordinate = actualYCoordinate;
	}
	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
	public Summary getSummary() {
		return summary;
	}
	public void setSummary(Summary summary) {
		this.summary = summary;
	}
    
}
