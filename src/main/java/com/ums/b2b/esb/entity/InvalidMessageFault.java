package com.ums.b2b.esb.entity;

/**
 * A fault indicating that the recipient could not process the message due to
 * problems with the message format or content. This represents a fatal failure
 * in processing and the message should not be resubmitted by the caller.
 * 
 * @author Sankalp
 */
@SuppressWarnings("serial")
public class InvalidMessageFault extends AbstractRemoteFault implements java.io.Serializable {
	
	public InvalidMessageFault() {
	}

	public InvalidMessageFault(java.lang.String faultCode, java.lang.String faultText,
			java.lang.String faultReference) {
		super(faultCode, faultText, faultReference);
	}

	private java.lang.Object __equalsCalc = null;

	@SuppressWarnings("unused")
	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof InvalidMessageFault))
			return false;
		InvalidMessageFault other = (InvalidMessageFault) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj);
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		__hashCodeCalc = false;
		return _hashCode;
	}
}