package com.ums.b2b.esb.entity;
/**
 * Activity.java
 *
 * @author Sankalp
 */
@SuppressWarnings("serial")
public class Activity  implements java.io.Serializable {
    /* description of activity */
    private java.lang.String description;

    /* date time of the activity */
    private java.util.Calendar startDate;

    /* activity codes */
    private Code code;

    public Activity() {
    }

    public Activity(
           java.lang.String description,
           java.util.Calendar startDate,
           Code code) {
           this.description = description;
           this.startDate = startDate;
           this.code = code;
    }


    /**
     * Gets the description value for this Activity.
     * 
     * @return description   * description of activity
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Activity.
     * 
     * @param description   * description of activity
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the startDate value for this Activity.
     * 
     * @return startDate   * date time of the activity
     */
    public java.util.Calendar getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this Activity.
     * 
     * @param startDate   * date time of the activity
     */
    public void setStartDate(java.util.Calendar startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the code value for this Activity.
     * 
     * @return code   * activity codes
     */
    public Code getCode() {
        return code;
    }


    /**
     * Sets the code value for this Activity.
     * 
     * @param code   * activity codes
     */
    public void setCode(Code code) {
        this.code = code;
    }

    private java.lang.Object __equalsCalc = null;
    @SuppressWarnings("unused")
	public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Activity)) return false;
        Activity other = (Activity) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }
    }
