package com.ums.b2b.esb.entity;
/**
 * OperationServiceEstimatedCostDetail.java
 *
 * @author Sankalp
 */

@SuppressWarnings("serial")
public class EstimateCostDetails implements java.io.Serializable {
	
	/* labour cost */
	private java.lang.String labour;

	/* material cost */
	private java.lang.String material;

	/* equipment cost */
	private java.lang.String equipment;

	public EstimateCostDetails() {
	}

	public EstimateCostDetails(java.lang.String labour, java.lang.String material, java.lang.String equipment) {
		this.labour = labour;
		this.material = material;
		this.equipment = equipment;
	}

	/**
	 * Gets the labour value for this OperationServiceEstimatedCostDetail.
	 * 
	 * @return labour * labour cost
	 */
	public java.lang.String getLabour() {
		return labour;
	}

	/**
	 * Sets the labour value for this OperationServiceEstimatedCostDetail.
	 * 
	 * @param labour
	 *            * labour cost
	 */
	public void setLabour(java.lang.String labour) {
		this.labour = labour;
	}

	/**
	 * Gets the material value for this OperationServiceEstimatedCostDetail.
	 * 
	 * @return material * material cost
	 */
	public java.lang.String getMaterial() {
		return material;
	}

	/**
	 * Sets the material value for this OperationServiceEstimatedCostDetail.
	 * 
	 * @param material
	 *            * material cost
	 */
	public void setMaterial(java.lang.String material) {
		this.material = material;
	}

	/**
	 * Gets the equipment value for this OperationServiceEstimatedCostDetail.
	 * 
	 * @return equipment * equipment cost
	 */
	public java.lang.String getEquipment() {
		return equipment;
	}

	/**
	 * Sets the equipment value for this OperationServiceEstimatedCostDetail.
	 * 
	 * @param equipment
	 *            * equipment cost
	 */
	public void setEquipment(java.lang.String equipment) {
		this.equipment = equipment;
	}

	private java.lang.Object __equalsCalc = null;

	@SuppressWarnings("unused")
	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof EstimateCostDetails))
			return false;
		EstimateCostDetails other = (EstimateCostDetails) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.labour == null && other.getLabour() == null)
						|| (this.labour != null && this.labour.equals(other.getLabour())))
				&& ((this.material == null && other.getMaterial() == null)
						|| (this.material != null && this.material.equals(other.getMaterial())))
				&& ((this.equipment == null && other.getEquipment() == null)
						|| (this.equipment != null && this.equipment.equals(other.getEquipment())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getLabour() != null) {
			_hashCode += getLabour().hashCode();
		}
		if (getMaterial() != null) {
			_hashCode += getMaterial().hashCode();
		}
		if (getEquipment() != null) {
			_hashCode += getEquipment().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}
}