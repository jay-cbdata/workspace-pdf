package com.ums.b2b.esb.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ErrorResponse")
public class ErrorResponse {

	private boolean result;
	private int code;
	private String message;
	
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
