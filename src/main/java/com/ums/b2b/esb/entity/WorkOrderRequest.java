package com.ums.b2b.esb.entity;

import java.sql.Date;
import org.json.simple.JSONObject;

public class WorkOrderRequest {

	private String orderId;
	private String parentOrder;
	private String duplicateOf;
	private String orderType;
	private String workDescription;
	private String status;
	private String priority;
	private String startDate;
	private String finishDate;
	private String activityType;
	private String plannerGroup;
	private String sequenceId;
	private String mainWorkCentre;
	private String address;
	private String locationId;
	private String equipmentId;
	private String releaseDate;
	private String lastUpdated;
	private String requiredStart;
	private String requiredFinish;
	private String purchaseOrderId;
	private String purchaseOrderItemId;
	private String estimatedCosts;
	private String loggedXCoordinate;
	private String loggedYCoordinate;
	private String actualXCoordinate;
	private String actualYCoordinate;
	private String supplierOrderId;
	private String supplierId;
	private Summary summary;
	private Service service;
	private Operation operation;
    private String actionType;
    private String workSystemName;
    private String workOrderNumber;
    private JSONObject readForDetailJson;
    
    
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getParentOrder() {
		return parentOrder;
	}
	public void setParentOrder(String parentOrder) {
		this.parentOrder = parentOrder;
	}
	public String getDuplicateOf() {
		return duplicateOf;
	}
	public void setDuplicateOf(String duplicateOf) {
		this.duplicateOf = duplicateOf;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getWorkDescription() {
		return workDescription;
	}
	public void setWorkDescription(String workDescription) {
		this.workDescription = workDescription;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getFinishDate() {
		return finishDate;
	}
	public void setFinishDate(String finishDate) {
		this.finishDate = finishDate;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public String getPlannerGroup() {
		return plannerGroup;
	}
	public void setPlannerGroup(String plannerGroup) {
		this.plannerGroup = plannerGroup;
	}
	public String getSequenceId() {
		return sequenceId;
	}
	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}
	public String getMainWorkCentre() {
		return mainWorkCentre;
	}
	public void setMainWorkCentre(String mainWorkCentre) {
		this.mainWorkCentre = mainWorkCentre;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getEquipmentId() {
		return equipmentId;
	}
	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getRequiredStart() {
		return requiredStart;
	}
	public void setRequiredStart(String requiredStart) {
		this.requiredStart = requiredStart;
	}
	public String getRequiredFinish() {
		return requiredFinish;
	}
	public void setRequiredFinish(String requiredFinish) {
		this.requiredFinish = requiredFinish;
	}
	public String getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(String purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public String getPurchaseOrderItemId() {
		return purchaseOrderItemId;
	}
	public void setPurchaseOrderItemId(String purchaseOrderItemId) {
		this.purchaseOrderItemId = purchaseOrderItemId;
	}
	public String getEstimatedCosts() {
		return estimatedCosts;
	}
	public void setEstimatedCosts(String estimatedCosts) {
		this.estimatedCosts = estimatedCosts;
	}
	public String getLoggedXCoordinate() {
		return loggedXCoordinate;
	}
	public void setLoggedXCoordinate(String loggedXCoordinate) {
		this.loggedXCoordinate = loggedXCoordinate;
	}
	public String getLoggedYCoordinate() {
		return loggedYCoordinate;
	}
	public void setLoggedYCoordinate(String loggedYCoordinate) {
		this.loggedYCoordinate = loggedYCoordinate;
	}
	public String getActualXCoordinate() {
		return actualXCoordinate;
	}
	public void setActualXCoordinate(String actualXCoordinate) {
		this.actualXCoordinate = actualXCoordinate;
	}
	public String getActualYCoordinate() {
		return actualYCoordinate;
	}
	public void setActualYCoordinate(String actualYCoordinate) {
		this.actualYCoordinate = actualYCoordinate;
	}
	public String getSupplierOrderId() {
		return supplierOrderId;
	}
	public void setSupplierOrderId(String supplierOrderId) {
		this.supplierOrderId = supplierOrderId;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	public Summary getSummary() {
		return summary;
	}
	public void setSummary(Summary summary) {
		this.summary = summary;
	}
	public Operation getOperation() {
		return operation;
	}
	/**
	 * @return the service
	 */
	public Service getService() {
		return service;
	}
	/**
	 * @param service the service to set
	 */
	public void setService(Service service) {
		this.service = service;
	}
	/**
	 * @return the typeOfRequet
	 */
	public String getActionType() {
		return actionType;
	}
	/**
	 * @param typeOfRequet the typeOfRequet to set
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	/**
	 * @return the workSystemName
	 */
	public String getWorkSystemName() {
		return workSystemName;
	}
	/**
	 * @param workSystemName the workSystemName to set
	 */
	public void setWorkSystemName(String workSystemName) {
		this.workSystemName = workSystemName;
	}
	/**
	 * @return the workOrderNumber
	 */
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	/**
	 * @param workOrderNumber the workOrderNumber to set
	 */
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	/**
	 * @return the readForDetailJson
	 */
	public JSONObject getReadForDetailJson() {
		return readForDetailJson;
	}
	/**
	 * @param readForDetailJson the readForDetailJson to set
	 */
	public void setReadForDetailJson(JSONObject readForDetailJson) {
		this.readForDetailJson = readForDetailJson;
	}
	
	
}
