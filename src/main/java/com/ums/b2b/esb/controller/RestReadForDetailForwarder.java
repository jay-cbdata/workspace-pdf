package com.ums.b2b.esb.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.entity.WorkOrderRequest;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

public class RestReadForDetailForwarder {

	protected final static Log logger = LogFactory.getLog(RestReadForDetailForwarder.class);
	String response = null;

	public String callToTechoneReadForDetail(WorkOrderRequest workOrderRequest) throws Exception {
		logger.info(new UMSTimeDateLog().getTimeDateLog() +" Input to Read Detail Techone API \t" + System.getProperty("techonereadfordetailurl")
				+ workOrderRequest.getWorkSystemName() + "/" + workOrderRequest.getWorkOrderNumber());

		try {
			Client client = Client.create();
			WebResource webResource = client.resource(System.getProperty("techonereadfordetailurl")
					+ workOrderRequest.getWorkSystemName() + "/" + workOrderRequest.getWorkOrderNumber());
			ClientResponse responseClient = webResource.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization")).get(ClientResponse.class);
			response = responseClient.getEntity(String.class);
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject) parser.parse(response.toString());
			workOrderRequest.setReadForDetailJson(jsonObject);
			workOrderRequest.setWorkSystemName(workOrderRequest.getWorkSystemName());
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response from TechOne READ end" + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
}
