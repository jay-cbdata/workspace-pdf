package com.ums.b2b.esb.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.entity.WorkOrderRequest;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

public class RestWorkOrderApproveWithCF {
	protected final static Log logger = LogFactory.getLog(RestSaveForDetailWithCF.class);
	String response = null;

	public String approveWorkOrderWithCF(WorkOrderRequest workOrderRequest) throws Exception {
	    String keyedWorkOrderNumber = (String) workOrderRequest.getReadForDetailJson().get("KeyedWorkOrderNumber");
		String workSystemName = (String) workOrderRequest.getReadForDetailJson().get("WorkSystemName");

		try {
			Client client = Client.create();
			WebResource webResource = client.resource(System.getProperty("techoneapproveworkorderurl"));
			String input = "{\"WorkSystemName\":\""
					+ workSystemName + "\","
					// + "\"KeyedWorkOrderNumber\":\"" + keyedWorkOrderNumber+
					// "\","
					+ "\"KeyedWorkOrderNumber\":\"000138\"}";

			ClientResponse responseClient = webResource.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, input);
			response = responseClient.getEntity(String.class);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Input to Techone for Approving the WorkOrder via UMSESB \t" + input);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response from TechOne end for Approve Workorder \t" + response);
	} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
}
