package com.ums.b2b.esb.controller;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.entity.WorkOrderRequest;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

public class RestRequestForwarder {

	protected final static Log logger = LogFactory.getLog(RestRequestForwarder.class);
	String response = null;

	public String callToTechone(WorkOrderRequest workOrderRequest) throws Exception {
		System.setProperty("javax.net.ssl.trustStore", System.getProperty("ssl"));
		String filename = System.getProperty("java.home") + "/lib/security/cacerts".replace('/', File.separatorChar);
		//String filename = "/etc/ssl/certs/java/cacerts".replace('/', File.separatorChar);
		String password = System.getProperty("password");
		System.setProperty("javax.net.ssl.trustStore", filename);
		System.setProperty("javax.net.ssl.trustStorePassword", password);
		System.setProperty("https.protocols", System.getProperty("https.protocols"));

		try {
			Client client = Client.create();
			WebResource webResource = client.resource(System.getProperty("techonecreatefromtemplateurl"));
			String input = "{\"Source\":\"3P\"," + "\"ExternalReference\":\"SAP\","
					+ "\"ExternalReferenceEntityKey1\":\"" + workOrderRequest.getOrderId() + "\","
					+ "\"TemplateWorkOrderNumber\":\"RODNEY\"," + "\"WorkSystemName\":\"ACNZ_WORKS\","
					+ "\"IsTemplate\":\"false\"," + "\"LevelType\":\"H\"," + "\"Status\":\""
					+ "I" + "\"," + "\"ApprovalStatus\":\"X\"," + "\"Stage\":\"D\","
					+ "\"Type\":\"O\"," + "\"WorkTypeCode\":\"WT001\"," + "\"Description\":\""
					+ workOrderRequest.getWorkDescription() 
					//+ "\"," + "\"ActualStartDateTime\":\""
					////+ workOrderRequest.getOperation().getActualStart() 
					//+ "\"," + "\"ActualFinishDateTime\":\""
					//+ workOrderRequest.getOperation().getActualFinish() 
					+ "\"," + "\"ActivityCode\":\"ACT001\"}";
			ClientResponse responseClient = webResource.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, input);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" "+ responseClient.getStatus());
			response = responseClient.getEntity(String.class);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Input to Techone via UMSESB \t" + input);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response from TechOne end" + response);
			
			
			
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(response.toString());
			String workSystemName = (String) json.get("WorkSystemName");
			String keyedWorkOrderNumber = (String) json.get("WorkOrderNumber");
			
			workOrderRequest.setWorkSystemName(workSystemName);
			workOrderRequest.setWorkOrderNumber(keyedWorkOrderNumber);
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
}
