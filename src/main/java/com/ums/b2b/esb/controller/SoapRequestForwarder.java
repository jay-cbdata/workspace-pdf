package com.ums.b2b.esb.controller;
/**
 * SoapRequestForwarder.java
 *
 * @author Sankalp
 */

import java.io.File;
import java.io.StringWriter;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.List;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mule.api.MuleContext;
import org.mule.api.annotations.expressions.Lookup;
import org.mule.api.context.MuleContextAware;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.workorder.Activity;
import com.service.workorder.Operation.Service.EstimatedCostDetail;
import com.service.workorder.WorkOrder;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

public class SoapRequestForwarder implements MuleContextAware {
	@Lookup
	private static MuleContext muleContext;

	protected final static Log logger = LogFactory.getLog(SoapRequestForwarder.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mule.api.context.MuleContextAware#setMuleContext(org.mule.api.
	 * MuleContext)
	 */
	@Override
	public void setMuleContext(MuleContext muleContext) {
		this.muleContext = muleContext;
	}

	/**
	 * @param workOrderRequest
	 * @param esbId
	 * @return String
	 */
	public String callToCouncil(WorkOrder workOrderRequest, String esbId) {
		SOAPConnectionFactory soapConnectionFactory = null;
		try {
			soapConnectionFactory = SOAPConnectionFactory.newInstance();
		} catch (UnsupportedOperationException | SOAPException e) {
			e.printStackTrace();
		}
		SOAPConnection soapConnection = null;
		try {
			soapConnection = soapConnectionFactory.createConnection();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(System.getProperty("soapcouncilusername"),
						System.getProperty("soapcouncilpassword").toCharArray());
			}
		});
		SOAPMessage soapResponse = null;
		try {

			soapResponse = soapConnection.call(createSOAPRequest(workOrderRequest, esbId),
					System.getProperty("soapcouncilurl"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		final StringWriter soapResponseFromService = new StringWriter();
		try {
			TransformerFactory.newInstance().newTransformer().transform(new DOMSource(soapResponse.getSOAPPart()),
					new StreamResult(soapResponseFromService));
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
		logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response from Council \t" + soapResponseFromService.toString());
		return soapResponseFromService.toString();
	}

	private static SOAPMessage createSOAPRequest(WorkOrder workOrderRequest, String esbId) throws Exception {
		MessageFactory messageFactory = null;
		try {
			messageFactory = MessageFactory.newInstance();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPMessage soapMessage = null;
		try {
			soapMessage = messageFactory.createMessage();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPPart soapPart = soapMessage.getSOAPPart();
		System.setProperty("javax.net.ssl.trustStore", "true");
		String filename = System.getProperty("java.home") + "/lib/security/cacerts".replace('/', File.separatorChar);
		String password = "changeit";
		System.setProperty("javax.net.ssl.trustStore", filename);
		System.setProperty("javax.net.ssl.trustStorePassword", password);
		SOAPEnvelope envelope = null;
		try {
			envelope = soapPart.getEnvelope();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		try {
			envelope.addNamespaceDeclaration("urn", System.getProperty("soapserveruri"));
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPBody soapBody = null;
		try {
			soapBody = envelope.getBody();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPElement soapBodyElem = null;
		try {
			soapBodyElem = soapBody.addChildElement("createWorkOrder", "urn");
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPElement soapBodyElem1 = null;
		try {
			soapBodyElem1 = soapBodyElem.addChildElement("workOrderRequest");
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPElement soapBodyElem11 = null;
		try {
			soapBodyElem11 = soapBodyElem1.addChildElement("orderId");
			if (workOrderRequest.getOrderId() != null && !workOrderRequest.getOrderId().equals(""))
				soapBodyElem11.addTextNode(workOrderRequest.getOrderId());
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPElement soapBodyOrderTypeElem = null;
		try {
			soapBodyOrderTypeElem = soapBodyElem1.addChildElement("orderType");
			if (workOrderRequest.getOrderType() != null && !workOrderRequest.getOrderType().equals(""))
				soapBodyOrderTypeElem.addTextNode(workOrderRequest.getOrderType());
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPElement soapBodyWorkDescriptionElem = null;
		try {
			soapBodyWorkDescriptionElem = soapBodyElem1.addChildElement("workDescription");
			if (workOrderRequest.getWorkDescription() != null && !workOrderRequest.getWorkDescription().equals(""))
				soapBodyWorkDescriptionElem.addTextNode(workOrderRequest.getWorkDescription());
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPElement soapBodyStatusElem = null;
		try {
			soapBodyStatusElem = soapBodyElem1.addChildElement("status");
			if (workOrderRequest.getStatus() != null && !workOrderRequest.getStatus().equals(""))
				soapBodyStatusElem.addTextNode(workOrderRequest.getStatus().toString());

		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPElement soapBodyPriorityElem = null;
		try {
			soapBodyPriorityElem = soapBodyElem1.addChildElement("priority");
			if (workOrderRequest.getPriority() != null && !workOrderRequest.getPriority().equals(""))
				soapBodyPriorityElem.addTextNode(workOrderRequest.getPriority());
			soapBodyPriorityElem.addTextNode(workOrderRequest.getPriority());
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPElement soapBodyActivityTypeElem = null;
		try {
			soapBodyActivityTypeElem = soapBodyElem1.addChildElement("activityType");
			if (workOrderRequest.getActivityType() != null && !workOrderRequest.getActivityType().equals(""))
				soapBodyActivityTypeElem.addTextNode(workOrderRequest.getActivityType());
			soapBodyActivityTypeElem.addTextNode(workOrderRequest.getActivityType().toString());
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		SOAPElement soapBodyPlannerGrpElem = soapBodyElem1.addChildElement("plannerGroup");
		if (workOrderRequest.getPlannerGroup() != null && !workOrderRequest.getPlannerGroup().equals(""))
			soapBodyPlannerGrpElem.addTextNode(workOrderRequest.getPlannerGroup());
		SOAPElement soapBodySequenceIdElem = soapBodyElem1.addChildElement("sequenceId");
		if (workOrderRequest.getSequenceId() != null && !workOrderRequest.getSequenceId().equals(""))
			soapBodySequenceIdElem.addTextNode(workOrderRequest.getSequenceId());
		SOAPElement soapBodyMainWorkCentreElem = soapBodyElem1.addChildElement("mainWorkCentre");
		if (workOrderRequest.getMainWorkCentre() != null && !workOrderRequest.getMainWorkCentre().equals(""))
			soapBodyMainWorkCentreElem.addTextNode(workOrderRequest.getMainWorkCentre());
		SOAPElement soapBodyAddressElem = soapBodyElem1.addChildElement("address");
		if (workOrderRequest.getAddress() != null && !workOrderRequest.getAddress().equals(""))
			soapBodyAddressElem.addTextNode(workOrderRequest.getAddress());
		SOAPElement soapBodyLocationIdElem = soapBodyElem1.addChildElement("locationId");
		if (workOrderRequest.getLocationId() != null && !workOrderRequest.getLocationId().equals(""))
			soapBodyLocationIdElem.addTextNode(workOrderRequest.getLocationId());
		SOAPElement soapBodyReleaseDateElem = soapBodyElem1.addChildElement("releaseDate");
		if (workOrderRequest.getReleaseDate() != null && !workOrderRequest.getReleaseDate().equals(""))
			soapBodyReleaseDateElem.addTextNode(workOrderRequest.getReleaseDate().toString());
		SOAPElement soapBodyLastUpdatedElem = soapBodyElem1.addChildElement("lastUpdated");
		if (workOrderRequest.getLastUpdated() != null && !workOrderRequest.getLastUpdated().equals(""))
			soapBodyLastUpdatedElem.addTextNode(workOrderRequest.getLastUpdated().toString());
		SOAPElement soapBodyRequiredStartElem = soapBodyElem1.addChildElement("requiredStart");
		if (workOrderRequest.getRequiredStart() != null && !workOrderRequest.getRequiredStart().equals(""))
			soapBodyRequiredStartElem.addTextNode(workOrderRequest.getRequiredStart().toString());
		SOAPElement soapBodyRequiredFinishElem = soapBodyElem1.addChildElement("requiredFinish");
		if (workOrderRequest.getRequiredFinish() != null && !workOrderRequest.getRequiredFinish().equals(""))
			soapBodyRequiredFinishElem.addTextNode(workOrderRequest.getRequiredFinish().toString());
		SOAPElement soapBodyPurchaseOrderIdElem = soapBodyElem1.addChildElement("purchaseOrderId");
		if (workOrderRequest.getPurchaseOrderId() != null && !workOrderRequest.getPurchaseOrderId().equals(""))
			soapBodyPurchaseOrderIdElem.addTextNode(workOrderRequest.getPurchaseOrderId());
		SOAPElement soapBodyPurchaseOrderItemIdElem = soapBodyElem1.addChildElement("purchaseOrderItemId");
		if (workOrderRequest.getPurchaseOrderItemId() != null && !workOrderRequest.getPurchaseOrderItemId().equals(""))
			soapBodyPurchaseOrderItemIdElem.addTextNode(workOrderRequest.getPurchaseOrderItemId());
		SOAPElement soapBodyEstimatedCostsElem = soapBodyElem1.addChildElement("estimatedCosts");
		if (workOrderRequest.getEstimatedCosts() != null && !workOrderRequest.getEstimatedCosts().equals(""))
			soapBodyEstimatedCostsElem.addTextNode(workOrderRequest.getEstimatedCosts().toString().substring(0, 1));
		SOAPElement soapBodyLoggedXCoordinateElem = soapBodyElem1.addChildElement("loggedXCoordinate");
		if (workOrderRequest.getLoggedXCoordinate() != null && !workOrderRequest.getLoggedXCoordinate().equals(""))
			soapBodyLoggedXCoordinateElem.addTextNode(workOrderRequest.getLoggedXCoordinate());
		SOAPElement soapBodyLoggedYCoordinateElem = soapBodyElem1.addChildElement("loggedYCoordinate");
		if (workOrderRequest.getLoggedYCoordinate() != null && !workOrderRequest.getLoggedYCoordinate().equals(""))
			soapBodyLoggedYCoordinateElem.addTextNode(workOrderRequest.getLoggedYCoordinate());
		SOAPElement soapBodySupplierIdElem = soapBodyElem1.addChildElement("supplierId");
		if (workOrderRequest.getSupplierId() != null && !workOrderRequest.getSupplierId().equals(""))
			soapBodySupplierIdElem.addTextNode(workOrderRequest.getLoggedYCoordinate());
		SOAPElement soapBodyTypeOfRequestElem = soapBodyElem1.addChildElement("actionType");
		if (workOrderRequest.getActivityType() != null && !workOrderRequest.getActivityType().equals(""))
			soapBodyTypeOfRequestElem.addTextNode(workOrderRequest.getActivityType());
		SOAPElement soapBodySupplierOrderIdElem = soapBodyElem1.addChildElement("supplierOrderId");
		if (workOrderRequest.getSupplierOrderId() != null && !workOrderRequest.getSupplierOrderId().equals(""))
			soapBodySupplierOrderIdElem.addTextNode(workOrderRequest.getSupplierOrderId());
		SOAPElement soapBodySummaryElem = soapBodyElem1.addChildElement("summary");
		if (workOrderRequest.getSummary() != null) {
			com.service.workorder.Summary summary = workOrderRequest.getSummary();
			if (summary.getFault() != null) {
				SOAPElement soapBodyFaultElem = soapBodySummaryElem.addChildElement("fault");
				SOAPElement soapBodyCodeElem = soapBodyFaultElem.addChildElement("code");
				soapBodyCodeElem.addTextNode(summary.getFault().getCode());
				SOAPElement soapBodyCodeGroupElem = soapBodyFaultElem.addChildElement("codeGroup");
				soapBodyCodeGroupElem.addTextNode(summary.getFault().getCodeGroup());
			}
			if (summary.getObject() != null) {
				SOAPElement soapBodyObjectElem = soapBodySummaryElem.addChildElement("object");
				SOAPElement soapBodyCodeElem = soapBodyObjectElem.addChildElement("code");
				soapBodyCodeElem.addTextNode(summary.getFault().getCode());
				SOAPElement soapBodyCodeGroupElem = soapBodyObjectElem.addChildElement("codeGroup");
				soapBodyCodeGroupElem.addTextNode(summary.getObject().getCodeGroup());
			}
			if (summary.getOutcome() != null) {
				SOAPElement soapBodyObjectElem = soapBodySummaryElem.addChildElement("outcome");
				SOAPElement soapBodyCodeElem = soapBodyObjectElem.addChildElement("code");
				soapBodyCodeElem.addTextNode(summary.getOutcome().getCode());
				SOAPElement soapBodyCodeGroupElem = soapBodyObjectElem.addChildElement("codeGroup");
				soapBodyCodeGroupElem.addTextNode(summary.getOutcome().getCodeGroup());
			}
			if (summary.getProblem() != null) {
				SOAPElement soapBodyObjectElem = soapBodySummaryElem.addChildElement("problem");
				SOAPElement soapBodyCodeElem = soapBodyObjectElem.addChildElement("code");
				soapBodyCodeElem.addTextNode(summary.getProblem().getCode());
				SOAPElement soapBodyCodeGroupElem = soapBodyObjectElem.addChildElement("codeGroup");
				soapBodyCodeGroupElem.addTextNode(summary.getProblem().getCodeGroup());
			}
		}
		if (workOrderRequest.getOperation() != null) {
			List<com.service.workorder.Operation> operation = workOrderRequest.getOperation();
			if (operation != null)
				for (int i = 0; i < operation.size(); i++) {
					SOAPElement soapBodyOperationElem = soapBodyElem1.addChildElement("operation");
					SOAPElement soapBodyOperationIdElem = soapBodyOperationElem.addChildElement("operationId");
					soapBodyOperationIdElem.addTextNode(operation.get(i).getOperationId());
					SOAPElement soapBodyWorkCentreElem = soapBodyOperationElem.addChildElement("workCentre");
					soapBodyWorkCentreElem.addTextNode(operation.get(i).getWorkCentre());
					SOAPElement soapBodysStatusElem = soapBodyOperationElem.addChildElement("status");
					soapBodysStatusElem.addTextNode(operation.get(i).getStatus());
					SOAPElement soapBodyDescriptionElem = soapBodyOperationElem.addChildElement("description");
					soapBodyDescriptionElem.addTextNode(operation.get(i).getDescription());
					SOAPElement soapBodyRequiredsStartElem = soapBodyOperationElem.addChildElement("requiredStart");
					soapBodyRequiredsStartElem.addTextNode(operation.get(i).getRequiredStart().toString());
					SOAPElement soapBodyRequiredsFinishElem = soapBodyOperationElem.addChildElement("requiredFinish");
					soapBodyRequiredsFinishElem.addTextNode(operation.get(i).getRequiredFinish().toString());
					SOAPElement soapBodylLocationIdElem = soapBodyOperationElem.addChildElement("locationId");
					if (operation.get(i).getLocationId() != null)
						soapBodylLocationIdElem.addTextNode(operation.get(i).getLocationId());
					SOAPElement soapBodyOperationSummaryElem = soapBodyOperationElem.addChildElement("summary");
					if (operation.get(i).getSummary() != null) {
						com.service.workorder.Summary summary = operation.get(i).getSummary();
						if (summary.getFault() != null) {
							SOAPElement soapBodyFaultElem = soapBodyOperationSummaryElem.addChildElement("fault");
							SOAPElement soapBodyCodeElem = soapBodyFaultElem.addChildElement("code");
							soapBodyCodeElem.addTextNode(summary.getFault().getCode());
							SOAPElement soapBodyCodeGroupElem = soapBodyFaultElem.addChildElement("codeGroup");
							soapBodyCodeGroupElem.addTextNode(summary.getFault().getCodeGroup());
						}
						if (summary.getObject() != null) {
							SOAPElement soapBodyObjectElem = soapBodyOperationSummaryElem.addChildElement("object");
							SOAPElement soapBodyCodeElem = soapBodyObjectElem.addChildElement("code");
							soapBodyCodeElem.addTextNode(summary.getFault().getCode());
							SOAPElement soapBodyCodeGroupElem = soapBodyObjectElem.addChildElement("codeGroup");
							soapBodyCodeGroupElem.addTextNode(summary.getObject().getCodeGroup());
						}
						if (summary.getProblem() != null) {
							SOAPElement soapBodyObjectElem = soapBodyOperationSummaryElem.addChildElement("problem");
							SOAPElement soapBodyCodeElem = soapBodyObjectElem.addChildElement("code");
							soapBodyCodeElem.addTextNode(summary.getProblem().getCode());
							SOAPElement soapBodyCodeGroupElem = soapBodyObjectElem.addChildElement("codeGroup");
							soapBodyCodeGroupElem.addTextNode(summary.getProblem().getCodeGroup());
						}
					}
					if (workOrderRequest.getOperation() != null) {
						List<com.service.workorder.Operation.Service> service = operation.get(i).getService();
						for (int j = 0; j < service.size(); j++) {
							SOAPElement soapBodyServiceElem = soapBodyOperationElem.addChildElement("service");
							SOAPElement soapBodyServiceIdElem = soapBodyServiceElem.addChildElement("serviceId");
							if (service.get(j).getServiceId() != null)
								soapBodyServiceIdElem.addTextNode(service.get(j).getServiceId());
							SOAPElement soapBodyQtyServiceElem = soapBodyServiceElem.addChildElement("serviceQuantity");
							if (service.get(j).getServiceQuantity() != null)
								soapBodyQtyServiceElem.addTextNode(service.get(j).getServiceQuantity());
							SOAPElement soapBodyUomElem = soapBodyServiceElem.addChildElement("uom");
							if (service.get(j).getUom() != null)
								soapBodyUomElem.addTextNode(service.get(j).getUom());
							SOAPElement soapBodyPlannedCostElem = soapBodyServiceElem.addChildElement("plannedCost");
							if (service.get(j).getPlannedCost() != null)
								soapBodyPlannedCostElem.addTextNode(service.get(j).getPlannedCost());
							SOAPElement soapBodyEstCostDetailElem = soapBodyServiceElem
									.addChildElement("estCostDetail");
							if (service.get(j).getEstimatedCostDetail() != null) {
								EstimatedCostDetail estimateCostDetails = service.get(j).getEstimatedCostDetail();
								if (estimateCostDetails != null) {
									SOAPElement soapBodyEstimateElem = soapBodyEstCostDetailElem
											.addChildElement("equipment");
									if (estimateCostDetails.getEquipment() != null)
										soapBodyEstimateElem.addTextNode(service.get(j).getPlannedCost());
									SOAPElement soapBodyLaborElem = soapBodyEstCostDetailElem.addChildElement("labour");
									if (estimateCostDetails.getLabour() != null)
										soapBodyLaborElem.addTextNode(estimateCostDetails.getLabour().toString());
									SOAPElement soapBodyMaterialElem = soapBodyEstCostDetailElem
											.addChildElement("material");
									if (estimateCostDetails.getLabour() != null)
										soapBodyMaterialElem.addTextNode(estimateCostDetails.getMaterial());
								}
							}
						}
					}

				}
		}
		if (workOrderRequest.getActivity() != null) {
			List<Activity> activity = workOrderRequest.getActivity();
			if (activity != null)
				for (int k = 0; k < activity.size(); k++) {
					SOAPElement soapBodyActivityElem = soapBodyElem1.addChildElement("activity");
					SOAPElement soapBodyActivityDescElem = soapBodyActivityElem.addChildElement("description");
					if (activity.get(k).getDescription() != null)
						soapBodyActivityDescElem.addTextNode(activity.get(k).getDescription());
					SOAPElement soapBodyActivityStartDateElem = soapBodyActivityElem.addChildElement("startDate");
					if (activity.get(k).getStartDate() != null)
						soapBodyActivityStartDateElem.addTextNode(activity.get(k).getStartDate().toString());
					if (activity.get(k).getCode() != null) {
						SOAPElement soapBodyObjectElem = soapBodyActivityElem.addChildElement("code");
						SOAPElement soapBodyCodeElem = soapBodyObjectElem.addChildElement("code");
						soapBodyCodeElem.addTextNode(activity.get(k).getCode().getCode().toString());
						SOAPElement soapBodyCodeGroupElem = soapBodyObjectElem.addChildElement("codeGroup");
						soapBodyCodeGroupElem.addTextNode(activity.get(k).getCode().getCodeGroup().toString());
					}
				}
		}
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("Content-Type", System.getProperty("contenttype"));
		headers.addHeader("SOAPAction", System.getProperty("soapaction"));
		soapMessage.saveChanges();
		final StringWriter payLoad = new StringWriter();
		TransformerFactory.newInstance().newTransformer().transform(new DOMSource(soapMessage.getSOAPPart()),
				new StreamResult(payLoad));
		new DbUtil().insertInMessageResponseTable(esbId, workOrderRequest.getOrderId(), "SUCCESS", payLoad.toString(),
				"Create", "Order", "ESB", muleContext);
		logger.info(new UMSTimeDateLog().getTimeDateLog() +" Original SOAP Payload Message sent to Council  and inserted into the DB \t" + payLoad.toString());
		return soapMessage;
	}

}
