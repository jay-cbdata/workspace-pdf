package com.ums.b2b.esb.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.entity.WorkOrderRequest;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

public class RestSaveForDetailWithCF {
	protected final static Log logger = LogFactory.getLog(RestSaveForDetailWithCF.class);
	String response = null;

	public String saveForDetailWithCF(WorkOrderRequest workOrderRequest) throws Exception {

		String keyedWorkOrderNumber = (String) workOrderRequest.getReadForDetailJson().get("KeyedWorkOrderNumber");
		String workSystemName = (String) workOrderRequest.getWorkSystemName();
		String availableForBudgeting = String
				.valueOf(workOrderRequest.getReadForDetailJson().get("AvailableForBudgeting"));
		String workOrderNumber = (String) workOrderRequest.getReadForDetailJson().get("WorkOrderNumber");
		String workTypeCode = (String) workOrderRequest.getReadForDetailJson().get("WorkTypeCode");
		String isTemplate = String.valueOf(workOrderRequest.getReadForDetailJson().get("IsTemplate"));
		String vers = String.valueOf(workOrderRequest.getReadForDetailJson().get("Vers"));
		String description = (String) workOrderRequest.getReadForDetailJson().get("Description");
		String details = (String) workOrderRequest.getReadForDetailJson().get("Details");
		String priorityCode = (String) workOrderRequest.getReadForDetailJson().get("PriorityCode");
		String type = (String) workOrderRequest.getReadForDetailJson().get("Type");
		String source = (String) workOrderRequest.getReadForDetailJson().get("Source");
		String status = (String) workOrderRequest.getReadForDetailJson().get("Status");
		String approvalStatus = (String) workOrderRequest.getReadForDetailJson().get("ApprovalStatus");
		String stage = (String) workOrderRequest.getReadForDetailJson().get("Stage");
		String responsiblePerson = (String) workOrderRequest.getReadForDetailJson().get("ResponsiblePerson");
		String projectCode = (String) workOrderRequest.getReadForDetailJson().get("ProjectCode");
		String activityCode = (String) workOrderRequest.getReadForDetailJson().get("ActivityCode");
		String headerWorkOrderNumberInternal = (String) workOrderRequest.getReadForDetailJson()
				.get("HeaderWorkOrderNumberInternal");
		String headerWorkOrderNumber = (String) workOrderRequest.getReadForDetailJson().get("HeaderWorkOrderNumber");
		String isBudgetWorkOrder = String.valueOf(workOrderRequest.getReadForDetailJson().get("IsBudgetWorkOrder"));
		String levelType = (String) workOrderRequest.getReadForDetailJson().get("LevelType");
		String rquiredByDate = (String) workOrderRequest.getReadForDetailJson().get("RequiredByDate");
		String requiredByTime = (String) workOrderRequest.getReadForDetailJson().get("RequiredByTime");
		String requiredByDateTime = (String) workOrderRequest.getReadForDetailJson().get("RequiredByDateTime");
		String estimatedStartDate = (String) workOrderRequest.getReadForDetailJson().get("EstimatedStartDate");
		if (estimatedStartDate == "" || estimatedStartDate == null) {
			SimpleDateFormat st = new SimpleDateFormat("yyyy-MM-dd");
			st.setTimeZone(TimeZone.getTimeZone("GMT"));
			estimatedStartDate = new String(st.format(new Date())).toString();
		}
		String estimatedStartTime = (String) workOrderRequest.getReadForDetailJson().get("EstimatedStartTime");
		String estimatedStartDateTime = (String) workOrderRequest.getReadForDetailJson().get("EstimatedStartDateTime");
		if (estimatedStartDateTime == "" || estimatedStartDateTime == null) {
			SimpleDateFormat st = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			st.setTimeZone(TimeZone.getTimeZone("GMT"));
			estimatedStartDateTime = new String(st.format(new Date())).toString();
		}
		String estimatedFinishDate = (String) workOrderRequest.getReadForDetailJson().get("EstimatedFinishDate");
		String estimatedFinishTime = (String) workOrderRequest.getReadForDetailJson().get("EstimatedFinishTime");
		String estimatedFinishDateTime = (String) workOrderRequest.getReadForDetailJson()
				.get("EstimatedFinishDateTime");
		String actualStartTime = (String) workOrderRequest.getReadForDetailJson().get("ActualStartTime");
		String actualFinishTime = (String) workOrderRequest.getReadForDetailJson().get("ActualFinishTime");
		String actualStartDateTime = (String) workOrderRequest.getReadForDetailJson().get("ActualStartDateTime");
		String actualFinishDate = (String) workOrderRequest.getReadForDetailJson().get("ActualFinishDate");
		String percentComplete = String.valueOf(workOrderRequest.getReadForDetailJson().get("PercentComplete"));
		String expectedDuration = String.valueOf(workOrderRequest.getReadForDetailJson().get("ExpectedDuration"));
		String billingType = (String) workOrderRequest.getReadForDetailJson().get("BillingType");
		String billingMethod = (String) workOrderRequest.getReadForDetailJson().get("BillingMethod");
		String billingLedgerCode = (String) workOrderRequest.getReadForDetailJson().get("BillingLedgerCode");
		String billingAccountNumberInternal = (String) workOrderRequest.getReadForDetailJson()
				.get("BillingAccountNumberInternal");
		String workOrderUserfield13 = (String) workOrderRequest.getReadForDetailJson().get("WorkOrderUserfield13");
		String workOrderUserfield8 = (String) workOrderRequest.getReadForDetailJson().get("WorkOrderUserfield8");
		String workOrderUserfield9 = (String) workOrderRequest.getReadForDetailJson().get("WorkOrderUserfield9");
		String workOrderUserfield11 = (String) workOrderRequest.getReadForDetailJson().get("WorkOrderUserfield11");
		String workOrderUserfield14 = (String) workOrderRequest.getReadForDetailJson().get("WorkOrderUserfield14");
		String workCodeUserField6 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField6");
		String workCodeUserField7 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField7");
		String workCodeUserField8 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField8");
		String workCodeUserField9 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField9");
		String workCodeUserField1 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField1");
		String workCodeUserField2 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField2");
		String workCodeUserField3 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField3");
		String workCodeUserField4 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField4");
		String workCodeUserField5 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField5");
		String workCodeUserField10 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField10");
		String workCodeUserField18 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField18");
		String workCodeUserField12 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField12");
		String workCodeUserField15 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField15");
		String workCodeUserField16 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField16");
		String workCodeUserField17 = (String) workOrderRequest.getReadForDetailJson().get("WorkCodeUserField17");

		try {
			Client client = Client.create();
			WebResource webResource = client.resource(System.getProperty("techonesavefordetailurl"));
			String input = "{\"AvailableForBudgeting\":\"" + availableForBudgeting + "\"," + "\"WorkSystemName\":\""
					+ workSystemName + "\","
					// + "\"KeyedWorkOrderNumber\":\"" + keyedWorkOrderNumber+
					// "\","
					+ "\"KeyedWorkOrderNumber\":\"000138\"," + "\"WorkOrderNumber\":\"" + workOrderNumber + "\","
					+ "\"IsTemplate\":\"" + isTemplate + "\"," + "\"Vers\":\"" + vers + "\"," + "\"Description\":\""
					+ description + "\"," + "\"Details\":\"" + details + "\"," + "\"PriorityCode\":\"" + priorityCode
					+ "\"," + "\"WorkTypeCode\":\"" + workTypeCode + "\"," + "\"Type\":\"" + type + "\","
					+ "\"Source\":\"" + source + "\"," + "\"Status\":\"" + status + "\"," + "\"ResponsiblePerson\":\""
					+ responsiblePerson + "\"," + "\"ProjectCode\":\"" + projectCode + "\"," + "\"ActivityCode\":\""
					+ activityCode + "\"," + "\"HeaderWorkOrderNumberInternal\":\"" + headerWorkOrderNumberInternal
					+ "\"," + "\"HeaderWorkOrderNumber\":\"" + headerWorkOrderNumber + "\","
					+ "\"IsBudgetWorkOrder\":\"" + isBudgetWorkOrder + "\"," + "\"EstimatedStartDate\":\""
					+ estimatedStartDate + "\"," + "\"EstimatedStartTime\":\"" + estimatedStartTime + "\","
					+ "\"ActualStartTime\":\"" + actualStartTime + "\"," + "\"ActualFinishTime\":\"" + actualFinishTime
					+ "\"," + "\"EstimatedStartDateTime\":\"" + estimatedStartDateTime + "\","
					+ "\"PercentComplete\":\"" + percentComplete + "\"," + "\"ApprovalStatus\":\"X\"," + "\"Stage\":\""
					+ stage + "\"," + "\"BillingMethod\":\"" + billingMethod + "\"," + "\"BillingType\":\""
					+ billingType + "\"," + "\"BillingLedgerCode\":\"" + billingLedgerCode + "\","
					+ "\"BillingAccountNumberInternal\":\"" + billingAccountNumberInternal + "\","
					+ "\"WorkCodeUserField1\":\"" + workCodeUserField1 + "\"," + "\"WorkCodeUserField2\":\""
					+ workCodeUserField2 + "\"," + "\"WorkCodeUserField3\":\"" + workCodeUserField3 + "\","
					+ "\"WorkCodeUserField4\":\"" + workCodeUserField4 + "\"," + "\"WorkCodeUserField5\":\""
					+ workCodeUserField5 + "\"," + "\"WorkCodeUserField17\":\"" + workCodeUserField17 + "\","
					+ "\"WorkCodeUserField12\":\"" + workCodeUserField12 + "\"," + "\"WorkCodeUserField16\":\""
					+ workCodeUserField16 + "\"," + "\"WorkCodeUserField15\":\"" + workCodeUserField15 + "\","
					+ "\"WorkCodeUserField18\":\"" + workCodeUserField18 + "\"," + "\"WorkCodeUserField10\":\""
					+ workCodeUserField10 + "\"," + "\"WorkOrderUserfield14\":\"" + workOrderUserfield14 + "\","
					+ "\"WorkCodeUserField6\":\"" + workCodeUserField6 + "\"," + "\"WorkCodeUserField7\":\""
					+ workCodeUserField7 + "\"," + "\"WorkCodeUserField8\":\"" + workCodeUserField8 + "\","
					+ "\"WorkCodeUserField9\":\"" + workCodeUserField9 + "\"}";

			ClientResponse responseClient = webResource.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, input);
			response = responseClient.getEntity(String.class);
			workOrderRequest.setWorkSystemName(workSystemName);
			workOrderRequest.setWorkOrderNumber(keyedWorkOrderNumber);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Input to Techone for Saving the Details via UMSESB\t" + input);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response from TechOne end" + response);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

}
