package com.ums.b2b.esb.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mule.api.MuleContext;
import org.mule.api.MuleMessage;
import org.mule.api.annotations.expressions.Lookup;
import org.mule.api.context.MuleContextAware;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.service.workorder.InvalidMessageFault_Exception;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.ums.b2b.esb.utility.Constant;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.TechOneCommon;

/**
 * @author Sankalp
 *
 */
public class PdfProcessor extends AbstractMessageTransformer implements MuleContextAware {

	protected final static Log logger = LogFactory.getLog(PdfProcessor.class);
	public String fileName = null;
	String destination = System.getProperty("tempxmlfolder");
	String xmlExtension = System.getProperty("fileextension");
	String functionalLocation;
	String[] parts;
	String risks;
	String riskCode;
	String cause;
	String causeCode;
	String warrantyCode;

	@Lookup
	private MuleContext muleContext;
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.mule.transformer.AbstractMessageTransformer#transformMessage(org.mule
	 * .api.MuleMessage, java.lang.String)
	 */

	@SuppressWarnings("unused")
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		String output = null;
		PdfReader reader = null;
		String sPdf = "";
		String xmlFilePath = null;
		String esbId = org.mule.util.UUID.getUUID();
		//ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		String workOrderNo, responseStatus, fileName = null, input, activityCode = null;
		JSONObject jsonObject, subWorkOrder, jsonObjectRead, jsonObjectApprove, jsonSubWorkOrderObject = null;
		JSONParser parser = new JSONParser();
		InputStream inStream = (InputStream) message.getPayload();
		try {
			reader = new PdfReader(inStream);
			File folder = new File(System.getProperty("input.folder"));
			File[] listOfFiles = folder.listFiles();
			if (listOfFiles != null) {
				for (File file : listOfFiles) {
					fileName = file.getName();
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		PdfDocument pdfDoc = new PdfDocument(reader);
		PdfAcroForm form = PdfAcroForm.getAcroForm(pdfDoc, true);
		com.itextpdf.forms.xfa.XfaForm xfaForm = form.getXfaForm();
		Node node = xfaForm.getDatasetsNode();
		Document dom = xfaForm.getDomDocument();
		
		dom.getDocumentElement().normalize();
		NodeList workOrderNode = dom.getElementsByTagName("IS_WORK_ORDER");
		
		logger.info(workOrderNode);
		
		
		for(int i=0;i<workOrderNode.getLength();i++) {
//			logger.info(workOrderNode.item(i).getNodeName() + "\t" + workOrderNode.item(i).getNodeValue() +
//					"\t" + workOrderNode.item(i).getTextContent() + "\r\n");
		}
		
//		logger.info("Child Count:\t"+workOrderNode.item(0).getChildNodes().getLength());
//		for(int i=0;i<workOrderNode.item(0).getChildNodes().getLength();i++){
//			logger.info(workOrderNode.item(0).getChildNodes().item(i).getNodeName() + "\t:" + 
//					workOrderNode.item(0).getChildNodes().item(i).getTextContent());
//		
//		}
	
		 
		Node WorkOrderNode = dom.getElementsByTagName("IS_WORK_ORDER").item(0);
		NodeList operations = ((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("PO_NUMBER_TEXT");
		logger.info("operations\t" + operations.item(0).getTextContent() );
		logger.info("-------------");
		
		NodeList isWorkOrderNodes = workOrderNode.item(0).getChildNodes();
		String purchaseOrderNumber = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("CONTRACT_PO"));
	    logger.info("Contract P.O.: \t" + purchaseOrderNumber);
		String supplier = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("VENDOR_NO"));
		//logger.info("Supplier: \t" + supplier);
		workOrderNo = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("WO_NUMBER"));
		logger.info("Work Order Number: \t" + workOrderNo);
		String responseFromPDF = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("AUARTTEXT"));
		//logger.info("Type: \t" + responseFromPDF);
		String orderType = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("AUART"));
		//logger.info("OrderType: \t" + orderType);
		String workTypeCode = null;
		try {
			workTypeCode = new TechOneCommon().getWorkTypeCode(orderType);
			//logger.info("workTypeCode \t" + workTypeCode);
		} catch (UnprocessedMessageFault_Exception e1) {
			e1.printStackTrace();
		}
		String priority = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("PRIOKX"));
		//logger.info("Priority: \t" + priority);
		
		String workOrderName = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("WO_NAME"));
		//logger.info("Work Order Name \t" + workOrderName);
		
		String address = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("ADDRESS_TEXT"));
		//logger.info("ADDRESS \t" + address);
		
//		String equipment = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("GV_ASSET_ID"));
//		String[] valArray = equipment.split("\\|");
//		String[] equipmentArray = valArray[1].toString().split("\\-");
		
//		functionalLocation = equipmentArray[0].toString();
//		
//		if (equipmentArray.length == 3) {
//			functionalLocation = functionalLocation.concat("-").concat(equipmentArray[1].toString());
//		functionalLocation = functionalLocation.concat("-").concat(equipmentArray[2].toString());
//		}
//		
//		if (equipmentArray.length == 2){
//			functionalLocation = functionalLocation.concat("-").concat(equipmentArray[1].toString());
//		}
//		
//		functionalLocation = functionalLocation.replace(".","");
		
		functionalLocation = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("FUNCT_LOC"));
		
		logger.info("Functional Location  \t" + functionalLocation);
		
		String locationId = functionalLocation.trim().substring(0, 5);
		logger.info("Location Id  \t" + locationId);
		
		String site_desc = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("SITE_DESC"));
		logger.info("Site Description \t" + site_desc);
		
		String legacyCode = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("LEGACY_CODE"));
		logger.info("LEGACY CODE \t" + legacyCode);
		
		String problem_desc = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("PROBLEM_DESC"));
		logger.info("Problem Decription \t" + problem_desc);
		
		String desc_of_work ="";// getXMLElementValue(dom.getElementsByTagName("DESC_OF_WORK"));
		//logger.info("Decription of Work \t" + desc_of_work);
		
		String act = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("PM_ACTIVITY"));
		logger.info("Activity \t" + act);
		logger.info("Order Type\t" + orderType);
		String activity = null;
		try {
			activity = new TechOneCommon().getMapppedActivityType(act, orderType);
		
			//logger.info("Activity   \t" + activity);
		} catch (UnprocessedMessageFault_Exception e1) {
			e1.printStackTrace();
			try {
				throw e1;
			} catch (UnprocessedMessageFault_Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String risk = getXMLElementValue(dom.getElementsByTagName("GV_RISK"));
		logger.info("Risk \t" + risk);
		//logger.info("Risk is as per pdf  \t" + risk);
		try {
			risks = new TechOneCommon().getRiskCode(risk);
			//logger.info("Risk is as per pdf 2   \t" + risks);
			riskCode = new TechOneCommon().getRiskCodeGroup(risks);
			//logger.info("Risk is as per pdf 3    \t" + riskCode);
		} catch (UnprocessedMessageFault_Exception e1) {
			e1.printStackTrace();
			try {
				throw e1;
			} catch (UnprocessedMessageFault_Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String specialContidion = getXMLElementValue(dom.getElementsByTagName("GV_SP_COND"));
		logger.info("Special Condition \t" + specialContidion);
		String causes = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("CAUSE"));
		cause ="";//new TechOneCommon().getCause(causes);
		causeCode ="";// new TechOneCommon().getCausePdf(cause);
		//logger.info("Cause \t" + cause);
		String warranty = getXMLElementValue(dom.getElementsByTagName("GV_WARRANTY"));
		if (warranty.equals("YES"))
			warrantyCode = "ZPM-WARR";
		else
			warrantyCode = "";
		////logger.info("Warranty \t" + warranty);
		String release_Date = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("RELEASED_DATE"));
		logger.info("RELEASED_DATE \t" + release_Date);
		String slaDate = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("SLA_DATE"));
		String slaTime = getXMLElementValue(((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("SLA_TIME"));
		logger.info("Respond By Date/time \t" + slaDate + "\t" + slaTime);
		String startDate = "";
		String startTime = "";
		//logger.info("Start Date\time  \t" + startDate + "\t" + startTime);
		String completedDate ="";
		String completedTime = "";
		//logger.info("Completed Date\time  \t" + completedDate + "\t" + completedTime);
		String statusis ="";
		String status = "";
		String plannerGroup = "";
		String serviceId = "";
		//logger.info("Status \t" + status);
		NodeList providedList = ((org.w3c.dom.Element)WorkOrderNode).getElementsByTagName("T_SERVICE");
		
		logger.info("T_SERVICE \t" + providedList);
		
		String provideList = providedList.item(0).getTextContent();
		logger.info("Services Provided \t" + provideList);
		parts = provideList.split("-");
		for (int i = 0; i < parts.length; i++) {
			logger.info("Service \t" + parts[i]);
			if (parts[i].startsWith("3000")){
				serviceId = parts[i].trim();
				break;
			}
			else if (parts[i].contains("3000")){
				serviceId = parts[i].substring(parts[i].indexOf("3000"), parts[i].indexOf("3000") + 7);
				break;
			}
		}
		logger.info("Service Id \t'" + serviceId);
		//desc_of_work = getXMLElementValue(dom.getElementsByTagName("DESC_OF_WORK"));
		//logger.info("Description Of Work \t" + desc_of_work);
		
		Transformer tf = null;
		try {
			tf = TransformerFactory.newInstance().newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
		tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		tf.setOutputProperty(OutputKeys.INDENT, "yes");
		StringWriter writer = new StringWriter();
		try {
			try {
				tf.transform(new DOMSource(node), new StreamResult(writer));
			} catch (javax.xml.transform.TransformerException e) {
				e.printStackTrace();
			}
			output = writer.getBuffer().toString();
			writer.flush();
			writer.close();
				//logger.info("::::::::::::::::::: PDF DATA INSERTED SUCCESSFULLY ::::::::::::::::::::::");
			FileOutputStream os = null;
			os = new FileOutputStream(destination + "\\" + fileName.replace(".pdf", xmlExtension));
			os.write(output.getBytes());
			os.flush();
			os.close();
			
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
			logger.info("::::::::::::::::::: PDF DATA INSERT FAILED ::::::::::::::::::::::");
			new DbUtil().insertIntoMessageTable(esbId, Constant.EXTERNAL_REF_PDF, "WorkOrder", Constant.CREATE, output,  workOrderNo,"INVALID",
					muleContext);
		}
		
		try {
			inStream.close();
			pdfDoc.close();
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//logger.info("PDF XML \t" + output);

		try {
			String validOrderExists = new DbUtil().getPdfDataToUpdateMain(workOrderNo, muleContext);
     logger.info("valid record exists?: " + validOrderExists);
     
			if (validOrderExists=="no") {
			JSONObject jsonObjectm = new com.ums.b2b.esb.service.umsesb.SoapCreateFromTemplate()
					.createFromTemplate( esbId, Constant.EXTERNAL_REF_PDF, Constant.LEVEL_TYPE_HEADER_WORK_ORDER,
							workOrderNo, workTypeCode, site_desc, activity,
							workOrderName, locationId, responseFromPDF,
							"",
							muleContext);
			
			JSONObject readHeaderJson = new com.ums.b2b.esb.service.umsesb.SoapReadFromApi().readFromApiHeaderWorkOrder(esbId, Constant.EXTERNAL_REF_PDF,
					jsonObjectm, workOrderNo, muleContext);
			
			if (purchaseOrderNumber.split(" ").length > 0){
				purchaseOrderNumber = purchaseOrderNumber.split(" ")[0];
			}
			
			String headerWorkOrderNumber = readHeaderJson.get("HeaderWorkOrderNumber").toString();
			jsonObject = new com.ums.b2b.esb.service.umsesb.SoapSaveForDetail().SaveWorkOrder(
					
					readHeaderJson, Constant.EXTERNAL_REF_PDF, purchaseOrderNumber, 
					workOrderNo, esbId, warranty, warrantyCode,
					risks, riskCode, cause, causeCode, 
					functionalLocation, "",  address, 
					headerWorkOrderNumber,
					priority, workOrderName,site_desc, release_Date,status, plannerGroup,"","","",
					"",
					startDate,
					startTime, "","","","","","","",completedDate, 
					completedTime, "","","",serviceId,					
					muleContext);
			
			/*
			 * startDate,
					startTime, 
					
					workOrderName,
			 */
			
			jsonObject = new com.ums.b2b.esb.service.umsesb.SoapApproveHeaderWorkOrder().approveHeaderWorkOrder(esbId,Constant.EXTERNAL_REF_PDF,
					jsonObject, workOrderNo, muleContext);
			
			subWorkOrder = new com.ums.b2b.esb.service.umsesb.SoapCreateFromTemplate()
					.createFromTemplate(esbId, Constant.EXTERNAL_REF_PDF, 
							Constant.LEVEL_TYPE_SUB_WORK_ORDER,  
							workOrderNo, workTypeCode,  
							site_desc,activity,
							workOrderName,
							locationId, 
							responseFromPDF,							
							headerWorkOrderNumber, muleContext);
			
			subWorkOrder = new com.ums.b2b.esb.service.umsesb.SoapReadFromApiSubWorkOrder()
					.readFromApiSubWorkOrder(esbId, Constant.EXTERNAL_REF_PDF, subWorkOrder, workOrderNo,headerWorkOrderNumber, muleContext);
			
			subWorkOrder = new com.ums.b2b.esb.service.umsesb.SoapSaveForDetail()					
					.SaveWorkOrder(subWorkOrder, Constant.EXTERNAL_REF_PDF,
							purchaseOrderNumber, workOrderNo, esbId, warranty, warrantyCode,risks, riskCode, 
							cause=="N/A"?"":cause,causeCode, functionalLocation,"",
							address,headerWorkOrderNumber,priority,site_desc,problem_desc,"",status, 
							plannerGroup, "","","",
							"",
							startDate,
							startTime, "","","","","","","",completedDate, 
							completedTime, "","","",serviceId, muleContext);
			
			subWorkOrder = new com.ums.b2b.esb.service.umsesb.SoapApproveSubWorkOrder().approveSubWorkOrder(esbId, Constant.EXTERNAL_REF_PDF,
					subWorkOrder, workOrderNo,headerWorkOrderNumber, muleContext);

			//for (int i = 1; i < parts.length; i++) {
				updateServiceIds(provideList, headerWorkOrderNumber);
			//}
			updateServiceIds(specialContidion,slaDate,slaTime,legacyCode, headerWorkOrderNumber);
			
			new DbUtil().insertIntoMessageTable(esbId, Constant.EXTERNAL_REF_PDF, "WorkOrder", Constant.CREATE, output, workOrderNo, "VALID",
					muleContext);
			}
			else{
				logger.info("PDF Order Id already exists in the Database \t");
			}
			/*
			 * getUploadDefaultAttachment(); uploadPDFfileAttachement(fileName,
			 * buffer.toByteArray());
			 */
		} catch (UnprocessedMessageFault_Exception r) {
			new DbUtil().insertIntoMessageTable(esbId, Constant.EXTERNAL_REF_PDF, "WorkOrder", Constant.CREATE, output, workOrderNo, "INVALID",
					muleContext);
			r.printStackTrace();
		} catch (InvalidMessageFault_Exception r) {
			new DbUtil().insertIntoMessageTable(esbId, Constant.EXTERNAL_REF_PDF, "WorkOrder", Constant.CREATE, output, workOrderNo, "INVALID",
					muleContext);
			r.printStackTrace();
		} catch (Exception e) {
			new DbUtil().insertIntoMessageTable(esbId, Constant.EXTERNAL_REF_PDF, "WorkOrder", Constant.CREATE, output, workOrderNo, "INVALID",
					muleContext);
			e.printStackTrace();
		}
		return output;
	}
	/**
	 * @param specialContidion
	 * @param slaDate
	 * @param slaTime
	 * @param headerWorkOrderNumber
	 * @return String
	 */
	public String updateServiceIds(String specialContidion,String slaDate,String slaTime, String legacyCode, String headerWorkOrderNumber) {
		String strMsg = null;
		SOAPConnectionFactory soapConnectionFactory;
		try {

			soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			SOAPMessage soapResponse = null;
			try {
				soapResponse = soapConnection.call(updateServiceId(specialContidion,slaDate,slaTime,legacyCode, headerWorkOrderNumber),
						System.getProperty("techoneserviceupdatedefaulturl"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				soapResponse.writeTo(out);
			} catch (IOException e) {
				e.printStackTrace();
			}
			strMsg = new String(out.toByteArray());
			logger.info("SOAP Response from  updateServiceId For NotesData   \t ");
			soapResponse.writeTo(System.out);
			out.close();
			soapConnection.close();
		} catch (Exception r) {
			r.printStackTrace();
		}
		return strMsg;
	}

	/**
	 * @param serviceId
	 * @param headerWorkOrderNumber
	 * @return
	 */
	public String updateServiceIds(String serviceId, String headerWorkOrderNumber) {
		String strMsg = null;
		SOAPConnectionFactory soapConnectionFactory;
		try {

			soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			SOAPMessage soapResponse = null;
			try {
				soapResponse = soapConnection.call(updateServiceId(serviceId, headerWorkOrderNumber),
						System.getProperty("techoneserviceupdatedefaulturl"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				soapResponse.writeTo(out);
			} catch (IOException e) {
				e.printStackTrace();
			}
			strMsg = new String(out.toByteArray());
			logger.info("SOAP Response from  updateServiceIds  \t ");
			soapResponse.writeTo(System.out);
			out.close();
			soapConnection.close();
		} catch (Exception r) {
			r.printStackTrace();
		}
		return strMsg;
	}

	/**
	 * @param workOrderNo
	 * @param encoded
	 * @return String
	 */
	public String uploadPDFfileAttachement(String workOrderNo, byte[] encoded) {
		String strMsg = null;
		SOAPConnectionFactory soapConnectionFactory;
		try {
			soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			SOAPMessage soapResponse = null;
			try {

				soapResponse = soapConnection.call(uploadSOAPPDFRequest(workOrderNo, encoded),
						System.getProperty("techoneattachmentdefaulturl"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				soapResponse.writeTo(out);
			} catch (IOException e) {
				e.printStackTrace();
			}
			strMsg = new String(out.toByteArray());
			logger.info("SOAP Response from  uploadPDFfileAttachement  \t ");
			soapResponse.writeTo(System.out);
			out.close();
			soapConnection.close();
		} catch (Exception r) {
			r.printStackTrace();
		}
		return strMsg;
	}

	/**
	 * @param workOrderNo
	 * @param encoded
	 * @return SOAPMessage
	 * @throws Exception
	 */
	private static SOAPMessage uploadSOAPPDFRequest(String workOrderNo, byte[] encoded) throws Exception {
		SOAPMessage soapMessage = null;
		MessageFactory messageFactory = MessageFactory.newInstance();
		soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String serverURI = "http://TechnologyOneCorp.com/Public/Services";
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("ser", serverURI);
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("Attachment_UploadAttachment", "ser");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("Request", "ser");
		soapBodyElem1.setAttribute("UploadId", "");
		soapBodyElem1.setAttribute("Filename", workOrderNo + ".pdf");
		soapBodyElem1.setAttribute("FragmentData", new String(encoded));
		soapBodyElem1.setAttribute("Tag", "");
		soapBodyElem1.setAttribute("ContactId", "");
		SOAPElement auth = soapBodyElem1.addChildElement("Auth", "ser");
		auth.setAttribute("UserId", "WSUSER");
		auth.setAttribute("Password", "qua1ity0!");
		auth.setAttribute("Config", System.getProperty("umsceswsrole"));
		auth.setAttribute("FunctionName", "$TB.ATT.UPLOADATT.WS");
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "http://TechnologyOneCorp.com/Public/Services/Attachment_UploadAttachment");
		soapMessage.saveChanges();
		logger.info("SOAP Message method Request to uploadSOAPPDFRequest  \t");
		soapMessage.writeTo(System.out);
		return soapMessage;
	}

	/**
	 * @return String
	 */
	public String getUploadDefaultAttachment() {
		String strMsg = null;
		SOAPConnectionFactory soapConnectionFactory;
		try {
			soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			SOAPMessage soapResponse = null;
			try {
				soapResponse = soapConnection.call(createSOAPRequest(),
						System.getProperty("techoneattachmentdefaulturl"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				soapResponse.writeTo(out);
			} catch (IOException e) {
				e.printStackTrace();
			}
			strMsg = new String(out.toByteArray());
			logger.info("SOAP Response from  AttachementGetUploadDefault  \t ");
			soapResponse.writeTo(System.out);
			out.close();
			soapConnection.close();
		} catch (Exception r) {
			r.printStackTrace();
		}
		return strMsg;
	}

	/**
	 * @param serviceId
	 * @param headerWorkOrderNumber
	 * @return
	 * @throws Exception
	 */
	private static SOAPMessage updateServiceId(String serviceId, String headerWorkOrderNumber) throws Exception {
		SOAPMessage soapMessage = null;
		MessageFactory messageFactory = MessageFactory.newInstance();
		soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String serverURI = "http://TechnologyOneCorp.com/T1.W1.Public/Services";
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("ser", serverURI);
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("Task_DoUpdateV2", "ser");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("Request", "ser");
		SOAPElement auth = soapBodyElem1.addChildElement("Auth", "ser");
		auth.setAttribute("UserId", "WSUSER");
		auth.setAttribute("Password", "qua1ity0!");
		auth.setAttribute("Config", System.getProperty("umsceswsrole"));
		auth.setAttribute("FunctionName", "$W1.TSK.UPDATE2.WS");
		SOAPElement task = soapBodyElem1.addChildElement("Task", "ser");
		task.setAttribute("WorkSystemName", "ACNZ_WORKS");
		task.setAttribute("TaskNumber", headerWorkOrderNumber);
		task.setAttribute("PerformReadAttachments", "true");
		task.setAttribute("PerformAttachmentUpload", "false");
		SOAPElement soapBodyElem2 = task.addChildElement("AttachmentsV2", "ser");
		SOAPElement service = soapBodyElem2.addChildElement("Attachment", "ser");
		service.setAttribute("SysKey", "");
		service.setAttribute("SequenceNbr", "");
		service.setAttribute("AttLabel", "Service");
		service.setAttribute("AttTypeInd", "N");
		service.setAttribute("NotesData", "ServiceId: " + serviceId);
		service.setAttribute("UniqueNbr", "");
		service.setAttribute("EntityType", "WRKTask");
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "http://TechnologyOneCorp.com/T1.W1.Public/Services/Task_DoUpdateV2");
		soapMessage.saveChanges();
		logger.info("SOAP Message method Request to PDF serviceId Update Request  \t");
		soapMessage.writeTo(System.out);

		return soapMessage;
	}

	/**
	 * @return SOAPMessage
	 * @throws Exception
	 */
	private static SOAPMessage createSOAPRequest() throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String serverURI = "http://TechnologyOneCorp.com/Public/Services";
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("ser", serverURI);
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("Attachment_GetUploadDefaults", "ser");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("Request", "ser");
		soapBodyElem1.setAttribute("TotalFileSizeInBytes", "10000");
		SOAPElement auth = soapBodyElem1.addChildElement("Auth", "ser");
		auth.setAttribute("UserId", "WSUSER");
		auth.setAttribute("Password", "qua1ity0!");
		auth.setAttribute("Config", System.getProperty("umsceswsrole"));
		auth.setAttribute("FunctionName", "$TB.ATT.UPDEFS.WS");
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "http://TechnologyOneCorp.com/Public/Services/Attachment_GetUploadDefaults");
		soapMessage.saveChanges();
		logger.info("SOAP Message method Request to AttachementGetUploadDefault  \t");
		soapMessage.writeTo(System.out);
		return soapMessage;
	}

	/**
	 * @param nodeList
	 * @return String
	 */
	String getXMLElementValue(NodeList nodeList) {
		return nodeList.item(0).getTextContent();
	}
	/**
	 * @param specialContidion
	 * @param slaDate
	 * @param slaTime
	 * @param headerWorkOrderNumber
	 * @return SOAPMessage
	 * @throws Exception
	 */
	private static SOAPMessage updateServiceId(String specialContidion,String slaDate,String slaTime,String legacyCode,  String headerWorkOrderNumber) throws Exception {
		SOAPMessage soapMessage = null;
		MessageFactory messageFactory = MessageFactory.newInstance();
		soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String serverURI = "http://TechnologyOneCorp.com/T1.W1.Public/Services";
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("ser", serverURI);
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("Task_DoUpdateV2", "ser");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("Request", "ser");
		SOAPElement auth = soapBodyElem1.addChildElement("Auth", "ser");
		auth.setAttribute("UserId", "WSUSER");
		auth.setAttribute("Password", "qua1ity0!");
		auth.setAttribute("Config", System.getProperty("umsceswsrole"));
		auth.setAttribute("FunctionName", "$W1.TSK.UPDATE2.WS");
		SOAPElement task = soapBodyElem1.addChildElement("Task", "ser");
		task.setAttribute("WorkSystemName", "ACNZ_WORKS");
		task.setAttribute("TaskNumber", headerWorkOrderNumber);
		task.setAttribute("PerformReadAttachments", "true");
		task.setAttribute("PerformAttachmentUpload", "false");
		SOAPElement soapBodyElem2 = task.addChildElement("AttachmentsV2", "ser");
		SOAPElement service = soapBodyElem2.addChildElement("Attachment", "ser");
		service.setAttribute("SysKey", "");
		service.setAttribute("SequenceNbr", "");
		service.setAttribute("AttLabel", "Additional Information");
		service.setAttribute("AttTypeInd", "N");
		service.setAttribute("NotesData", "Respond By Date/Time " + slaDate+" "+slaTime+", Special Condition:" +specialContidion + ""+ ", Legacy Code:" +legacyCode);
		service.setAttribute("UniqueNbr", "");
		service.setAttribute("EntityType", "WRKTask");
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "http://TechnologyOneCorp.com/T1.W1.Public/Services/Task_DoUpdateV2");
		soapMessage.saveChanges();
		logger.info("SOAP Message method Request to NotesData Special Condition Responded By DataTime Request  \t");
		soapMessage.writeTo(System.out);

		return soapMessage;
	}
	/**
	 * 
	 */
	public PdfProcessor() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.mule.transformer.AbstractTransformer#setMuleContext(org.mule.api.
	 * MuleContext)
	 */
	@Override
	public void setMuleContext(MuleContext muleContext) {
		this.muleContext = muleContext;
	}

}