package com.ums.b2b.esb.controller;

import com.ums.b2b.esb.entity.WorkOrderResponse;

public class SampleResponse {

	public WorkOrderResponse hardcodedSampleRsponse() {
		
		WorkOrderResponse workOrderResponse = new WorkOrderResponse();
		
		workOrderResponse.setApprovalStatus("approvalStatus");
		workOrderResponse.setDetails("details");
		workOrderResponse.setExternalReference("externalReference");
		workOrderResponse.setIsTemplate(true);
		workOrderResponse.setLevelType("levelType");
		workOrderResponse.setSource("source");
		workOrderResponse.setStage("stage");
		workOrderResponse.setStatus("status");
		workOrderResponse.setTemplateWorkOrderNumber("templateWorkOrderNumber");
		workOrderResponse.setType("type");
		workOrderResponse.setWorkSystemName("workSystemName");
		
		return workOrderResponse;
		
	}
}
