package com.ums.b2b.esb.controller;

import com.lmax.disruptor.WorkProcessor;
import com.ums.b2b.esb.entity.ErrorResponse;
import com.ums.b2b.esb.entity.WorkOrderRequest;

public class ValidateRequestMessage {

	public ErrorResponse checkWorkOrderRequest(WorkOrderRequest workOrderRequest) {
		
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setResult(true);
		/*try {
			
			if(workOrderRequest.getOrderId().equals("") ||  workOrderRequest.getOrderId().length() > 12) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("order id should not be blank and not more than 12 characters");
			}else if(workOrderRequest.getParentOrder().equals("") ||  workOrderRequest.getParentOrder().length() > 12) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("parent order id should not be blank and not more than 12 characters");
			}else if(workOrderRequest.getDuplicateOf().equals("") ||  workOrderRequest.getDuplicateOf().length() > 12) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("duplicate order id should not be blank and not more than 12 characters");
			}else if(workOrderRequest.getPlannerGroup().equals("") ||  workOrderRequest.getPlannerGroup().length() > 12) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("planner group should not be blank and not more than 25 characters");
			}else if(workOrderRequest.getMainWorkCentre().equals("") ||  workOrderRequest.getMainWorkCentre().length() > 8) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("main work center should not be blank and not more than 8 characters");
			}else if(workOrderRequest.getAddress().equals("") ||  workOrderRequest.getAddress().length() > 125) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("address should not be blank and not more than 125 characters");
			}else if(workOrderRequest.getLocationId().equals("") ||  workOrderRequest.getLocationId().length() > 30) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("location id  should not be blank and not more than 30  characters");
			}else if(workOrderRequest.getEquipmentId().equals("") ||  workOrderRequest.getEquipmentId().length() > 18) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("equipment  id  should not be blank and not more than 18 characters");
			}else if(workOrderRequest.getPurchaseOrderId().equals("") ||  workOrderRequest.getPurchaseOrderId().length() > 10) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("purchase order id  should not be blank and not more than 10 characters");
			}else if(workOrderRequest.getPurchaseOrderItemId().equals("") ||  workOrderRequest.getPurchaseOrderItemId().length() >5) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("purchase order item  id  should not be blank and not more than 5 characters");
			}else if(workOrderRequest.getSupplierId().equals("") ||  workOrderRequest.getSupplierId().length() >10) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("supplier  id  should not be blank and not more than 10 characters");
			}else if(workOrderRequest.getSupplierOrderId().equals("") ||  workOrderRequest.getSupplierOrderId().length() >25) {
				errorResponse.setCode(401);
				errorResponse.setResult(false);
				errorResponse.setMessage("supplier orderId  should not be blank and not more than 25 characters");
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			errorResponse.setCode(500);
			errorResponse.setResult(false);
			errorResponse.setMessage(e.getMessage());
		}*/
		return errorResponse;
	}
}
