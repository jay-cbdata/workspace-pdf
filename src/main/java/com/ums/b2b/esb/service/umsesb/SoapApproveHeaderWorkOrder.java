package com.ums.b2b.esb.service.umsesb;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mule.api.MuleContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.service.workorder.controller.model.ErrorMessages;
import com.service.workorder.controller.model.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.utility.Constant;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

/**
 * @author Sankalp
 *
 */
public class SoapApproveHeaderWorkOrder {

	protected final static Log logger = LogFactory.getLog(SoapApproveHeaderWorkOrder.class);

	JSONObject jsonObject;
	String input;
	String response;
	String responseStatus;
	JSONParser parser = new JSONParser();

	/**
	 * @param esbId
	 * @param jsonObjectApprove
	 * @param workOrderNo
	 * @param muleContext
	 * @return JSONObject
	 */
	public JSONObject approveHeaderWorkOrder(String esbId, String sourceSystem, JSONObject jsonObjectApprove, String workOrderNo,
			MuleContext muleContext) throws Exception{
		try {
			Client clientApprove = Client.create();
			WebResource webResourceApprove = clientApprove.resource(System.getProperty("techoneapproveworkorderurl"));
			input = "{\"WorkSystemName\":\"" + jsonObjectApprove.get("WorkSystemName") + "\","
					+ "\"KeyedWorkOrderNumber\":\"" + jsonObjectApprove.get("KeyedWorkOrderNumber") + "\"}";
			ClientResponse responseClientApprove = webResourceApprove.type("application/json")
					.accept("application/json").header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, input);
			response = responseClientApprove.getEntity(String.class);
			responseStatus = String.valueOf(responseClientApprove.getStatus());
			if (responseStatus.equals("200"))
				responseStatus = "SUCCESS";

			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Input to Techone for Approving the WorkOrder via UMSESB \t" + input);
			logger.info(
					new UMSTimeDateLog().getTimeDateLog() +" Response Status  from TechOne  end for Approving the WorkOrder via UMSESB \t" + responseStatus);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response from TechOne end for Approve Workorder \t" + response);

			ObjectMapper mapper = new ObjectMapper();
			Response resobjest = mapper.readValue(response.toString(), Response.class);
			jsonObject = (JSONObject) parser.parse(response.toString());
			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

			if (errorMessageList.size() > 0) {
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" Errors Found in " + Constant.HEADER_WORKORDER + " Approve Response"
						+ errorMessageList.toString());
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.HEADER_WORKORDER, "APPROVE", input,
						workOrderNo, "INVALID", muleContext);
			
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("WorkOrderNumber").toString(), "FAIL", response, "APPROVE",
						Constant.HEADER_WORKORDER, muleContext);

				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.HEADER_WORKORDER, "APPROVE", input,
						workOrderNo, "VALID", muleContext);
				
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" No Errors Found in " + Constant.HEADER_WORKORDER + " Approve Response");
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("KeyedWorkOrderNumber").toString(), "SUCCESS", response, "APPROVE",
						Constant.HEADER_WORKORDER, muleContext);

			}

			
		} catch (Exception e) {
			throw e;
		}
		return jsonObject;
	}

}
