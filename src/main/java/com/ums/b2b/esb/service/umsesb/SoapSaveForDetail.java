package com.ums.b2b.esb.service.umsesb;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import com.ums.b2b.esb.utility.UMSTimeDateLog;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mule.api.MuleContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.service.workorder.controller.model.ErrorMessages;
import com.service.workorder.controller.model.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.utility.Constant;
import com.ums.b2b.esb.utility.DbUtil;

/**
 * @author Sankalp
 *
 */
public class SoapSaveForDetail {  
/*
 * TODO: 
 * Move all tech one api/ws calls to this class (CreateTemplate, Approve, Notes Data)
 * Rename to TechOneWorkOrderApiWebServiceConnector?
 * Move this class to a common / core library
 * Changes to this file to be restricted and only deployed after peer review
*/
	protected final static Log logger = LogFactory.getLog(SoapSaveForDetail.class);

	JSONObject jsonObject;

	String response;
	String responseStatus;
	JSONParser parser = new JSONParser();

	@SuppressWarnings("unchecked")
	public JSONObject SaveWorkOrder(JSONObject readObject, String sourceSys,  String purchaseOrderNumber, String workOrderNo, String esbId,
			String warranty, String warrantyCode, String risks, String riskCode, String cause, String causeCode,
			String locationId, String equipmentId, String address, String headerWorkOrderNumber, String priority,
			String workOrderDescription, String workOrderDetails, String release_Date, String status, String plannerGroup,
			String operationId, String parentOrder, String duplicateOf, String estimatedStartDateTime,
			String estimatedStartDate, String estimatedStartTime, String estimatedFinishDateTime,
			String estimatedFinishDate, String estimatedFinishTime, String actualStartDateTime, String actualStartDate,
			String actualStartTime, String actualFinishDateTime, String actualFinishDate, String actualFinishTime,
			String requiredStartDateTime, String requiredStartDate, String requiredStartTime, String serviceId, MuleContext muleContext)
			throws Exception {

		Client saveWorkOrderClient = Client.create();
		WebResource saveWorkOderResource = saveWorkOrderClient.resource(System.getProperty("techonesavefordetailurl"));
		JSONObject result = new JSONObject();

		try {
			String trimmedDesc = workOrderDescription;
			if (workOrderDescription.length()>40){
				trimmedDesc = workOrderDescription.substring(0,40);
			}
			
			workOrderDetails = workOrderDetails.trim().toString().replace("\n", "").replace("\r", "");
			String trimmedDetails = workOrderDetails;
			
			if(workOrderDetails.length()>1000){
				trimmedDetails = workOrderDetails.substring(0,1000);
			}
			
			result.put("ActivityCode", readData(readObject, "ActivityCode", "", ""));
			result.put("ApprovalStatus", readData(readObject, "ApprovalStatus", "", ""));
			result.put("AvailableForBudgeting", readData(readObject, "AvailableForBudgeting", "", ""));
			result.put("Description", readData(readObject, "Description", "", trimmedDesc));
			result.put("Details", readData(readObject, "Details", "",
					trimmedDetails));
		
			if (readObject.get("LevelType")=="H"){
				result.put("HeaderWorkOrderNumber", readData(readObject, "HeaderWorkOrderNumber", "", headerWorkOrderNumber));
				result.put("HeaderWorkOrderNumberInternal", readData(readObject, "HeaderWorkOrderNumberInternal", "", headerWorkOrderNumber));			
			}
			else {
				result.put("HeaderWorkOrderNumber", readData(readObject, "HeaderWorkOrderNumber", "", ""));
				result.put("HeaderWorkOrderNumberInternal", readData(readObject, "HeaderWorkOrderNumberInternal", "", ""));
			}
				
			result.put("IsBudgetWorkOrder", readData(readObject, "IsBudgetWorkOrder", "", ""));
			result.put("IsTemplate", readData(readObject, "IsTemplate", "", ""));
			result.put("KeyedWorkOrderNumber", readData(readObject, "KeyedWorkOrderNumber", "", ""));
			result.put("LevelType", readData(readObject, "LevelType", "", ""));
			result.put("PriorityCode", readData(readObject, "PriorityCode", "NA", priority));
			result.put("ProjectCode", readData(readObject, "ProjectCode", "", ""));
			result.put("RequiredByDate", readData(readObject, "RequiredByDate", "", ""));
			result.put("ResponsiblePerson", readData(readObject, "ResponsiblePerson", "", ""));
			result.put("Source", readData(readObject, "Source", "", ""));
			result.put("Stage", readData(readObject, "Stage", "", ""));
			result.put("Status", readData(readObject, "Status", "", ""));
			result.put("Type", readData(readObject, "Type", "", ""));
			result.put("Vers", readData(readObject, "Vers", "", ""));
			result.put("WorkOrderNumber", readData(readObject, "WorkOrderNumber", "", ""));
			
			result.put("WorkSystemName", readData(readObject, "WorkSystemName", "", ""));
			result.put("WorkTypeCode", readData(readObject, "WorkTypeCode", "", ""));

			result.put("RequiredByTime", readData(readObject, "RequiredByTime", "", ""));
			result.put("RequiredByDate", readData(readObject, "RequiredByDate", "", ""));
			result.put("RequiredByDateTime", readData(readObject, "RequiredByDateTime", "", ""));

			result.put("EstimatedStartDateTime",
					readData(readObject, "EstimatedStartDateTime", "", estimatedStartDateTime));
			result.put("EstimatedStartDate", readData(readObject, "EstimatedStartDate", "", estimatedStartDateTime));
			result.put("EstimatedStartTime",
					readData(readObject, "EstimatedStartTime", "14:00:00Z", estimatedStartTime));

			result.put("EstimatedFinishDateTime",
					readData(readObject, "EstimatedFinishDateTime", "", estimatedFinishDateTime));
			result.put("EstimatedFinishDate", readData(readObject, "EstimatedFinishDate", "", estimatedFinishDate));
			result.put("EstimatedFinishTime",
					readData(readObject, "EstimatedFinishTime", "14:00:00Z", estimatedFinishDate));

			result.put("ActualStartDateTime", readData(readObject, "ActualStartDateTime", "", actualStartDateTime));
			result.put("ActualStartDate", readData(readObject, "ActualStartDate", "", actualStartDate));
			result.put("ActualStartTime", readData(readObject, "ActualStartTime", "14:00:00Z", actualStartTime));

			result.put("ActualFinishDateTime", readData(readObject, "ActualFinishDateTime", "", actualFinishDateTime));
			result.put("ActualFinishDate", readData(readObject, "ActualFinishDate", "", actualFinishDate));
			result.put("ActualFinishTime", readData(readObject, "ActualFinishTime", "14:00:00Z", actualFinishTime));

			result.put("PercentComplete", readData(readObject, "PercentComplete", "", ""));
			result.put("ExpectedDuration", readData(readObject, "ExpectedDuration", "", ""));
			result.put("BillingType", readData(readObject, "BillingType", "", ""));
			result.put("BillingMethod", readData(readObject, "BillingMethod", "", ""));
			result.put("BillingLedgerCode", readData(readObject, "BillingLedgerCode", "", ""));
			result.put("BillingAccountNumberInternal", readData(readObject, "BillingAccountNumberInternal", "", ""));

			result.put("WorkOrderUserField1", readData(readObject, "WorkOrderUserField1", "", ""));
			result.put("WorkOrderUserField2", readData(readObject, "WorkOrderUserField2", "", ""));
			result.put("WorkOrderUserField3", readData(readObject, "WorkOrderUserField3", "", ""));
			result.put("WorkOrderUserField4", readData(readObject, "WorkOrderUserField4", "", ""));
			result.put("WorkOrderUserField5", readData(readObject, "WorkOrderUserField5", "", ""));
			result.put("WorkOrderUserField6", readData(readObject, "WorkOrderUserField6", "", ""));
			result.put("WorkOrderUserField7", readData(readObject, "WorkOrderUserField7", "", ""));

			String workOrderUserField8 = workOrderNo;
			if (readObject.get("LevelType") == "S") {
				workOrderUserField8 = operationId;
			}
			
			logger.info(new UMSTimeDateLog().getTimeDateLog() + "workirderuserfield8:::\t" + workOrderUserField8);
//			logger.info(new UMSTimeDateLog().getTimeDateLog() + "workOrderNo:::\t" + workOrderNo);
			logger.info(new UMSTimeDateLog().getTimeDateLog() + "readObject.get(LevelType):::\t" + readObject.get("LevelType"));
//			
			result.put("WorkOrderUserField8", readData(readObject, "WorkOrderUserField8", "", workOrderUserField8));
			result.put("WorkOrderUserField9", readData(readObject, "WorkOrderUserField9", "", address));
			result.put("WorkOrderUserField10", readData(readObject, "WorkOrderUserField10", "", equipmentId));
			result.put("WorkOrderUserField11", readData(readObject, "WorkOrderUserField11", "", locationId));
			result.put("WorkOrderUserField12", readData(readObject, "WorkOrderUserField12", "", ""));
			result.put("WorkOrderUserField13", readData(readObject, "WorkOrderUserField13", "", status));
			result.put("WorkOrderUserField14", readData(readObject, "WorkOrderUserField14", "", plannerGroup));
			result.put("WorkOrderUserField15", readData(readObject, "WorkOrderUserField15", "", parentOrder));
			result.put("WorkOrderUserField16", readData(readObject, "WorkOrderUserField16", "", duplicateOf));
			result.put("WorkOrderUserfield17", readData(readObject, "WorkOrderUserfield17", "", ""));
			result.put("WorkOrderUserField18", readData(readObject, "WorkOrderUserField18", "", release_Date));
			result.put("WorkOrderUserfield18_Time", readData(readObject, "WorkOrderUserfield18_Time", "0", ""));
			result.put("WorkOrderUserField19", readData(readObject, "WorkOrderUserField19", "", ""));
			result.put("WorkOrderUserField20", readData(readObject, "WorkOrderUserField20", "", ""));

			/*DO NOT change any of these fields and their values till confirmation from T1/Jay*/
			
			result.put("WorkOrderSelectionType1", readData(readObject, "WorkOrderSelectionType1", "", ""));
			result.put("WorkOrderSelectionType2", readData(readObject, "WorkOrderSelectionType2", "", ""));
			result.put("WorkOrderSelectionType3", readData(readObject, "WorkOrderSelectionType3", "", ""));
			result.put("WorkOrderSelectionType4", readData(readObject, "WorkOrderSelectionType4", "", ""));
			result.put("WorkOrderSelectionType5", readData(readObject, "WorkOrderSelectionType5", "", ""));
			result.put("WorkOrderSelectionType6", readData(readObject, "WorkOrderSelectionType6", "", ""));
			result.put("WorkOrderSelectionType7", readData(readObject, "WorkOrderSelectionType7", "", ""));
			result.put("WorkOrderSelectionType8", readData(readObject, "WorkOrderSelectionType8", "", ""));
			result.put("WorkOrderSelectionType9", readData(readObject, "WorkOrderSelectionType9", "", ""));
			result.put("WorkOrderSelectionType10", readData(readObject, "WorkOrderSelectionType10", "", ""));
			result.put("WorkOrderSelectionType11", readData(readObject, "WorkOrderSelectionType11", "", ""));
			result.put("WorkOrderSelectionType12", readData(readObject, "WorkOrderSelectionType12", "", ""));
			result.put("WorkOrderSelectionType13", readData(readObject, "WorkOrderSelectionType13", "", ""));
			result.put("WorkOrderSelectionType14", readData(readObject, "WorkOrderSelectionType14", "", ""));
			result.put("WorkOrderSelectionType15", readData(readObject, "WorkOrderSelectionType15", "", ""));
			result.put("WorkOrderSelectionType16", readData(readObject, "WorkOrderSelectionType16", "", serviceId));
			result.put("WorkOrderSelectionType17", readData(readObject, "WorkOrderSelectionType17", "", ""));
			result.put("WorkOrderSelectionType18", readData(readObject, "WorkOrderSelectionType18", "", ""));
			result.put("WorkOrderSelectionType19", readData(readObject, "WorkOrderSelectionType19", "", ""));
			result.put("WorkOrderSelectionType20", readData(readObject, "WorkOrderSelectionType20", "", ""));

			result.put("WorkTypeUserfield1", readData(readObject, "WorkTypeUserfield1", "", causeCode));
			result.put("WorkTypeUserfield2", readData(readObject, "WorkTypeUserfield2", "", risks));
			result.put("WorkTypeUserfield3", readData(readObject, "WorkTypeUserfield3", "", ""));
			result.put("WorkTypeUserfield4", readData(readObject, "WorkTypeUserfield4", "", ""));
			result.put("WorkTypeUserfield5", readData(readObject, "WorkTypeUserfield5", "", ""));
			result.put("WorkTypeUserfield6", readData(readObject, "WorkTypeUserfield6", "", purchaseOrderNumber));
			result.put("WorkTypeUserfield7", readData(readObject, "WorkTypeUserfield7", "", ""));
			result.put("WorkTypeUserfield8", readData(readObject, "WorkTypeUserfield8", "", ""));
			result.put("WorkTypeUserfield9", readData(readObject, "WorkTypeUserfield9", "", ""));
			result.put("WorkTypeUserfield10", readData(readObject, "WorkTypeUserfield10", "", ""));
			result.put("WorkTypeUserfield11", readData(readObject, "WorkTypeUserfield11", "", ""));
			result.put("WorkTypeUserfield12", readData(readObject, "WorkTypeUserfield12", "", ""));
			result.put("WorkTypeUserfield13", readData(readObject, "WorkTypeUserfield13", "", ""));
			
			result.put("WorkTypeUserfield13_Time", readData(readObject, "WorkTypeUserfield13_Time", "0", ""));
			result.put("WorkTypeUserfield14", readData(readObject, "WorkTypeUserfield14", "", ""));
			result.put("WorkTypeUserfield14_Time", readData(readObject, "WorkTypeUserfield14_Time", "0", ""));
			
			result.put("WorkTypeUserfield15", readData(readObject, "WorkTypeUserfield15", "", warrantyCode));
			result.put("WorkTypeUserfield16", readData(readObject, "WorkTypeUserfield16", "", warranty));
			result.put("WorkTypeUserfield17", readData(readObject, "WorkTypeUserfield17", "", ""));
			result.put("WorkTypeUserfield18", readData(readObject, "WorkTypeUserfield18", "", ""));

			result.put("WorkTypeUserfield19", readData(readObject, "WorkTypeUserfield19", "", ""));
			result.put("WorkTypeUserfield20", readData(readObject, "WorkTypeUserfield20", "false", ""));

			ClientResponse responseClientclientReadForSave = saveWorkOderResource.type("application/json")
					.accept("application/json").header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, result);

			response = responseClientclientReadForSave.getEntity(String.class);
			ObjectMapper mapper = new ObjectMapper();
			Response resobject = mapper.readValue(response.toString(), Response.class);

			responseStatus = String.valueOf(responseClientclientReadForSave.getStatus());
			if (responseStatus.equals("200"))
				responseStatus = "SUCCESS";
			
			String messgeType = Constant.SUBORDER_WORKORDER;
			
			if (readObject.get("LevelType").equals("H")){
				messgeType = Constant.HEADER_WORKORDER;
			}
			
		
			
//			logger.info(
//					"Sub Order  ==== Input to Techone via UMS ESB  for PDF Data Save For Detail  Step-III \t" + result);
//			logger.info(
//					"Sub Order  ==== Response Status  from TechOne   via  UMS ESB  for PDF Data Save For Detail  Step-III\t"
//							+ responseStatus);
//			logger.info(new UMSTimeDateLog().getTimeDateLog() + "Sub Order  ===== Response from TechOne end for PDF Data Sub Order Save For Detail  Step-III \t"
//					+ response);
			jsonObject = (JSONObject) parser.parse(response.toString());
			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobject);

			if (errorMessageList.size() > 0) {
				logger.info(new UMSTimeDateLog().getTimeDateLog() + "Errors Found in " + messgeType + " Save Response"
						+ errorMessageList.toString());
				new DbUtil().insertIntoMessageTable(esbId, sourceSys, messgeType,
						Constant.SAVEFORDETAILWITHCF, 
						result != null? (result.toJSONString() + " " + result.toString()):"Failed to read JSON", 
								workOrderNo, "INVALID",muleContext);
				
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("WorkOrderNumberInternal").toString(),
						"FAIL", response, Constant.SAVEFORDETAILWITHCF, messgeType, muleContext);
				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				new DbUtil().insertIntoMessageTable(esbId, sourceSys, messgeType,
						Constant.SAVEFORDETAILWITHCF, result.toJSONString() + " " 
				+ result.toString(), workOrderNo, "VALID",muleContext);
		
//				logger.info(new UMSTimeDateLog().getTimeDateLog() + "No Errors Found in " + messgeType + " Save Response");
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("WorkOrderNumberInternal").toString(),
						"SUCCESS", response, Constant.SAVEFORDETAILWITHCF, messgeType, muleContext);
				updateWorkOrderDescription(jsonObject.get("WorkOrderNumberInternal").toString(), workOrderDetails );
			}
		} catch (Exception e) {
			throw e;
		}

		return result;
	}
	
	public String updateWorkOrderDescription(String headerWorkOrderNumber, String text) {
		String strMsg = null;
		SOAPConnectionFactory soapConnectionFactory;
		try {

			soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			SOAPMessage soapResponse = null;
			try {
				soapResponse = soapConnection.call(updateWorkOrderDesc(headerWorkOrderNumber, text),
						System.getProperty("techoneserviceupdatedefaulturl"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				soapResponse.writeTo(out);
			} catch (IOException e) {
				e.printStackTrace();
			}
			strMsg = new String(out.toByteArray());
			logger.info("SOAP Response from  updateServiceId For NotesData   \t ");
			soapResponse.writeTo(System.out);
			out.close();
			soapConnection.close();
		} catch (Exception r) {
			r.printStackTrace();
		}
		return strMsg;
	}
	
	private static SOAPMessage updateWorkOrderDesc(String headerWorkOrderNumber, String text) throws Exception {
		SOAPMessage soapMessage = null;
		MessageFactory messageFactory = MessageFactory.newInstance();
		soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String serverURI = "http://TechnologyOneCorp.com/T1.W1.Public/Services";
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("ser", serverURI);
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("Task_DoUpdateV2", "ser");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("Request", "ser");
		SOAPElement auth = soapBodyElem1.addChildElement("Auth", "ser");
		auth.setAttribute("UserId", "WSUSER");
		auth.setAttribute("Password", "qua1ity0!");
		auth.setAttribute("Config", System.getProperty("umsceswsrole"));
		auth.setAttribute("FunctionName", "$W1.TSK.UPDATE2.WS");
		SOAPElement task = soapBodyElem1.addChildElement("Task", "ser");
		task.setAttribute("WorkSystemName", "ACNZ_WORKS");
		task.setAttribute("TaskNumber", headerWorkOrderNumber);
		task.setAttribute("PerformReadAttachments", "true");
		task.setAttribute("PerformAttachmentUpload", "false");
		SOAPElement soapBodyElem2 = task.addChildElement("AttachmentsV2", "ser");
		SOAPElement service = soapBodyElem2.addChildElement("Attachment", "ser");
		service.setAttribute("SysKey", "");
		service.setAttribute("SequenceNbr", "");
		service.setAttribute("AttLabel", "SAP Work Order Description");
		service.setAttribute("AttTypeInd", "N");
		service.setAttribute("NotesData", text);
		service.setAttribute("UniqueNbr", "");
		service.setAttribute("EntityType", "WRKTask");
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "http://TechnologyOneCorp.com/T1.W1.Public/Services/Task_DoUpdateV2");
		soapMessage.saveChanges();
		logger.info("SOAP Message method Request to NotesData Work Order Description Responded By DataTime Request  \t");
		soapMessage.writeTo(System.out);

		return soapMessage;
	}

	public String readData(JSONObject readObject, String columnName, String defaultValue, String overwriteValue) {

		//logger.info(new UMSTimeDateLog().getTimeDateLog() + "ReadData(" + columnName + ", default: " + defaultValue + ", overwrite: " + overwriteValue + ")");

		//1. First preference to overwrite value.
		if (overwriteValue != null && overwriteValue.trim().length() > 0)
			return overwriteValue;

		//2. Second check if read object contains the key
		if (readObject.containsKey(columnName)) {
			//logger.info(new UMSTimeDateLog().getTimeDateLog() + "ReadData Value: " + readObject.get(columnName));
			if (readObject.get(columnName) == "null") // date fields are
														// returned as "null"
														// string. but on save
														// they have to be ""
														// according to T1
				return "";

			if (readObject.get(columnName) != null) {
				return (String) readObject.get(columnName).toString();
			}
		}
		
		return defaultValue;
	}	
}