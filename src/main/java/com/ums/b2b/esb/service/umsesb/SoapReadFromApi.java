package com.ums.b2b.esb.service.umsesb;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mule.api.MuleContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.service.workorder.controller.model.ErrorMessages;
import com.service.workorder.controller.model.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.utility.Constant;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

/**
 * @author Sankalp
 *
 */
public class SoapReadFromApi {

	protected final static Log logger = LogFactory.getLog(SoapCreateFromTemplate.class);

	JSONObject jsonObject;
	String input;
	String response;
	String responseStatus;
	JSONParser parser = new JSONParser();

	/**
	 * @param esbId
	 * @param jsonObject
	 * @param workOrderNo
	 * @param muleContext
	 * @return JSONObject
	 */
	public JSONObject readFromApiHeaderWorkOrder(String esbId, String sourceSystem, JSONObject jsonObject, String workOrderNo,
			MuleContext muleContext) throws Exception {
		try {

			Client clientRead = Client.create();
			WebResource webResourceRead = clientRead.resource(System.getProperty("techonereadfordetailurl")
					+ jsonObject.get("WorkSystemName") + "/" + jsonObject.get("WorkOrderNumber"));
			
			ClientResponse responseReadClient = webResourceRead.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization")).get(ClientResponse.class);
			
			response = responseReadClient.getEntity(String.class);
			
			responseStatus = String.valueOf(responseReadClient.getStatus());
			
			if (responseStatus.equals("200"))
				responseStatus = "SUCCESS";
			
			ObjectMapper mapper = new ObjectMapper();
			Response resobjest = mapper.readValue(response.toString(), Response.class);

			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Input to TechOne READ ForDetail end \t " + "{" + "WorkSystemName:"
					+ jsonObject.get("WorkSystemName") + "," + "WorkOrderNumber:" + jsonObject.get("WorkOrderNumber")
					+ "}");
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response Status  from TechOne READ ForDetail HeaderWorkOrder \t" + responseStatus);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response from TechOne READ ForDetail HeaderWorkOrder  \t" + response);
		

			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

			if (errorMessageList.size() > 0) {
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" Errors Found in " + Constant.HEADER_WORKORDER + " Read Response"
						+ errorMessageList.toString());
				
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.HEADER_WORKORDER,
						Constant.READFROMAPI, "{WorkSystemName:" + jsonObject.get("WorkSystemName").toString()
								+ ",WorkOrderNumber:" + jsonObject.get("WorkOrderNumber").toString(),
						workOrderNo,"INVALID", muleContext);
				
				new DbUtil().insertIntoMessageResponseTable(esbId,jsonObject.get("WorkOrderNumber").toString(), "FAIL", response, Constant.READFROMAPI,
						Constant.HEADER_WORKORDER, muleContext);

				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.HEADER_WORKORDER,
						Constant.READFROMAPI, "{WorkSystemName:" + jsonObject.get("WorkSystemName").toString()
								+ ",WorkOrderNumber:" + jsonObject.get("WorkOrderNumber").toString(),
						workOrderNo, "VALID",muleContext);
				
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("WorkOrderNumber").toString(), "SUCCESS", response,
						Constant.READFROMAPI, Constant.HEADER_WORKORDER, muleContext);
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" No Errors Found in " + Constant.HEADER_WORKORDER + " Read Response ");
			}
			jsonObject = (JSONObject) parser.parse(response.toString());
		} catch (Exception e) {
			throw e;
		}
		return jsonObject;
	}
}
