package com.ums.b2b.esb.service.aklc;

import java.io.StringWriter;

import javax.xml.transform.TransformerFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.XML;
import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.annotations.expressions.Lookup;
import org.mule.api.context.MuleContextAware;
import org.w3c.dom.Document;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.service.workorder.InvalidMessageFault;
import com.service.workorder.InvalidMessageFault_Exception;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.service.workorder.WorkOrder;
import com.service.workorder.WorkOrderResponse;
import com.service.workorder.WorkOrderService;
import com.ums.b2b.esb.controller.SoapRequestForwarder;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.TechOnePdfUpdation;
import com.ums.b2b.esb.utility.TechOneSoapObjectRequestMaker;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

/**
 * @author Sankalp
 *
 */
public class WorkOrderServiceImpl implements WorkOrderService, org.mule.api.lifecycle.Callable, MuleContextAware {
	@Lookup
	private MuleContext muleContext;
	private String esbId = org.mule.util.UUID.getUUID();
	String status = null;
	String templateWorkOrderNumber = null;
	protected final static Log logger = LogFactory.getLog(WorkOrderServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mule.api.context.MuleContextAware#setMuleContext(org.mule.api.
	 * MuleContext)
	 */
	public void setMuleContext(MuleContext muleContext) {
		this.muleContext = muleContext;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.service.workorder.WorkOrderService#updateWorkOrder(com.service.
	 * workorder.WorkOrder)
	 */
	@Override
	public WorkOrderResponse updateWorkOrder(WorkOrder workOrderRequest)
			throws UnprocessedMessageFault_Exception, InvalidMessageFault_Exception {
		try {
			if (!new DbUtil().getPdfDataToUpdate(workOrderRequest.getOrderId(), muleContext).equals("FAILURE")) {
				String payload = new TechOneSoapObjectRequestMaker().getWorkOrderRequestSOAPObject(workOrderRequest);
				new TechOnePdfUpdation().updatePdfDocument(payload, workOrderRequest.getOrderId(), muleContext);
				new DbUtil().insertIntoMessageTable(esbId, "SAP", "WorkOrder", "UPDATE", payload,
						workOrderRequest.getOrderId(), "", "", muleContext);
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" DB insertion of the updated PDF Request Payload  \t " + payload.toString());
				return processWorkOrder(workOrderRequest);
			} else {
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" Call Failed Updated Request OrderId does not exist ");
				return processFaliurePDFWorkOrder(workOrderRequest);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processWorkOrder(workOrderRequest);
	}

	/**
	 * @param workOrderRequest
	 * @return WorkOrderResponse
	 * @throws UnprocessedMessageFault_Exception
	 * @throws InvalidMessageFault_Exception
	 */
	private WorkOrderResponse processFaliurePDFWorkOrder(WorkOrder workOrderRequest)
			throws UnprocessedMessageFault_Exception, InvalidMessageFault_Exception {
		WorkOrderResponse wor = new WorkOrderResponse();
		wor.setOrderId(workOrderRequest.getOrderId());
		wor.setStatus(System.getProperty("pdfupdatefailmessage"));
		return wor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.service.workorder.WorkOrderService#createWorkOrder(com.service.
	 * workorder.WorkOrder)
	 */
	@Override
	public WorkOrderResponse createWorkOrder(WorkOrder workOrder)
			throws UnprocessedMessageFault_Exception, InvalidMessageFault_Exception {
		String soapResponseMessageFromCouncil = null;
		WorkOrderResponse workOrderResponse = new WorkOrderResponse();
		try {
			new DbUtil().insertIntoMessageTable(esbId, "TechOne", "Order", "Create",
					new XmlMapper().writeValueAsString(workOrder), workOrder.getOrderId(), "", "", muleContext);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Original SOAP Payload Message sent to Council  and inserted into the DB \t"
					+ new XmlMapper().writeValueAsString(workOrder).toString());
			soapResponseMessageFromCouncil = new SoapRequestForwarder().callToCouncil(workOrder, esbId);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response from Council stored into the database \t");
			new DbUtil().insertInMessageResponseTable(esbId, workOrder.getOrderId(), "SUCCESS",
					soapResponseMessageFromCouncil, "Create", "Order", "SAP_TO_ESB", muleContext);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		new DbUtil().insertInMessageResponseTable(esbId, workOrder.getOrderId(), "SUCCESS",
				XML.toJSONObject(soapResponseMessageFromCouncil).toString(), "Create", "Order", "ESB_TO_TECHONE",
				muleContext);
		logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response send to TechOne" + XML.toJSONObject(soapResponseMessageFromCouncil).toString());

		workOrderResponse.setOrderId(workOrder.getOrderId());
		workOrderResponse.setSupplierId(System.getProperty("supplierId"));
		workOrderResponse.setSupplierOrderId(workOrder.getWorkOrderNumber());
		workOrderResponse.setStatus(workOrder.getStatus());
		return workOrderResponse;
	}

	/**
	 * @param workOrderRequest
	 * @return WorkOrderResponse
	 * @throws UnprocessedMessageFault_Exception
	 * @throws InvalidMessageFault_Exception
	 */
	private WorkOrderResponse processWorkOrder(WorkOrder workOrderRequest)
			throws UnprocessedMessageFault_Exception, InvalidMessageFault_Exception {
		WorkOrderResponse wor = new WorkOrderResponse();
		wor.setOrderId(workOrderRequest.getOrderId());
		wor.setSupplierId(workOrderRequest.getSupplierId());
		wor.setSupplierOrderId(workOrderRequest.getSupplierOrderId());
		wor.setStatus(workOrderRequest.getStatus());
		return wor;
	}

	/**
	 * @param workOrderRequest
	 * @param operation
	 * @param rqType
	 * @return String
	 * @throws Exception
	 */

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.mule.api.lifecycle.Callable#onCall(org.mule.api.MuleEventContext)
	 */
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		return eventContext.getMessage().getPayload();
	}

}