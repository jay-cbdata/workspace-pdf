package com.ums.b2b.esb.service.umsesb;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.annotations.expressions.Lookup;
import org.mule.api.context.MuleContextAware;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.service.workorder.controller.model.ErrorMessages;
import com.service.workorder.controller.model.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.utility.Constant;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

public class SoapCreateFromTemplateSubWorkOrder implements org.mule.api.lifecycle.Callable, MuleContextAware {
	@Lookup
	private MuleContext muleContext;
	protected final static Log logger = LogFactory.getLog(SoapCreateFromTemplateSubWorkOrder.class);
	JSONObject jsonObject;
	String input;
	String response;
	String responseStatus;
	JSONParser parser = new JSONParser();

	/**
	 * @param esbId
	 * @param responseFromPDF
	 * @param jsonObject
	 * @param locationId
	 * @param muleContext
	 * @return
	 */
	public JSONObject createFromTemplateHeaderSubWorkOrder(String esbId, String sourceSystem, String responseFromPDF, JSONObject jsonObjectm,
			String locationId, String workOrderNo, String workTypeCode, String activityCode,
			String headerWorkOrderNumber, String headerWorkOrderNumberInternal, MuleContext muleContext) {
		try {
			String templateWorkOrderNum = new DbUtil().getLocationBoardDesc(locationId, muleContext);
			String templateWorkOrderNumber = templateWorkOrderNum.trim().toString() + responseFromPDF.toString();
			templateWorkOrderNumber.replaceAll("\\s{2,}", " ").trim();
			try {
				Client clientRead = Client.create();
				String thePath = System.getProperty("techonereadfordetailurl") + "ACNZ_WORKS" + "/"
						+ templateWorkOrderNumber.toUpperCase().trim();
				URL url = new URL(thePath);
				URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(), url.getQuery());
				WebResource webResourceRead = clientRead.resource(uri.toString());
				ClientResponse responseReadClient = webResourceRead.type("application/json").accept("application/json")
						.header("Authorization", System.getProperty("techoneauthorization")).get(ClientResponse.class);
				response = responseReadClient.getEntity(String.class);
				if (response != null && response != "") {
					input = "{\"Source\":\"3P\"," + "\"ExternalReference\":\"SAP\","
							+ "\"ExternalReferenceEntityKey1\":\"" + workOrderNo + "\","
							+ "\"TemplateWorkOrderNumber\":\"" + templateWorkOrderNumber.toUpperCase().trim() + "\","
							+ "\"WorkSystemName\":\"ACNZ_WORKS\"," + "\"IsTemplate\":\"false\","
							+ "\"LevelType\":\"S\"," + "\"Status\":\"" + "I" + "\"," + "\"Type\":\"O\","
							+ "\"WorkTypeCode\":\"" + workTypeCode + "\"," + "\"ActivityCode\":\"" + activityCode
							+ "\"," + "\"HeaderWorkOrderNumber\":\"" + headerWorkOrderNumber + "\","
							+ "\"HeaderWorkOrderNumberInternal\":\"" + headerWorkOrderNumberInternal + "\"}";
					logger.info(new UMSTimeDateLog().getTimeDateLog() +" Input String is \t " + input);
					Client client = Client.create();
					WebResource webResource = client.resource(System.getProperty("techonecreatefromtemplateurl"));
					ClientResponse responseClient = webResource.type("application/json").accept("application/json")
							.header("Authorization", System.getProperty("techoneauthorization"))
							.post(ClientResponse.class, input);
					response = responseClient.getEntity(String.class);
					ObjectMapper mapper = new ObjectMapper();
					Response resobjest = mapper.readValue(response.toString(), Response.class);
					jsonObject = (JSONObject) parser.parse(response.toString());
					String responseStatus = String.valueOf(responseClient.getStatus());
					if (responseStatus.equals("200"))
						responseStatus = "SUCCESS";
						
					ErrorMessages errorMessage = new ErrorMessages();
					ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

					if (errorMessageList.size() > 0) {
						logger.info(new UMSTimeDateLog().getTimeDateLog() +" Errors Found in " + Constant.SUBORDER_WORKORDER + " CreateTemplate Response");

						new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.SUBORDER_WORKORDER,
								"CREATEFROMTEMPLATE", input, workOrderNo, "INVALID", muleContext);

						new DbUtil().insertIntoMessageResponseTable(esbId,  jsonObject.get("WorkOrderNumberInternal").toString(), "FAIL", response,
								"CREATEFROMTEMPLATE", Constant.SUBORDER_WORKORDER, muleContext);
						throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
					} else {

						new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.SUBORDER_WORKORDER,
								"CREATEFROMTEMPLATE", input, workOrderNo, "VALID", muleContext);

						new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("WorkOrderNumberInternal").toString(), "SUCCESS", response,
								"CREATEFROMTEMPLATE", Constant.SUBORDER_WORKORDER, muleContext);
						logger.info(new UMSTimeDateLog().getTimeDateLog() +" No Errors Found in " + Constant.SUBORDER_WORKORDER + " CreateTemplate  Response "
								+ errorMessageList.toString());
					}

					logger.info(
							new UMSTimeDateLog().getTimeDateLog() +" Sub Order  ==== Input to Techone via UMS ESB  for PDF Data CreateFromTemplate Step-I \t"
									+ input);
					logger.info(
							new UMSTimeDateLog().getTimeDateLog() +" Sub Order  ==== Response Status  from TechOne   via UMS ESB  for PDF Data CreateFromTemplate Step-I \t"
									+ responseStatus);
					logger.info(
							new UMSTimeDateLog().getTimeDateLog() +" Sub Order  ===== Response from TechOne end for PDF Data Sub Order CreateFromTemplate Step-I \t"
									+ response);
					
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			throw e;
		}
		return jsonObject;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.mule.api.lifecycle.Callable#onCall(org.mule.api.MuleEventContext)
	 */
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		return eventContext.getMessage().getPayload();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mule.api.context.MuleContextAware#setMuleContext(org.mule.api.
	 * MuleContext)
	 */
	@Override
	public void setMuleContext(MuleContext muleContext) {
		this.muleContext = muleContext;
	}

}
