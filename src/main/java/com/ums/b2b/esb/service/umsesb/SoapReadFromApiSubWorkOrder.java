package com.ums.b2b.esb.service.umsesb;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mule.api.MuleContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.service.workorder.controller.model.ErrorMessages;
import com.service.workorder.controller.model.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.utility.Constant;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

/**
 * @author Sankalp
 *
 */
public class SoapReadFromApiSubWorkOrder {

	protected final static Log logger = LogFactory.getLog(SoapReadFromApiSubWorkOrder.class);

	JSONObject jsonObject;
	String input;
	String response;
	String responseStatus;
	JSONParser parser = new JSONParser();

	/**
	 * @param esbId
	 * @param jsonObject
	 * @param workOrderNo
	 * @param muleContext
	 * @return JSONObject
	 */
	public JSONObject readFromApiSubWorkOrder(String esbId, String sourceSystem, JSONObject jsonSubWorkOrderObject, String workOrderNo,
			String headerWorkOrderNumber,MuleContext muleContext)throws Exception {
		String response = null;
		try {
			Client clientRead = Client.create();
			WebResource webResourceRead = clientRead.resource(
					System.getProperty("techonereadfordetailurl") + jsonSubWorkOrderObject.get("WorkSystemName") + "/"
							+ jsonSubWorkOrderObject.get("WorkOrderNumber"));
			ClientResponse responseReadClient = webResourceRead.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization")).get(ClientResponse.class);
			response = responseReadClient.getEntity(String.class);
			ObjectMapper mapper = new ObjectMapper();
			Response resobjest = mapper.readValue(response.toString(), Response.class);

			String responseStatus = String.valueOf(responseReadClient.getStatus());
			if (responseStatus.equals("200"))
				responseStatus = "SUCCESS";
		
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Sub Order  ==== Inout to TechOne READForDetailSubWorkOrder end  Step-II \t " + "{"
					+ "WorkSystemName:" + jsonSubWorkOrderObject.get("WorkSystemName") + "," + "WorkOrderNumber:"
					+ jsonSubWorkOrderObject.get("WorkOrderNumber") + "}");
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Sub Order  ==== Response Status  from TechOne READForDetailSubWorkOrder end Step-II \t"
					+ responseStatus);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Sub Order  ==== Response from TechOne READForDetailSubWorkOrder end   Step-II \t" + response);
			jsonObject = (JSONObject) parser.parse(response.toString());
			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

			if (errorMessageList.size() > 0) {
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" Errors Found in " + Constant.SUBORDER_WORKORDER + " Read Response"
						+ errorMessageList.toString());
				
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.SUBORDER_WORKORDER, "READFROMAPI", "{" + "WorkSystemName:" + jsonSubWorkOrderObject.get("WorkSystemName") + "," + "WorkOrderNumber:"
						+ jsonSubWorkOrderObject.get("WorkOrderNumber") + "}",
							workOrderNo, "INVALID",muleContext);
				
				new DbUtil().insertIntoMessageResponseTable(esbId,  jsonObject.get("WorkOrderNumberInternal").toString(), "FAIL", response, Constant.READFROMAPI,
						Constant.SUBORDER_WORKORDER, muleContext);

				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.SUBORDER_WORKORDER, "READFROMAPI", "{" + "WorkSystemName:" + jsonSubWorkOrderObject.get("WorkSystemName") + "," + "WorkOrderNumber:"
						+ jsonSubWorkOrderObject.get("WorkOrderNumber") + "}",
							workOrderNo, "VALID",muleContext);
			
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("WorkOrderNumberInternal").toString(), "SUCCESS", response,
						Constant.READFROMAPI, Constant.SUBORDER_WORKORDER, muleContext);
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" No Errors Found in " + Constant.SUBORDER_WORKORDER + " Read Response ");
			}
		} catch (Exception e) {
			throw e;
		}
		return jsonObject;
	}
}
