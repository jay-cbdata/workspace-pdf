package com.ums.b2b.esb.service.aklc;

import javax.jws.WebService;

import com.ums.b2b.esb.entity.WorkOrderRequest;

@WebService
public interface OrderService {

	String createWorkOrder(WorkOrderRequest request);
	String updateWorkOrder(WorkOrderRequest request);
}
