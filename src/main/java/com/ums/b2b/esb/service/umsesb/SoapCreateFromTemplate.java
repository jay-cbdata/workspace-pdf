package com.ums.b2b.esb.service.umsesb;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mule.api.MuleContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.service.workorder.controller.model.ErrorMessages;
import com.service.workorder.controller.model.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.utility.Constant;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

/**
 * @author Sankalp
 *
 */
public class SoapCreateFromTemplate {

	protected final static Log logger = LogFactory.getLog(SoapCreateFromTemplate.class);

	JSONObject jsonObject;
	
	String response;
	String responseStatus;
	JSONParser parser = new JSONParser();

	/**
	 * @param esbId
	 * @param workOrderNo
	 * @param workTypeCode
	 * @param desc_of_work
	 * @param startDate
	 * @param startTime
	 * @param completedDate
	 * @param completedTime
	 * @param activityCode
	 * @param muleContext
	 * @return JSONObject
	 */

	@SuppressWarnings("unchecked")
	public JSONObject createFromTemplate(String esbId, String sourceSystem,  String levelType, 
			String workOrderNo, String workTypeCode, String desc_of_work, String activityCode, 
			String workOrderName, String locationId, String responseFromPDF, 
			String headerWorkOrderNumber,
			MuleContext muleContext) throws Exception {
		try {
			String templateWorkOrderNum = new DbUtil().getLocationBoardDesc(locationId, muleContext);
			String templateWorkOrderNumber = templateWorkOrderNum.trim().toString() + responseFromPDF.toString();
			templateWorkOrderNumber.replaceAll("\\s{2,}", " ").trim();
			Client client = Client.create();
			logger.info("template:\t" + templateWorkOrderNumber);
			WebResource webResource = client.resource(System.getProperty("techonecreatefromtemplateurl"));
			
			JSONObject result = new JSONObject();

			result.put("Source","3P");
			result.put("ExternalReference",sourceSystem);
			result.put("ExternalReferenceEntityKey1",workOrderNo);
			result.put("TemplateWorkOrderNumber",templateWorkOrderNumber.toUpperCase().trim());
			result.put("WorkSystemName","ACNZ_WORKS");
			result.put("IsTemplate","false");
			result.put("LevelType",levelType);
			result.put("Status","I");
			result.put("ApprovalStatus","X");
			result.put("Stage","D");
			result.put("Type","O");
			result.put("WorkTypeCode",workTypeCode);
			result.put("Description",workOrderName);
			result.put("ActivityCode",activityCode);
			
			result.put("ActualStartDateTime","");
			result.put("ActualFinishDateTime","");
			
			if (levelType.equals(Constant.LEVEL_TYPE_SUB_WORK_ORDER)){
				result.put("HeaderWorkOrderNumber",headerWorkOrderNumber);
				result.put("HeaderWorkOrderNumberInternal",headerWorkOrderNumber);			
			}
						
			ClientResponse responseClient = webResource.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, result);
			
			response = responseClient.getEntity(String.class);
			responseStatus = String.valueOf(responseClient.getStatus());
			ObjectMapper mapper = new ObjectMapper();
			Response resobjest = mapper.readValue(response.toString(), Response.class);

			if (responseStatus.equals("200"))
				responseStatus = "SUCCESS";
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Input to Techone via UMS ESB  for PDF Data CreateFromTemplate \t" + result.toJSONString());
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response Status  from TechOne end for PDF Data CreateFromTemplate  \t" + responseStatus);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response from TechOne end for PDF Data CreateFromTemplate  \t" + response);
			
			String messageType = Constant.SUBORDER_WORKORDER;
			
			if (levelType.equals(Constant.LEVEL_TYPE_HEADER_WORK_ORDER)){
				logger.info("Level type \t" + levelType);
				logger.info("JSON \t" + result.toJSONString());
				messageType = Constant.HEADER_WORKORDER;
			}
			
			jsonObject = (JSONObject) parser.parse(response.toString());
//			new DbUtil().insertIntoMessageTable(esbId, sourceSystem, messageType,
//					Constant.CREATEFROMTEMPLATE, result.toJSONString(), workOrderNo, muleContext);

			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

			if (errorMessageList.size() > 0) {
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, messageType,
						Constant.CREATEFROMTEMPLATE, result.toJSONString(), workOrderNo,"INVALID", muleContext);

				logger.info(new UMSTimeDateLog().getTimeDateLog() +" Errors Found in " + messageType + " CreateTemplate Response - " + "Template Work Order Num: " + templateWorkOrderNumber.toUpperCase().trim());
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("WorkOrderNumber").toString(), "FAIL", response,
						Constant.CREATEFROMTEMPLATE, messageType, muleContext);
				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, messageType,
						Constant.CREATEFROMTEMPLATE, result.toJSONString(), workOrderNo, "VALID",muleContext);

				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("WorkOrderNumber").toString(), "SUCCESS", response,
						Constant.CREATEFROMTEMPLATE,messageType, muleContext);
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" No Errors Found in " + messageType + " CreateTemplate  Response "
						+ errorMessageList.toString());
			}

		} catch (Exception e) {
			throw e;
		}

		return jsonObject;
	}
}
