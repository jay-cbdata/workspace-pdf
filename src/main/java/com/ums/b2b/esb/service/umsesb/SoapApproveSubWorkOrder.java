package com.ums.b2b.esb.service.umsesb;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mule.api.MuleContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.service.workorder.controller.model.ErrorMessages;
import com.service.workorder.controller.model.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.utility.Constant;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

/**
 * @author Sankalp
 *
 */
public class SoapApproveSubWorkOrder {
	protected final static Log logger = LogFactory.getLog(SoapApproveSubWorkOrder.class);

	JSONObject jsonObject;
	String input;
	String response;
	String responseStatus;
	JSONParser parser = new JSONParser();

	/**
	 * @param esbId
	 * @param jsonObjectApprove
	 * @param workOrderNo
	 * @param muleContext
	 * @return JSONObject
	 */
	public JSONObject approveSubWorkOrder(String esbId, String sourceSystem, JSONObject jsonObjectApprove, String workOrderNo,
			String headerWorkOrderNumber,MuleContext muleContext) throws Exception{
		try {
			Client clientApprove = Client.create();
			WebResource webResourceApprove = clientApprove.resource(System.getProperty("techoneapproveworkorderurl"));
			input = "{\"WorkSystemName\":\"" + jsonObjectApprove.get("WorkSystemName") + "\","
					+ "\"KeyedWorkOrderNumber\":\"" + jsonObjectApprove.get("KeyedWorkOrderNumber") + "\"}";
			ClientResponse responseClientApprove = webResourceApprove.type("application/json")
					.accept("application/json").header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, input);
			response = responseClientApprove.getEntity(String.class);
			ObjectMapper mapper = new ObjectMapper();
			Response resobjest = mapper.readValue(response.toString(), Response.class);

			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

			responseStatus = String.valueOf(responseClientApprove.getStatus());
			if (responseStatus.equals("200"))
				responseStatus = "SUCCESS";
		
		
			logger.info(new UMSTimeDateLog().getTimeDateLog() +"  Sub Order  =====  Input to Techone for Approving the Sub WorkOrder via UMSESB  =====Step IV\t"
					+ input);
			logger.info(
					new UMSTimeDateLog().getTimeDateLog() +" Sub Order  ==== Response Status  from TechOne   via  UMS ESB  for PDF Data Save For Detail  Step-III\t"
							+ responseStatus);
			logger.info(
					new UMSTimeDateLog().getTimeDateLog() +" Sub Order  =====  Response from TechOne end for Approve Sub Workorder ====Step IV \t" + response);
			jsonObject = (JSONObject) parser.parse(response.toString());
			if (errorMessageList.size() > 0) {
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.SUBORDER_WORKORDER, "APPROVE", input,
						workOrderNo,"INVALID", muleContext);
			
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" Errors Found in " + Constant.SUBORDER_WORKORDER+ " Approve Response"
						+ errorMessageList.toString());
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("WorkOrderNumberInternal").toString(), "FAIL", response, "APPROVE",
						Constant.SUBORDER_WORKORDER, muleContext);

				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.SUBORDER_WORKORDER, "APPROVE", input,
						workOrderNo, "VALID",muleContext);
			
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" No Errors Found in " +Constant.SUBORDER_WORKORDER+ " Approve Response");
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("WorkOrderNumberInternal").toString(), "SUCCESS", response, "APPROVE",
						Constant.SUBORDER_WORKORDER, muleContext);

			}


		} catch (Exception e) {
			throw e;
		}
		return jsonObject;
	}

}
