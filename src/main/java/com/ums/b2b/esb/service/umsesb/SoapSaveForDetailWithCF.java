package com.ums.b2b.esb.service.umsesb;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mule.api.MuleContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.service.workorder.controller.model.ErrorMessages;
import com.service.workorder.controller.model.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.utility.Constant;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

/**
 * @author Sankalp
 *
 */
public class SoapSaveForDetailWithCF {

	protected final static Log logger = LogFactory.getLog(SoapCreateFromTemplate.class);

	JSONObject jsonObject;
	String input;
	String response;
	String responseStatus;
	JSONParser parser = new JSONParser();

	/**
	 * @param esbId
	 * @param jsonObject
	 * @param workOrderNo
	 * @param asset_Text
	 * @param address
	 * @param muleContext
	 * @return JSONObject
	 */
	@SuppressWarnings("unused")
	public JSONObject saveForDetailwithCF(String esbId, String sourceSystem, JSONObject jsonObject, String workOrderNo, String address,
			String equipmentId, String locationId, String startDate, String startTime, String contract_no, String risks,
			String riskCode, String cause, String causeCode, String warranty, String warrantyCode, String site_desc,
			String workOrderName, String priority, String release_Date, String completedDate, String completedTime,
			String status, String plannerGroup, String source, String WorkOrderType, String operationId,			
			MuleContext muleContext) throws Exception {
		try {
			String cNo[] = contract_no.split(" ");
			Client clientReadForSave = Client.create();
			WebResource webResourceclientReadForSave = clientReadForSave
					.resource(System.getProperty("techonesavefordetailurl"));
			String estimatedStartDate = (String) jsonObject.get("EstimatedStartDate");
			String AvailableForBudgeting = jsonObject.get("AvailableForBudgeting").toString();
			if (AvailableForBudgeting == "" || AvailableForBudgeting == null) {
				AvailableForBudgeting = Constant.FALSE;
			}

			String addressUserField9 = "NA";
			String actualStartDateTime = "";
			String temptime = "";
		/*	if (startTime.endsWith("00:00:00")) {
				temptime = startTime + "Z";

			} else {
				temptime = startTime + ":00Z";
			}*/
			String endTime = "";
		/*	if (completedTime.endsWith("00:00:00")) {
				endTime = completedTime + "Z";

			} else {
				endTime = completedTime + ":00Z";
			}
		*/	String completedStartDateTime = "";
			/*if (completedDate != "") {
				if (endTime.length() == 9) {
					completedStartDateTime = completedDate + "T" + completedTime + ":00Z";

				} else
					completedStartDateTime = completedDate + "T" + completedTime + "Z";
			} else
				completedStartDateTime = "";*/
			String startDateTime = "";
		/*	if (startDate != "") {
				if (temptime.length() == 9) {
					startDateTime = completedDate + "T" + completedTime + ":00Z";

				} else
					startDateTime = completedDate + "T" + completedTime + "Z";
			} else
				startDateTime = "";
		*/	String actualFinishDateTime = "";
			/*if (completedDate != "") {
				actualFinishDateTime = completedDate + "T" + temptime;
			} else
				actualFinishDateTime = "";*/
			if (jsonObject.get("WorkOrderUserfield9") != null)
				addressUserField9 = jsonObject.get("WorkOrderUserfield9").toString();
			input = "{\"WorkTypeUserfieldWorkCodeType\":\"" + isNull(jsonObject.get("WorkTypeUserfieldWorkCodeType"))
					+ "\"," + "\"AvailableForBudgeting\":\"" + jsonObject.get("AvailableForBudgeting") + "\","
					+ "\"WorkSystemName\":\"" + jsonObject.get("WorkSystemName") + "\"," + "\"KeyedWorkOrderNumber\":\""
					+ jsonObject.get("KeyedWorkOrderNumber") + "\"," + "\"WorkOrderNumberInternal\":\""
					+ jsonObject.get("WorkOrderNumberInternal") + "\"," + "\"WorkOrderNumber\":\""
					+ jsonObject.get("WorkOrderNumber") + "\"," + "\"Description\":\"" + workOrderName + "\","
					+ "\"Details\":\"" + site_desc + "\"," + "\"IsTemplate\":\"" + jsonObject.get("IsTemplate") + "\","
					+ "\"Type\":\"" + jsonObject.get("Type") + "\"," + "\"Status\":\"" + jsonObject.get("Status")
					+ "\"," + "\"ApprovalStatus\":\"X\"," + "\"Stage\":\"" + jsonObject.get("Stage") + "\","
					+ "\"Source\":\"" + jsonObject.get("Source") + "\"," + "\"ResponsiblePerson\":\""
					+ jsonObject.get("ResponsiblePerson") + "\"," + "\"ProjectCode\":\"" + jsonObject.get("ProjectCode")
					+ "\"," + "\"ActivityCode\":\"" + jsonObject.get("ActivityCode") + "\"," + "\"PriorityCode\":\""
					+ priority + "\"," + "\"WorkTypeCode\":\"" + jsonObject.get("WorkTypeCode") + "\","
					+ "\"LevelType\":\"" + jsonObject.get("LevelType") + "\"," + "\"HeaderWorkOrderNumber\":\""
					+ jsonObject.get("HeaderWorkOrderNumber") + "\"," + "\"HeaderWorkOrderNumberInternal\":\""
					+ jsonObject.get("HeaderWorkOrderNumberInternal") + "\"," + "\"IsBudgetWorkOrder\":\""
					+ jsonObject.get("IsBudgetWorkOrder") + "\"," + "\"RowId\":\"" + jsonObject.get("RowId") + "\","
					+ "\"Vers\":\"" + jsonObject.get("Vers") + "\"," + "\"LocationType\":\""
					+ jsonObject.get("LocationType") + "\"," + "\"LocationKey1\":\"" + jsonObject.get("LocationKey1")
					+ "\"," + "\"LocationKey2\":\"" + jsonObject.get("LocationKey2") + "\"," + "\"Location\":\""
					+ isEmpty(jsonObject.get("Location")) + "\"," + "\"RequiredByDate\":\""
					+ isEmpty(jsonObject.get("RequiredByDate")) + "\"," + "\"RequiredByTime\":\""
					+ isEmpty(jsonObject.get("RequiredByTime")) + "\"," + "\"RequiredByDateTime\":\""
					+ isEmpty(jsonObject.get("RequiredByDateTime")) + "\"," + "\"EstimatedStartDate\":\""
					+ isEmpty(jsonObject.get("EstimatedStartDate")) + "\"," + "\"EstimatedStartTime\":\"" + temptime
					+ "\"," + "\"EstimatedStartDateTime\":\"" + startDateTime + "\"," + "\"EstimatedFinishDate\":\""
					+ completedDate + "\"," + "\"EstimatedFinishTime\":\"" + endTime + "\","
					+ "\"EstimatedFinishDateTime\":\"" + completedStartDateTime + "\"," + "\"ActualStartDateTime\":\""
					+ startDateTime + "\"," + "\"ActualStartDate\":\"" + startDate + "\"," + "\"ActualStartTime\":\""
					+ temptime + "\"," + "\"ActualFinishDateTime\":\"" + actualFinishDateTime + "\","
					+ "\"ActualFinishDate\":\"" + completedDate + "\"," + "\"ActualFinishTime\":\"" + endTime + "\","
					+ "\"ExpectedDuration\":\"" + jsonObject.get("ExpectedDuration") + "\"," + "\"PercentComplete\":\""
					+ "0" + "\"," + "\"BillingType\":\"" + jsonObject.get("BillingType") + "\","
					+ "\"BillingMethod\":\"" + jsonObject.get("BillingMethod") + "\"," + "\"BillingLedgerCode\":\""
					+ jsonObject.get("BillingLedgerCode") + "\"," + "\"BillingAccountNumberInternal\":\""
					+ jsonObject.get("BillingAccountNumberInternal") + "\"," + "\"Comment\":\""
					+ jsonObject.get("Comment") + "\"," + "\"WorkOrderSelectionType1\":\"" + "NA" + "\","
					+ "\"WorkOrderSelectionType2\":\"" + "NA" + "\"," + "\"WorkOrderSelectionType3\":\"" + "NA" + "\","
					+ "\"WorkOrderSelectionType4\":\"" + "NA" + "\"," + "\"WorkOrderSelectionType5\":\"" + "NA" + "\","
					+ "\"WorkOrderSelectionType6\":\"" + "NO" + "\"," + "\"WorkOrderSelectionType7\":\"" + "NO" + "\","
					+ "\"WorkOrderSelectionType8\":\"" + "NA" + "\"," + "\"WorkOrderSelectionType9\":\"" + "NA" + "\","
					+ "\"WorkOrderSelectionType10\":\"" + "NA" + "\"," + "\"WorkOrderSelectionType11\":\"" + "NA"
					+ "\"," + "\"WorkOrderSelectionType12\":\"" + "NA" + "\"," + "\"WorkOrderSelectionType13\":\""
					+ "NA" + "\"," + "\"WorkOrderSelectionType14\":\"" + "NA" + "\","
					+ "\"WorkOrderSelectionType15\":\"" + "NA" + "\"," + "\"WorkOrderSelectionType16\":\"" + ""
					+ "\"," + "\"WorkOrderSelectionType17\":\"" + "" + "\"," + "\"WorkOrderSelectionType18\":\""
					+ "" + "\"," + "\"WorkOrderSelectionType19\":\"" + "" + "\","
					+ "\"WorkOrderSelectionType20\":\"" + "" + "\"," 
					
					+ "\"WorkTypeUserField1\":\"" + causeCode + "\","
					+ "\"WorkTypeUserField2\":\"" + risks + "\"," 
					+ "\"WorkTypeUserField3\":\"" + isEmpty(jsonObject.get("WorkTypeUserfield3")) + "\"," 
					+ "\"WorkTypeUserField4\":\"" + isEmpty(jsonObject.get("WorkTypeUserfield4")) + "\","
					+ "\"WorkTypeUserField5\":\"" + isNull(jsonObject.get("WorkTypeUserfield5")) + "\"," 
					+ "\"WorkTypeUserField6\":\"" + cNo[0].toString().trim() + "\"," 
					+ "\"WorkTypeUserField7\":\"" + isNull(jsonObject.get("WorkTypeUserfield7")) + "\"," 
					+ "\"WorkTypeUserField8\":\"" + "NA" + "\","
					+ "\"WorkTypeUserField9\":\"" + isNull(jsonObject.get("WorkTypeUserfield9")) + "\","
					+ "\"WorkTypeUserField10\":\"" + isNull(jsonObject.get("WorkTypeUserfield10")) + "\","
					+ "\"WorkTypeUserField11\":\"" + isNull(jsonObject.get("WorkTypeUserfield11")) + "\","
					+ "\"WorkTypeUserField12\":\"" + isNull(jsonObject.get("WorkTypeUserfield12")) + "\","
					+ "\"WorkTypeUserField13\":\"" + isEmpty(jsonObject.get("WorkTypeUserfield13")) + "\","
					+ "\"WorkTypeUserfield13_Time\":\"" + "0" + "\"," 
					+ "\"WorkTypeUserField14\":\"" + isEmpty(jsonObject.get("WorkTypeUserField14")) + "\","
					+ "\"WorkTypeUserfield14_Time\":\"" + "0" + "\","
		            + "\"WorkTypeUserField15\":\"" + warranty + "\"," 
					+ "\"WorkTypeUserField16\":\"" + warrantyCode + "\"," 
					+ "\"WorkTypeUserField17\":\"" + riskCode + "\"," 
					+ "\"WorkTypeUserField18\":\"" + "" + "\"," 
					+ "\"WorkOrderUserfield18_Time\":\"" + "0" + "\","
					+ "\"WorkTypeUserField19\":\"" + isNull(jsonObject.get("WorkOrderUserfield19")) + "\"," 
					+ "\"WorkTypeUserField20\":\"" + isNull(jsonObject.get("WorkOrderUserfield20")) + "\"," 
					
					+ "\"WorkOrderUserfield1\":\"" + "0" + "\","
					+ "\"WorkOrderUserfield2\":\"" + "NA" + "\"," 
					+ "\"WorkOrderUserfield3\":\"" + isNull(jsonObject.get("WorkOrderUserfield3")) + "\"," 
					+ "\"WorkOrderUserfield4\":\"" + jsonObject.get("WorkOrderUserfield4") + "\"," 
					+ "\"WorkOrderUserfield5\":\"" + "" + "\","
					+ "\"WorkOrderUserfield6\":\"" + "NA" + "\"," 
					+ "\"WorkOrderUserfield7\":\"" + "NA" + "\","
					+ "\"WorkOrderUserfield8\":\"" + workOrderNo + "\"," 
					+ "\"WorkOrderUserfield9\":\"" + address + "\"," 
					+ "\"WorkOrderUserfield10\":\"" + isNull(jsonObject.get("WorkOrderUserfield10")) + "\","
					+ "\"WorkOrderUserfield11\":\"" + equipmentId + "\"," 
					+ "\"WorkOrderUserfield12\":\"" + "" + "\","
					+ "\"WorkOrderUserfield13\":\"" + status + "\"," 
					
					+ "\"WorkOrderUserfield14\":\"" + plannerGroup + "\","				
	               + "\"WorkOrderUserfield15\":\"" + isNull(jsonObject.get("WorkOrderUserfield15")) + "\","
					+ "\"WorkOrderUserfield16\":\"" + isNull(jsonObject.get("WorkOrderUserfield16")) + "\","
					+ "\"WorkOrderUserfield17\":\"" + isNull(jsonObject.get("WorkOrderUserfield17")) + "\","
					+ "\"WorkOrderUserfield18\":\"" + release_Date + "\","
					+ "\"WorkOrderUserfield19\":\"" + "" + "\","
					+ "\"WorkOrderUserfield20\":\"" + "false" + "\"" + "}";
			
			ClientResponse responseClientclientReadForSave = webResourceclientReadForSave.type("application/json")
					.accept("application/json").header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, input);
			response = responseClientclientReadForSave.getEntity(String.class);
			responseStatus = String.valueOf(responseClientclientReadForSave.getStatus());
			if (responseStatus.equals("200"))
				responseStatus = "SUCCESS";
			jsonObject = (JSONObject) parser.parse(response.toString());
			ObjectMapper mapper = new ObjectMapper();
			Response resobjest = mapper.readValue(response.toString(), Response.class);
			
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Input to Techone for Saving the Details via UMSESB\t" + input);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response Status  from TechOne  end for  Saving the Details \t" + responseStatus);
			logger.info(new UMSTimeDateLog().getTimeDateLog() +" Response from TechOne end for  Saving the Details \t" + response);

			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

			if (errorMessageList.size() > 0) {
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" Errors Found in " + Constant.HEADER_WORKORDER + " Save Response"
						+ errorMessageList.toString());
				
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.HEADER_WORKORDER,
						Constant.SAVEFORDETAILWITHCF, input, workOrderNo, "INVALID", muleContext);
				
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("KeyedWorkOrderNumber").toString(),
						"FAIL", response, Constant.SAVEFORDETAILWITHCF, Constant.HEADER_WORKORDER, muleContext);
				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				
				
				logger.info(new UMSTimeDateLog().getTimeDateLog() +" No Errors Found in " + Constant.HEADER_WORKORDER + " Save Response");
				new DbUtil().insertIntoMessageTable(esbId, sourceSystem, Constant.HEADER_WORKORDER,
						Constant.SAVEFORDETAILWITHCF, input, workOrderNo, "VALID", muleContext);
				
				new DbUtil().insertIntoMessageResponseTable(esbId, jsonObject.get("KeyedWorkOrderNumber").toString(),
						"SUCCESS", response, Constant.SAVEFORDETAILWITHCF, Constant.HEADER_WORKORDER, muleContext);
			}
		} catch (Exception e) {
			throw e;
		}
		return jsonObject;
	}

	/**
	 * @param jsonObject
	 * @return Object
	 */
	private Object isNull(Object jsonObject) {
		if (jsonObject == null)
			return "NA";
		return jsonObject;
	}

	/**
	 * @param value
	 * @return Object
	 */
	private Object isEmpty(Object value) {
		if (value == null)
			return "";
		return value;
	}

}
