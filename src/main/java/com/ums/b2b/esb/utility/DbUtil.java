package com.ums.b2b.esb.utility;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mule.api.MuleContext;
import org.mule.api.annotations.expressions.Lookup;
import org.mule.api.context.MuleContextAware;
import org.mule.module.db.internal.resolver.database.DbConfigResolver;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

/**
 * @author Sankalp
 *
 */
public class DbUtil implements MuleContextAware {

	@Lookup
	private MuleContext muleContext;

	protected final static Log logger = LogFactory.getLog(DbUtil.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mule.api.context.MuleContextAware#setMuleContext(org.mule.api.
	 * MuleContext)
	 */
	@Override
	public void setMuleContext(MuleContext muleContext) {
		this.muleContext = muleContext;
	}

	/**
	 * @param orderNo
	 * @param muleContext
	 * @return String
	 */
	public String getOrderIdToUpdate(String orderNo, MuleContext muleContext) {
		String xmlOutput = null;
		try {
			DbConfigResolver dbConfigResolverma = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSourcewwa = dbConfigResolverma.resolve(null).getDataSource();
			String sql = "SELECT payload FROM message WHERE sapid ='" + orderNo + "'and " + "actiontype= " + "'"
					+ "CREATE" + "'and sourcesys='" + "SAP' and messagetype=" + "'" + "WorkOrder" + "'" + "and valid ="
					+ "'Valid'" + " ORDER BY datecreated DESC " + " LIMIT 1";
			JdbcTemplate jdbcTemplatewwa = new JdbcTemplate(dataSourcewwa);
			xmlOutput = jdbcTemplatewwa.queryForObject(sql, String.class);
		} catch (Exception e) {
			return "FAILURE";
		}
		return xmlOutput;
	}

	/**
	 * @param sourceSys
	 * @param messageType
	 * @param actionType
	 * @param workOrderNo
	 * @param outgoingMsg
	 */
	public void insertIntoMessageTable(String esbId, String sourceSys, String messageType, String actionType,
			String payload, String workOrderNo, String valid, MuleContext muleContext) {
		try {
			DbConfigResolver dbConfigResolver = muleContext.getRegistry().get("MySQL_Configuration");
//			logger.info("==\nMessageType \t" + messageType);
//			logger.info("Payload \t" + payload);
//			logger.info("==\n");
			DataSource dataSource = dbConfigResolver.resolve(null).getDataSource();
			String sql = "INSERT INTO message(esbid,sourcesys,messagetype,actiontype,payload,sapid, valid) VALUES (" + "'"
					+ esbId + "'," + "'" + sourceSys + "'," + "'" + messageType + "'," + "'" + actionType + "'," + "'"
					+ payload.replace("'", "''") + "'," + "'" + workOrderNo + "'," + 
					"'" + valid + "'" + ")";
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			jdbcTemplate.execute(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @param response,actionType
	 */
	public void insertIntoMessageResponseTable(String esbId, String workOrderNo, String responseStatus, String response,
			String actionType, String messageType, MuleContext muleContext) {
		try {
			DbConfigResolver dbConfigResolver = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSource = dbConfigResolver.resolve(null).getDataSource();
			JdbcTemplate jdbcTemplateResponseTable = new JdbcTemplate(dataSource);
			String sqlInsertion = "INSERT INTO messageresponse(esbId,status,targetsys,actiontype,messagetype,techOneID,result) VALUES ("
					+ "'" + esbId + "'," + "'" + responseStatus + "'," + "'" + "TECHONE" + "'," + "'" + actionType
					+ "'," + "'" + messageType + "'," + "'" + workOrderNo + "'," + "'" + response + "'" + ")";
			jdbcTemplateResponseTable.execute(sqlInsertion);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param orderCode
	 * @param muleContext
	 * @return String
	 */
	public String getOrderType(String orderCode, MuleContext muleContext) {
		String desc = null;
		try {
			DbConfigResolver dbConfigResolver = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSourcer = dbConfigResolver.resolve(null).getDataSource();
			String sql = "SELECT description FROM ordertype WHERE code ='" + orderCode + "'";
			JdbcTemplate jdbcTemplatet = new JdbcTemplate(dataSourcer);
			desc = jdbcTemplatet.queryForObject(sql, String.class);
		} catch (Exception e) {
			return "FAILURE";
		}
		return desc;
	}

	/**
	 * @param esbId
	 * @param techOneId
	 * @param status
	 * @param result
	 * @param actionType
	 * @param messageType
	 * @param muleContext
	 */
	public void insertInMessageResponseTable(String esbId, String techOneId, String status, String result,
			String actionType, String messageType, String targetSystem, MuleContext muleContext) {
		try {
			DbConfigResolver dbConfigResolver = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSourceee = dbConfigResolver.resolve(null).getDataSource();
			JdbcTemplate jdbcTemplateResponseTable = new JdbcTemplate(dataSourceee);
			String sqlInsertione = "INSERT INTO messageresponse(esbId,status,targetsys,actiontype,messagetype,techOneID,result) VALUES ("
					+ "'" + esbId + "'," + "'" + status + "'," + "'" + targetSystem + "'," + "'" + actionType + "',"
					+ "'" + messageType + "'," + "'" + techOneId + "'," + "'" + result.replace("'", "''") + "'" + ")";
			jdbcTemplateResponseTable.execute(sqlInsertione);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertInMessageResponseTable(String esbId, String techOneId, String status, String result,
			String actionType, String messageType, MuleContext muleContext) {

		insertInMessageResponseTable(esbId, techOneId, status, result, actionType, messageType, "TECHONE_TO_ESB",
				muleContext);
	}

	/**
	 * @param orderNo
	 * @param muleContext
	 * @return String
	 */
	public String getPdfDataToUpdate(String orderNo, MuleContext muleContext) {
		String xmlOutput = null;
		try {
			DbConfigResolver dbConfigResolverm = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSourceww = dbConfigResolverm.resolve(null).getDataSource();
			String sql = "SELECT payload FROM message WHERE sapid ='" + orderNo + "'and " + "actiontype= " + "'"
					+ "CREATE" + "'and sourcesys='" + "SAP' and messagetype=" + "'" + "WorkOrder" + "'";
			JdbcTemplate jdbcTemplateww = new JdbcTemplate(dataSourceww);
			xmlOutput = jdbcTemplateww.queryForObject(sql, String.class);
		} catch (Exception e) {
			return "FAILURE";
		}
		return xmlOutput;
	}

	/**
	 * @param orderNo
	 * @param muleContext
	 * @return String
	 */
	public String getPdfDataToUpdateMain(String orderNo, MuleContext muleContext) {
		String xmlOutput = null;
		try {
			
			DbConfigResolver dbConfigResolverm = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSourcem = dbConfigResolverm.resolve(null).getDataSource();
			String sql = "SELECT valid FROM message WHERE sapid ='" + orderNo + "'and " + "actiontype= " + "'"
					+ "CREATE" + "'and sourcesys='" + "PDF' and messagetype=" + "'" + "WorkOrder" + "'" 
					+ "and (valid='' or valid = 'valid') order by datecreated desc limit 1;" 
					;
			//logger.info("SQL: " + sql);
			JdbcTemplate jdbcTemplatem = new JdbcTemplate(dataSourcem);
			xmlOutput = jdbcTemplatem.queryForObject(sql, String.class);
			logger.info(xmlOutput.trim().toLowerCase().equals("valid")?"yes":"no");
		} catch (Exception e) {
			//e.printStackTrace();
			return "no";
		}
		return xmlOutput;
	}

	/**
	 * @param orderNo
	 * @return String
	 */
	public String getPdfDataToUpdate(String orderNo) {
		String pdfOutput = null;
		try {
			DbConfigResolver dbConfigResolver = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSource = dbConfigResolver.resolve(null).getDataSource();
			String sql = "SELECT payload FROM message WHERE sapid ='" + orderNo + "'and " + "actiontype= " + "'"
					+ "CREATE" + "'and sourcesys='" + "PDF' and messagetype=" + "'" + "WorkOrder" + "'";
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			pdfOutput = jdbcTemplate.queryForObject(sql, String.class);
		} catch (Exception e) {
			return "FAILURE";
		}
		return pdfOutput;
	}

	/**
	 * @param location
	 * @return String
	 */
	public String getLocationBoardDesc(String location, MuleContext muleContext) {
		String desc = null;
		try {
			DbConfigResolver dbConfigResolver = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSource = dbConfigResolver.resolve(null).getDataSource();
			String sql = "SELECT localboarddescription FROM site WHERE site ='" + location + "'";
			JdbcTemplate jdbcTemplater = new JdbcTemplate(dataSource);
			logger.info(sql);
			desc = jdbcTemplater.queryForObject(sql, String.class);
		} catch (Exception e) {
			return "FAILURE";
		}
		return desc;
	}

	/**
	 * @param site
	 * @param code
	 * @param muleContext
	 * @return String
	 * @throws Exception
	 */
	public String getLocalBordedDescriptor(String site, String code, MuleContext muleContext) throws Exception {
		String descriptor = null;
		try {
			DbConfigResolver dbConfigResolver = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSource = dbConfigResolver.resolve(null).getDataSource();
			JdbcTemplate jdbcTemplateResponseTable = new JdbcTemplate(dataSource);
			String sql = "SELECT localboarddescription FROM site where site='" + site + "'";
			String resultSite = jdbcTemplateResponseTable.query(sql, new ResultSetExtractor<String>() {
				@Override
				public String extractData(ResultSet rs) throws SQLException, DataAccessException {
					return rs.next() ? rs.getString(1) : null;
				}
			});
			sql = "SELECT description FROM ordertype where code='" + code + "'";
			String resultCode = jdbcTemplateResponseTable.query(sql, new ResultSetExtractor<String>() {
				@Override
				public String extractData(ResultSet rs) throws SQLException, DataAccessException {
					return rs.next() ? rs.getString(1) : null;
				}
			});
			if (resultCode == null || resultSite == null) {
				descriptor = "INVALID_DATA";

			} else
				descriptor = resultSite + resultCode;
		} catch (Exception e) {
			throw e;
		}
		return descriptor;
	}

	/**
	 * @param id
	 * @param SourceSys
	 * @param MessageType
	 * @param ActionType
	 * @param Payload
	 * @param sapId
	 * @param valid
	 * @param operationId
	 * @param muleContext
	 */
	public void insertIntoMessageTable(String id, String SourceSys, String MessageType, String ActionType,
			String Payload, String sapId, String valid, String operationId, MuleContext muleContext) {
		try {
			DbConfigResolver dbConfigResolver = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSource = dbConfigResolver.resolve(null).getDataSource();
			JdbcTemplate jdbcTemplateResponseTable = new JdbcTemplate(dataSource);

			String sqlInsertion = "INSERT INTO message(esbid,sourcesys,messagetype,actiontype,payload,sapid,valid,operationid) VALUES ("
					+ "'" + id + "'," + "'" + SourceSys + "'," + "'" + MessageType + "'," + "'" + ActionType + "',"
					+ "'" + Payload.replace("'", "''") + "'," + "'" + sapId + "'" + "," + "'" + valid + "'" + "," + "'"
					+ operationId + "'" + ")";
			logger.info(new UMSTimeDateLog().getTimeDateLog() + " sqlInsertion \t" + sqlInsertion);
			jdbcTemplateResponseTable.execute(sqlInsertion);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}