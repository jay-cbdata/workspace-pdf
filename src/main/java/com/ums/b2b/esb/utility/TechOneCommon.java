package com.ums.b2b.esb.utility;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.service.workorder.UnprocessedMessageFault_Exception;

/**
 * @author Sankalp
 *
 */
public class TechOneCommon {

	protected final static Log logger = LogFactory.getLog(TechOneCommon.class);

	/**
	 * 
	 */
	public TechOneCommon() {
		super();

	}

	/**
	 * @param status
	 * @return String
	 */
	public String getStatus(String status) {
		if (status.equals("COMP"))
			status = "Work Completed";
		if (status.equals("APPR"))
			status = "In Progress";
		if (status.equals("WFAP"))
			status = "Additional Approval Required";
		if (status.equals("INCC"))
			status = "Incorrect Contractor Assigned";
		if (status.equals("MDEF"))
			status = "Maintenance Deferred";
		if (status.equals("PROG"))
			status = "Work Programmed and Waiting";
		if (status.equals("REJ"))
			status = "Rejected";
		return status;
	}

	/**
	 * @param cause
	 * @return String
	 */
	public String getCause(String cause) {
		if (cause.equals("C004"))
			cause = "Inappropriate design";
		if (cause.equals("C003"))
			cause = "Poor materials/ workmanship";
		if (cause.equals("C002"))
			cause = "Natural event";
		if (cause.equals("C001"))
			cause = "Normal wear and tear";
		if (cause.equals("C005"))
			cause = "Vandalism";
		if (cause.equals("C006"))
			cause = "Accidental damage";
		if (cause.equals("C007"))
			cause = "Deferred maintenance";
		if (cause.equals("C998"))
			cause = "Regular Compliance Work";
		if (cause.equals("C999"))
			cause = "Regular Planned Work";
		return cause;
	}

	/**
	 * @param orderType
	 * @return String
	 */
	public String getWorkTypeCode(String orderType) throws UnprocessedMessageFault_Exception {
		switch (orderType) {
		case "ZPM1":
			orderType = "WT001";
			break;

		case "ZPM2":
			orderType = "WT002";
			break;

		case "ZPM3":
			orderType = "WT003";
			break;

		case "ZPM4":
			orderType = "WT004";
			break;
		default:
			throw new UnprocessedMessageFault_Exception("invalid OrderType.");
		}
		return orderType;

	}

	/**
	 * @param activityType
	 * @param orderType
	 * @return String
	 * @throws UnprocessedMessageFault_Exception
	 */
	public String getMapppedActivityType(String activityType, String orderType)
			throws UnprocessedMessageFault_Exception {
		String result = null;
		switch (activityType) {
		case "INS":

			if (orderType.equalsIgnoreCase("ZPM1")) {
				result = "ACT001";
			} else if (orderType.equalsIgnoreCase("ZPM3")) {
				result = "ACT008";
			}
			break;

		case "SER":
			if (orderType.equalsIgnoreCase("ZPM1")) {
				result = "ACT003";
			} else if (orderType.equalsIgnoreCase("ZPM3")) {
				result = "ACT010";
			}
			break;

		case "REP":
			return "ACT002";
		case "ENV":
			result = "ACT004";
			break;

		case "HEA":
			result = "ACT005";
			break;

		case "SAF":
			result = "ACT006";
			break;

		case "DAT":
			result = "ACT007";
			break;

		case "MNT":
			result = "ACT009";
			break;
		case "NEW":
			result = "ACT011";
			break;
		case "RNW":
			result = "ACT012";
			break;

		default:
			throw new UnprocessedMessageFault_Exception("invalid ActivityType.");
		}
		return result;
	}

	/**
	 * @param code
	 * @return String
	 * @throws UnprocessedMessageFault_Exception
	 */
	public String getPriorityCode(String code) throws UnprocessedMessageFault_Exception {
		switch (code) {
		case "1":
			code = "Crtitical";
			break;

		case "3":
			code = "Urgent";
			break;

		case "5":
			code = "Normal";
			break;

		case "7":
			code = "Low";
			break;
		default:
			code = "NA";
		}
		return code;

	}

	/**
	 * @param orderType
	 * @return String
	 */
	public String getRiskCode(String risk) throws UnprocessedMessageFault_Exception {
		switch (risk) {
		case "Asbestos":
			risk = "ASB";
			break;

		case "Flooding":
			risk = "FLD";
			break;

		case "Isolate and Minimise":
			risk = "ISO";
			break;

		case "Eliminate":
			risk = "ELM";
			break;
		default:
			risk = "N/A";
		}
		return risk;

	}

	/**
	 * @param orderType
	 * @return String
	 */
	public String getRiskCodeGroup(String risk) throws UnprocessedMessageFault_Exception {
		switch (risk) {
		case "ASB":
			risk = "ZPM-HAZ";
			break;

		case "FLD":
			risk = "ZPM-HAZ";
			break;

		case "ISO":
			risk = "ZPM-HAZ";
			break;

		case "ELM":
			risk = "ZPM-HAZ";
			break;
		default:
			risk = "N/A";;
		}
		return risk;

	}

	/**
	 * @param cause
	 * @return String
	 */
	public String getCausePdf(String cause) {
		if (cause.equals("Inappropriate design"))
			cause = "C004";
		if (cause.equals("Poor materials/ workmanship"))
			cause = "C003";
		if (cause.equals("Natural event"))
			cause = "C002";
		if (cause.equals("Normal wear and tear"))
			cause = "C001";
		if (cause.equals("Vandalism"))
			cause = "C005";
		if (cause.equals("Accidental damage"))
			cause = "C006";
		if (cause.equals("Deferred maintenance"))
			cause = "C007";
		if (cause.equals("Regular Compliance Work"))
			cause = "C998";
		if (cause.equals("Regular Planned Work"))
			cause = "C999";
		return cause;
	}

	
	/**
	 * @param activityType
	 * @param orderType
	 * @return String
	 * @throws UnprocessedMessageFault_Exception
	 */
	public String mapActivityType(String activityType, String orderType) throws UnprocessedMessageFault_Exception {
		String result = null;
		switch (activityType) {
		case "INS":

			if (orderType.equalsIgnoreCase("ZPM1")) {
				result = "ACT001";
			} else if (orderType.equalsIgnoreCase("ZPM3")) {
				result = "ACT008";
			}
			break;

		case "SER":
			if (orderType.equalsIgnoreCase("ZPM1")) {
				result = "ACT003";
			} else if (orderType.equalsIgnoreCase("ZPM3")) {
				result = "ACT010";
			}
			break;

		case "ENV":
			result = "ACT004";
			break;

		case "HEA":
			result = "ACT005";
			break;

		case "SAF":
			result = "ACT006";
			break;

		case "DAT":
			result = "ACT007";
			break;

		case "MNT":
			result = "ACT009";
			break;
		case "NEW":
			result = "ACT011";
			break;
		case "RNW":
			result = "ACT012";
			break;

		default:
			throw new UnprocessedMessageFault_Exception("invalid ActivityType.");
		}
		return result;
	}

	/**
	 * @param orderType
	 * @return String
	 * @throws UnprocessedMessageFault_Exception
	 */
	public String mapOrderType(String orderType) throws UnprocessedMessageFault_Exception {
		switch (orderType) {
		case "ZPM1":
			orderType = "WT001";
			break;

		case "ZPM2":
			orderType = "WT002";
			break;

		case "ZPM3":
			orderType = "WT003";
			break;

		case "ZPM4":
			orderType = "WT004";
			break;
		default:
			throw new UnprocessedMessageFault_Exception("invalid OrderType.");
		}
		return orderType;
	}

	/**
	 * @param priority
	 * @return String
	 */
	public String mapPriority(String priority) {
		switch (priority) {
		case "1":
			return "Critical";

		case "3":
			return "Urgent";

		case "5":
			return "Normal";

		case "7":
			return "Low";

		default:
			return "NA";

		}
	}
	
	/**
	 * @param priority
	 * @return String
	 * @throws UnprocessedMessageFault_Exception
	 */
	public String getPriority(String priority) throws UnprocessedMessageFault_Exception {
		switch (priority) {
		case "Critical":
			priority = "1";
			break;

		case "Urgent":
			priority = "3";
			break;

		case "Normal":
			priority = "5";
			break;

		case "Low":
			priority = "7";
			break;
		default:
			priority = "N/A";
		}
		return priority;

	}

}
