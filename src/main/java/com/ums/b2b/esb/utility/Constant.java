package com.ums.b2b.esb.utility;

/**
 * @author Sankalp
 *
 */
public class Constant {

	public static String HEADER_WORKORDER = "HEADER_WORKORDER";
	public static String SUBORDER_WORKORDER = "SUB_WORKORDER";
	
	public static String EXTERNAL_REF_PDF = "PDF";
	public static String EXTERNAL_REF_B2B = "B2B";
	
	public static String LEVEL_TYPE_HEADER_WORK_ORDER = "H";
	public static String LEVEL_TYPE_SUB_WORK_ORDER = "S";
	
	public static String CREATE = "CREATE";
	public static String READFROMAPI = "READFROMAPI";
	public static String CREATEFROMTEMPLATE = "CREATEFROMTEMPLATE";
	public static String SAVEFORDETAILWITHCF = "SAVEFORDETAILWITHCF";
	public static String FALSE = "FALSE";
	public static String DUP_SAP_WO_ID = "DUP_SAP_WO_ID";
	public static String DUP_SAP_OR_ID = "Duplicate SAP Order ID";
	public static String UPD_SAP_WO = "SAP WorkOrder does not exist";
	public static String SAP_WO_NOT_FOUND = "SAP_WO_NOT_FOUND";
	public static String INVALID_SUPPLIER_ID = "INVALID_SUPPLIER_ID";
	public static String INVALID_SUPPLIER_TEXT="Invalid Supplier Id specified";
	
	
}
