package com.ums.b2b.esb.utility;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class UMSTimeDateLog {

	public String  getTimeDateLog(){
		return ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME);
	}
}
