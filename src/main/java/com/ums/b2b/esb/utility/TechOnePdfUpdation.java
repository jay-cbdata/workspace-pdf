package com.ums.b2b.esb.utility;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mule.api.MuleContext;
import org.mule.api.annotations.expressions.Lookup;
import org.mule.api.context.MuleContextAware;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.forms.xfa.XfaForm;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.StampingProperties;

public class TechOnePdfUpdation implements MuleContextAware {

	protected final static Log logger = LogFactory.getLog(TechOnePdfUpdation.class);

	String destination = System.getProperty("tempxmlfolder");
	String xmlExtension = System.getProperty("fileextension");
	@Lookup
	public MuleContext muleContext;

	@Override
	public void setMuleContext(MuleContext muleContext) {
		this.muleContext = muleContext;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String updatePdfDocument(String str, String workOrderNo, MuleContext smuleContext) {

		PdfReader pdfReader = null;
		PdfDocument pdfDoc = null;
		Map setMappedValues = new HashMap();
		StringWriter writer = new StringWriter();
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Document doc, docPdfXml;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = null;
			try {
				builder = factory.newDocumentBuilder();
			} catch (ParserConfigurationException e2) {
				e2.printStackTrace();
			}
			try {
				doc = builder.parse(new InputSource(new StringReader(str)));
				Transformer transformer = null;
				try {
					transformer = transformerFactory.newTransformer();
				} catch (TransformerConfigurationException e) {
					e.printStackTrace();
				}
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				try {
					transformer.transform(new DOMSource(doc), new StreamResult(writer));
				} catch (javax.xml.transform.TransformerException e) {
					e.printStackTrace();
				}
				NodeList nodeList = doc.getElementsByTagName("*");
				for (int i = 0; i < nodeList.getLength(); i++) {
					Node node = nodeList.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						if (node.getNodeName().equals("activityType"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getNodeValue());
						if (node.getNodeName().equals("status"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("supplierId"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("description"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("workDescription"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("address"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("orderType"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("requiredFinish"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("requiredStart"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("releaseDate"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("orderId"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("purchaseOrderId"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
						if (node.getNodeName().equals("locationId"))
							setMappedValues.put(node.getNodeName(), node.getFirstChild().getTextContent());
					}
				}
				String outputPdfXmlL = new DbUtil().getPdfDataToUpdate(workOrderNo, smuleContext);
				docPdfXml = builder.parse(new InputSource(new StringReader(outputPdfXmlL)));
				NodeList nodePdfList = docPdfXml.getElementsByTagName("*");
				for (int i = 0; i < nodePdfList.getLength(); i++) {
					Node node = nodePdfList.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						if (node.getNodeName().equals("PM_ACTIVITY") && node.getFirstChild() != null) {
							if (!setMappedValues.get("activityType").equals(node.getFirstChild().getTextContent()))
								node.getFirstChild().setTextContent((String) setMappedValues.get("activityType"));
						}
						if (node.getNodeName().equals("STATUS") && node.getFirstChild() != null) {
							if (!setMappedValues.get("status").equals(
									new TechOneCommon().getStatus(node.getFirstChild().getTextContent().toString())))
								node.getFirstChild().setTextContent((String) setMappedValues.get("status"));
						}
						if (node.getNodeName().equals("VENDOR") && node.getFirstChild() != null) {
							if (!setMappedValues.get("supplierId")
									.equals(node.getFirstChild().getTextContent().toString()
											.substring(node.getFirstChild().getTextContent().toString().indexOf("/"))))
								node.getFirstChild().setTextContent((String) setMappedValues.get("supplierId"));
						}
						if (node.getNodeName().equals("RELEASED_DATE") && node.getFirstChild() != null) {
							if (!setMappedValues.get("releaseDate").equals(node.getFirstChild().getTextContent()))
								node.getFirstChild().setTextContent((String) setMappedValues.get("releaseDate"));
						}
						if (node.getNodeName().equals("PROBLEM_DESC") && node.getFirstChild() != null) {
							if (!setMappedValues.get("workDescription").equals(node.getFirstChild().getTextContent()))
								node.getFirstChild().setTextContent((String) setMappedValues.get("workDescription"));
						}
						if (node.getNodeName().equals("ADDRESS_TEXT") && node.getFirstChild() != null) {
							if (!setMappedValues.get("address").equals(node.getFirstChild().getTextContent()))
								node.getFirstChild().setTextContent((String) setMappedValues.get("address"));
						}
						if (node.getNodeName().equals("ASSET_ID") && node.getFirstChild() != null) {
							String locId = setMappedValues.get("locationId").toString().trim().substring(1, 8);
							if (!locId.trim().equals(node.getFirstChild().getTextContent()))
								node.getFirstChild().setTextContent((String) setMappedValues.get("locationId"));
						}
						if (node.getNodeName().equals("PO_NUMBER_TEXT") && node.getFirstChild() != null) {
							if (!setMappedValues.get("purchaseOrderId").equals(node.getFirstChild().getTextContent()))
								node.getFirstChild()
								.setTextContent((String) setMappedValues.get("purchaseOrderId")
										+ node.getFirstChild().getTextContent().toString().substring(
												node.getFirstChild().getTextContent().toString().indexOf(" ")));
						}
					}
				}
				String xmlString = org.apache.ws.security.util.XMLUtils.PrettyDocumentToString(docPdfXml);
				FileOutputStream os = null;
				os = new FileOutputStream(destination + "\\" + setMappedValues.get("orderId".toString() + ".xml"));
				os.write(xmlString.getBytes());
				os.flush();
				os.close();
				pdfReader = new PdfReader(System.getProperty("tempxmlfolder") + "\\" + "SampleDoc.pdf");
				pdfDoc = new PdfDocument(pdfReader,
						new PdfWriter(new PdfWriter(System.getProperty("output.folder") + "\\" + workOrderNo + ".pdf")),
						new StampingProperties().preserveEncryption());
				PdfAcroForm form = PdfAcroForm.getAcroForm(pdfDoc, false);
				XfaForm xfa = form.getXfaForm();
				xfa.fillXfaForm(
						new FileInputStream(destination + "\\" + setMappedValues.get("orderId".toString() + ".xml")));
				xfa.write(pdfDoc);
				pdfDoc.close();
			} catch (SAXException e1) {
				e1.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}

}
