package com.ums.b2b.esb.serviceimpl.aklc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.annotation.Resource;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.WebServiceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.annotations.expressions.Lookup;
import org.mule.api.context.MuleContextAware;

import com.ums.b2b.esb.utility.UMSTimeDateLog;

/**
 * @author Sankalp
 *
 */
public class UpdateServiceIdImpl implements org.mule.api.lifecycle.Callable, MuleContextAware {
	@Lookup
	private MuleContext muleContext;
	@Resource
	private WebServiceContext webServiceContext;

	protected final static Log logger = LogFactory.getLog(UpdateServiceIdImpl.class);

	/**
	 * @param notes
	 * @param headerWorkOrderNumber
	 * @return String
	 */
	public String updateServiceIds(String notes, String headerWorkOrderNumber) {
		String strMsg = null;
		SOAPConnectionFactory soapConnectionFactory;
		try {

			soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			SOAPMessage soapResponse = null;
			try {
				soapResponse = soapConnection.call(updateServiceId(notes, headerWorkOrderNumber),
						System.getProperty("techoneserviceupdatedefaulturl"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				soapResponse.writeTo(out);
			} catch (IOException e) {
				e.printStackTrace();
			}
			strMsg = new String(out.toByteArray());
			logger.info(
					new UMSTimeDateLog().getTimeDateLog() + " SOAP Response from  updateServiceId(s) for Notes  \t ");
			soapResponse.writeTo(System.out);
			out.close();
			soapConnection.close();
		} catch (Exception r) {
			r.printStackTrace();
		}
		return strMsg;
	}

	/**
	 * @param notes
	 * @param headerWorkOrderNumber
	 * @return SOAPMessage
	 * @throws Exception
	 */
	private static SOAPMessage updateServiceId(String notes, String headerWorkOrderNumber) throws Exception {
		SOAPMessage soapMessage = null;
		MessageFactory messageFactory = MessageFactory.newInstance();
		soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String serverURI = System.getProperty("techoneserviceuri");
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("ser", serverURI);
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("Task_DoUpdateV2", "ser");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("Request", "ser");
		SOAPElement auth = soapBodyElem1.addChildElement("Auth", "ser");
		auth.setAttribute("UserId", System.getProperty("usernameupdateidservice"));
		auth.setAttribute("Password", System.getProperty("passwdupdateidservice"));
		auth.setAttribute("Config", System.getProperty("umsceswsrole"));
		auth.setAttribute("FunctionName", "$W1.TSK.UPDATE2.WS");
		SOAPElement task = soapBodyElem1.addChildElement("Task", "ser");
		task.setAttribute("WorkSystemName", "ACNZ_WORKS");
		task.setAttribute("TaskNumber", headerWorkOrderNumber);
		task.setAttribute("PerformReadAttachments", "true");
		task.setAttribute("PerformAttachmentUpload", "false");
		SOAPElement soapBodyElem2 = task.addChildElement("AttachmentsV2", "ser");
		SOAPElement service = soapBodyElem2.addChildElement("Attachment", "ser");
		service.setAttribute("SysKey", "");
		service.setAttribute("SequenceNbr", "");
		service.setAttribute("AttLabel", "SAP Work Order description");
		service.setAttribute("AttTypeInd", "N");
		service.setAttribute("NotesData", "SAP Work Order description " + notes);
		service.setAttribute("UniqueNbr", "");
		service.setAttribute("EntityType", "WRKTask");
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "http://TechnologyOneCorp.com/T1.W1.Public/Services/Task_DoUpdateV2");
		soapMessage.saveChanges();
		logger.info(new UMSTimeDateLog().getTimeDateLog() + " SOAP Message method Request to serviceId(s) Notes  \t");
		soapMessage.writeTo(System.out);

		return soapMessage;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.mule.api.lifecycle.Callable#onCall(org.mule.api.MuleEventContext)
	 */
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		return eventContext.getMessage().getPayload();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mule.api.context.MuleContextAware#setMuleContext(org.mule.api.
	 * MuleContext)
	 */
	public void setMuleContext(MuleContext muleContext) {
		this.muleContext = muleContext;
	}

}
