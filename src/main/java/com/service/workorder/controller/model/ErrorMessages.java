package com.service.workorder.controller.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorMessages {

	@JsonProperty("Code")
	private String code;
	@JsonProperty("Message")
	private String message;
	@JsonProperty("NotificationType")
	private String notificationType;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public ArrayList<ErrorMessages> filterErrorMessages(Response resobjest) throws Exception {

		try {
			ArrayList<ErrorMessages> messageList = (ArrayList<ErrorMessages>) resobjest.getErrorMessages();

			if(messageList != null ){
				ArrayList<ErrorMessages> errorMessages = (ArrayList<ErrorMessages>) messageList.stream()
						.filter(x -> "Error".equals(x.getNotificationType())).collect(Collectors.toList());
				// System.out.println(messageList);
				return errorMessages;
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return new ArrayList<ErrorMessages>();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return " Code:" + this.code + " Message:" + this.message + " NotificationType:" + this.notificationType + " ";
	}

}
