package com.service.workorder.controller.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)

public class Response {

	@JsonProperty("Messages")
	private List<ErrorMessages> errorMessages = null;
	@JsonProperty("WorkSystemName")
	private String workSystemName;
	@JsonProperty("WorkOrderNumber")
	private String workOrderNumber;
	@JsonProperty("WorkOrderNumberInternal")
	private String workOrderNumberInternal;
	@JsonProperty("KeyedWorkOrderNumber")
	private String keyedWorkOrderNumber;
	@JsonProperty("Vers")
	private Integer vers;
	@JsonProperty("Key")
	private String key;
	public List<ErrorMessages> getErrorMessages() {
		return errorMessages;
	}
	public void setErrorMessages(List<ErrorMessages> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public String getWorkSystemName() {
		return workSystemName;
	}
	public void setWorkSystemName(String workSystemName) {
		this.workSystemName = workSystemName;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public String getWorkOrderNumberInternal() {
		return workOrderNumberInternal;
	}
	public void setWorkOrderNumberInternal(String workOrderNumberInternal) {
		this.workOrderNumberInternal = workOrderNumberInternal;
	}
	public Integer getVers() {
		return vers;
	}
	public void setVers(Integer vers) {
		this.vers = vers;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getKeyedWorkOrderNumber() {
		return keyedWorkOrderNumber;
	}
	public void setKeyedWorkOrderNumber(String keyedWorkOrderNumber) {
		this.keyedWorkOrderNumber = keyedWorkOrderNumber;
	}

}
