
package com.service.workorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.ums.b2b.esb.entity.Outcome;


/**
 * <p>Java class for Summary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Summary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fault" type="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}Code" minOccurs="0"/>
 *         &lt;element name="problem" type="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}Code" minOccurs="0"/>
 *         &lt;element name="object" type="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}Code" minOccurs="0"/>
 *         &lt;element name="cause" type="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}Code" minOccurs="0"/>
 *           &lt;element name="outcome" type="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}Code" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Summary", propOrder = {
    "fault",
    "problem",
    "object",
    "cause",
    "outcome"
})
public class Summary {

    protected Code fault;
    protected Code problem;
    protected Code object;
    protected Code cause;
    protected Outcome outcome;
    /**
     * Gets the value of the fault property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getFault() {
        return fault;
    }

    /**
     * Sets the value of the fault property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setFault(Code value) {
        this.fault = value;
    }

    /**
     * Gets the value of the problem property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getProblem() {
        return problem;
    }

    /**
     * Sets the value of the problem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setProblem(Code value) {
        this.problem = value;
    }

    /**
     * Gets the value of the object property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getObject() {
        return object;
    }

    /**
     * Sets the value of the object property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setObject(Code value) {
        this.object = value;
    }

    /**
     * Gets the value of the cause property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
    public Code getCause() {
        return cause;
    }

    /**
     * Sets the value of the cause property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setCause(Code value) {
        this.cause = value;
    }
	public Outcome getOutcome() {
		return outcome;
	}
	public void setOutcome(Outcome outcome) {
		this.outcome = outcome;
	}
}
