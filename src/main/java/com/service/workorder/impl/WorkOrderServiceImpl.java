package com.service.workorder.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.TimeZone;
import java.util.function.Supplier;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.ws.WebServiceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.annotations.expressions.Lookup;
import org.mule.api.context.MuleContextAware;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.service.workorder.InvalidMessageFault;
import com.service.workorder.InvalidMessageFault_Exception;
import com.service.workorder.Operation;
import com.service.workorder.UnprocessedMessageFault_Exception;
import com.service.workorder.WorkOrder;
import com.service.workorder.WorkOrderResponse;
import com.service.workorder.WorkOrderService;
import com.service.workorder.controller.model.ErrorMessages;
import com.service.workorder.controller.model.Response;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ums.b2b.esb.serviceimpl.aklc.UpdateServiceIdImpl;
import com.ums.b2b.esb.utility.Constant;
import com.ums.b2b.esb.utility.DbUtil;
import com.ums.b2b.esb.utility.TechOneCommon;
import com.ums.b2b.esb.utility.UMSTimeDateLog;

public class WorkOrderServiceImpl implements WorkOrderService, org.mule.api.lifecycle.Callable, MuleContextAware {
	@Lookup
	private MuleContext muleContext;
	@Resource
	private WebServiceContext webServiceContext;
	private String esbId = org.mule.util.UUID.getUUID();
	String status = null;
	String templateWorkOrderNumber = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mule.api.context.MuleContextAware#setMuleContext(org.mule.api.
	 * MuleContext)
	 */
	public void setMuleContext(MuleContext muleContext) {
		this.muleContext = muleContext;
	}

	protected final static Log logger = LogFactory.getLog(WorkOrderServiceImpl.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see com.service.workorder.WorkOrderService#updateWorkOrder(com.service.
	 * workorder.WorkOrder)
	 */
	@SuppressWarnings("static-access")
	@Override
	public WorkOrderResponse updateWorkOrder(WorkOrder workOrderRequest)
			throws InvalidMessageFault_Exception, UnprocessedMessageFault_Exception {
		WorkOrderResponse wor = new WorkOrderResponse();
		StringWriter workOrderReq = new StringWriter();
		JAXBContext jaxbContext = null;
		try {
			jaxbContext = JAXBContext.newInstance(WorkOrder.class);
		} catch (JAXBException e1) {
			e1.printStackTrace();
		}
		Marshaller jaxbMarshaller = null;
		try {
			jaxbMarshaller = jaxbContext.createMarshaller();
		} catch (JAXBException e1) {
			e1.printStackTrace();
		}
		try {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		} catch (PropertyException e1) {
			e1.printStackTrace();
		}
		try {
			jaxbMarshaller.marshal(workOrderRequest, workOrderReq);
		} catch (JAXBException e1) {
			e1.printStackTrace();
		}
		new DbUtil().insertIntoMessageTable(esbId, "SAP", "WorkOrder", "Update", workOrderReq.toString(),
				workOrderRequest.getOrderId(), "Valid", "", muleContext);
		logger.info(new UMSTimeDateLog().getTimeDateLog()
				+ "  Original Request comes from Council for UpdateWorkOrder  /t " + workOrderReq.toString());
		if (!new DbUtil().getOrderIdToUpdate(workOrderRequest.getOrderId(), muleContext).equals("FAILURE")) {
			logger.info(new UMSTimeDateLog().getTimeDateLog()
					+ " Original Request comes from Council eligible for Update process   \t "
					+ workOrderReq.toString());
			if (!workOrderRequest.getSupplierId().equals(System.getProperty("supplierid"))) {
				logger.info(new UMSTimeDateLog().getTimeDateLog()
						+ " Supplier ID does not matched with the System configured Supplier ID ");
				InvalidMessageFault invalidMessageFault = new InvalidMessageFault();
				invalidMessageFault.setFaultCode(Constant.INVALID_SUPPLIER_ID);
				invalidMessageFault.setFaultText(Constant.INVALID_SUPPLIER_TEXT);
				invalidMessageFault.setFaultReference(workOrderRequest.getOrderId());
				wor.setInvalidMessageFault(invalidMessageFault);
				return wor;
			}
		} else {
			logger.info(new UMSTimeDateLog().getTimeDateLog() + " Call not accepted OrderId already  exist ");
			InvalidMessageFault invalidMessageFault = new InvalidMessageFault();
			invalidMessageFault.setFaultCode(Constant.SAP_WO_NOT_FOUND);
			invalidMessageFault.setFaultText(Constant.UPD_SAP_WO);
			invalidMessageFault.setFaultReference(workOrderRequest.getOrderId());
			wor.setInvalidMessageFault(invalidMessageFault);
			return wor;
		}
		return processWorkOrder(workOrderRequest);
	}
	/*
	 * Task #2 - please keep this section. Update work order is a two step
	 * process, SAP will pass sub work order Id, as Task #1 sends them sub work
	 * order id
	 * 
	 * ===================================================================== 1.
	 * Log SOAP Request into Message Table SourceSys='SAP', Action='Update',
	 * MessageType='WorkORder', payload=soapRequest 2. Validate and update
	 * status in Message Table 3. If valid 2. CALL - Tech One ReadForDetails
	 * {workOrderRequest.getSupplierOrderId() } This will return SUB WORK ORDER
	 * DETALS (once Task 1 is implemented) 2. Log response from Tech One to
	 * Message Response, TargetSys='TECHONE_TO_ESB' 3. STORE - headerWorkOrderId
	 * returned from response of part 1 4. CALL - Techone.SaveForDetailsWithCF
	 * after updating data from <operation> of the request. 5. Log response from
	 * Tech One to Message Response, TargetSys='TECHONE_TO_ESB'
	 * 
	 * 6. CALL - ReadForDetails { headerWorkOrderId } & log to message response
	 * 7. CALL - SaveForDetailsWithCF after updating data from
	 * <workOrderRequest> & l 8. return work order response
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.service.workorder.WorkOrderService#createWorkOrder(com.service.
	 * workorder.WorkOrder)
	 */
	@Override
	public WorkOrderResponse createWorkOrder(WorkOrder workOrder)
			throws UnprocessedMessageFault_Exception, InvalidMessageFault_Exception {
		status = workOrder.getStatus().toString();
		String locationId[] = workOrder.getLocationId().toString().split("-");
		String orderId = workOrder.getOrderId();
		String templateWorkOrderNum = null;
		String templateOrderTypeCode = null;
		String supplierOrderId = null;
		String detail = null;
		String notes = null;
		WorkOrderResponse wor = new WorkOrderResponse();

		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(WorkOrder.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(workOrder, sw);
			if (locationId[0] != null && locationId[0] != "") {
				templateWorkOrderNum = new DbUtil().getLocationBoardDesc(locationId[0].toString(), muleContext);
				if (templateWorkOrderNum.equalsIgnoreCase("FAILURE")) {
					InvalidMessageFault invalidMessageFault = new InvalidMessageFault();
					invalidMessageFault.setFaultText("Invalid Location");
					invalidMessageFault.setFaultReference(workOrder.getOrderId());
					wor.setInvalidMessageFault(invalidMessageFault);
					new DbUtil().insertIntoMessageTable(esbId, "SAP", "WorkOrder", "Create", sw.toString(), orderId,
							"InValid", "", muleContext);
					return wor;

				}
				if (workOrder.getOrderType().toString() != null && workOrder.getOrderType().toString() != "") {
					templateOrderTypeCode = new DbUtil().getOrderType(workOrder.getOrderType().toString(), muleContext);
					templateWorkOrderNumber = templateWorkOrderNum.trim().toString()
							+ templateOrderTypeCode.trim().toString();
					templateWorkOrderNumber.replaceAll("\\s{2,}", " ").trim();
				}
			}

			logger.info(
					new UMSTimeDateLog().getTimeDateLog() + " Template Work Order number \t" + templateWorkOrderNumber);

			if (new DbUtil().getOrderIdToUpdate(workOrder.getOrderId(), muleContext).equals("FAILURE")) {
				if (!workOrder.getSupplierId().equals(System.getProperty("supplierid"))) {
					logger.info(new UMSTimeDateLog().getTimeDateLog()
							+ " Supplier ID does not matched with the System configured Supplier ID ");
					InvalidMessageFault invalidMessageFault = new InvalidMessageFault();
					invalidMessageFault.setFaultCode(Constant.INVALID_SUPPLIER_ID);
					invalidMessageFault.setFaultText(Constant.INVALID_SUPPLIER_TEXT);
					invalidMessageFault.setFaultReference(workOrder.getOrderId());
					wor.setInvalidMessageFault(invalidMessageFault);
					new DbUtil().insertIntoMessageTable(esbId, "SAP", "WorkOrder", "Create", sw.toString(), orderId,
							"InValid", "", muleContext);
					return wor;
				}
				if (workOrder.getWorkDescription().length() > 40) {

				}
				new DbUtil().insertIntoMessageTable(esbId, "SAP", "WorkOrder", "Create", sw.toString(), orderId,
						"Valid", "", muleContext);
				logger.info(new UMSTimeDateLog().getTimeDateLog() + "  New Create WorkOrder Request recieved \t"
						+ sw.toString());

				int lengthOfWorkDescription = workOrder.getWorkDescription().length();
				if (lengthOfWorkDescription != 0 && lengthOfWorkDescription < 41) {
					String temp = workOrder.getWorkDescription();
					workOrder.setWorkDescription(workOrder.getWorkDescription().substring(0, lengthOfWorkDescription));
					detail = temp.substring(0, lengthOfWorkDescription);
					notes = temp.substring(0, lengthOfWorkDescription);
				} else if (lengthOfWorkDescription != 0 && lengthOfWorkDescription < 1001) {
					String temp = workOrder.getWorkDescription();
					workOrder.setWorkDescription(workOrder.getWorkDescription().substring(0, 40));
					detail = temp.substring(0, lengthOfWorkDescription);
					notes = temp.substring(0, lengthOfWorkDescription);
				} else {
					String temp = workOrder.getWorkDescription();
					workOrder.setWorkDescription(workOrder.getWorkDescription().substring(0, 40));
					detail = temp.substring(0, 1000);
					notes = temp.substring(0, lengthOfWorkDescription);
				}

				logger.info(new UMSTimeDateLog().getTimeDateLog() + " HeaderWorkOrder callToTechone Started.");
				callToTechone(workOrder, null, templateWorkOrderNumber.toUpperCase().trim(), Constant.HEADER_WORKORDER);

				logger.info(
						new UMSTimeDateLog().getTimeDateLog() + " HeaderWorkOrder callToTechoneReadForDetail Started.");
				JSONObject jsonObjectReadWorkOrder = callToTechoneReadForDetail(workOrder, null,
						Constant.HEADER_WORKORDER);

				logger.info(new UMSTimeDateLog().getTimeDateLog() + " HeaderWorkOrder saveForDetailWithCF Started.");
				saveForDetailWithCF(workOrder, detail, null, Constant.HEADER_WORKORDER);

				logger.info(
						new UMSTimeDateLog().getTimeDateLog() + " HeaderWorkOrder restWorkOrderApproveWithCF Started.");
				approveWorkOrderWithCF(workOrder, null, Constant.HEADER_WORKORDER);
				new UpdateServiceIdImpl().updateServiceIds(notes,
						jsonObjectReadWorkOrder.get("HeaderWorkOrderNumber").toString());
				List<Operation> operations = workOrder.getOperation();

				if (operations.size() > 0) {
					for (Operation operation : operations) {

						logger.info(new UMSTimeDateLog().getTimeDateLog() + " SubWorkOrder callToTechone Started.");
						callToTechone(workOrder, operation, templateWorkOrderNumber.toUpperCase().trim(),
								Constant.SUBORDER_WORKORDER);

						logger.info(new UMSTimeDateLog().getTimeDateLog()
								+ " SubWorkOrder callToTechoneReadForDetail Started.");

						JSONObject jsonObjectReadSubWorkOrder = callToTechoneReadForDetail(workOrder, operation,
								Constant.SUBORDER_WORKORDER);

						logger.info(
								new UMSTimeDateLog().getTimeDateLog() + " SubWorkOrder saveForDetailWithCF Started.");
						saveForDetailWithCF(workOrder, detail, operation, Constant.SUBORDER_WORKORDER);

						logger.info(new UMSTimeDateLog().getTimeDateLog()
								+ " SubWorkOrder restWorkOrderApproveWithCF Started.");
						// Task #1: assign supplierWorkOrderId =
						// response.WorkOrderid
						supplierOrderId = approveWorkOrderWithCF(workOrder, operation, Constant.SUBORDER_WORKORDER);
						new UpdateServiceIdImpl().updateServiceIds(notes,
								jsonObjectReadSubWorkOrder.get("HeaderWorkOrderNumberInternal").toString());
					}
				}
				wor.setOrderId(workOrder.getOrderId());
				wor.setSupplierId(System.getProperty("supplierId"));
				wor.setSupplierOrderId(workOrder.getWorkOrderNumber());
				wor.setStatus(status);
				logger.info(new UMSTimeDateLog().getTimeDateLog() + " Capturing CreateTemplate Response");

				try {

					// Task #1: use supplierWorkOrderId instead of
					// workOrder.getWorkOrderNumber() below
					//
					new DbUtil().insertInMessageResponseTable(esbId, supplierOrderId, // TODO
							// #1
							// see
							// above
							// comment
							// line
							// 193,
							// 167,
							// 168
							"SUCCESS", new XmlMapper().writeValueAsString(wor), "Create", "WORKORDER_COMPLETE",
							"ESB_TO_SAP", muleContext);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}

			} else {
				logger.info(new UMSTimeDateLog().getTimeDateLog() + " Call not accepted OrderId already  exist ");
				InvalidMessageFault invalidMessageFault = new InvalidMessageFault();
				invalidMessageFault.setFaultCode(Constant.DUP_SAP_WO_ID);
				invalidMessageFault.setFaultText(Constant.DUP_SAP_OR_ID);
				invalidMessageFault.setFaultReference(workOrder.getOrderId());
				wor.setInvalidMessageFault(invalidMessageFault);
				return wor;
			}
		} catch (Exception e) {
			logger.error(new UMSTimeDateLog().getTimeDateLog() + " failed to create createWorkOrder.");
			e.printStackTrace();
			throw new InvalidMessageFault_Exception(e.getMessage());
		}

		return wor;

	}

	/**
	 * @param workOrderRequest
	 * @return WorkOrderResponse
	 * @throws UnprocessedMessageFault_Exception
	 * @throws InvalidMessageFault_Exception
	 */
	private WorkOrderResponse processWorkOrder(WorkOrder workOrderRequest)
			throws UnprocessedMessageFault_Exception, InvalidMessageFault_Exception {
		WorkOrderResponse wor = new WorkOrderResponse();

		wor.setOrderId(workOrderRequest.getOrderId());
		wor.setSupplierId(workOrderRequest.getSupplierId());
		wor.setSupplierOrderId(workOrderRequest.getSupplierOrderId());
		wor.setStatus(workOrderRequest.getStatus());
		return wor;

		/*
		 * Task #2 - please keep this section. Update work order is a two step
		 * process, SAP will pass sub work order Id, as Task #1 sends them sub
		 * work order id
		 * 
		 * =====================================================================
		 * 1. Log SOAP Request into Message Table SourceSys='SAP',
		 * Action='Update', MessageType='WorkORder', payload=soapRequest 2.
		 * Validate and update status in Message Table 3. If valid 2. CALL -
		 * Tech One ReadForDetails {workOrderRequest.getSupplierOrderId() } This
		 * will return SUB WORK ORDER DETALS (once Task 1 is implemented) 2. Log
		 * response from Tech One to Message Response,
		 * TargetSys='TECHONE_TO_ESB' 3. STORE - headerWorkOrderId returned from
		 * response of part 1 4. CALL - Techone.SaveForDetailsWithCF after
		 * updating data from <operation> of the request. 5. Log response from
		 * Tech One to Message Response, TargetSys='TECHONE_TO_ESB'
		 * 
		 * 6. CALL - ReadForDetails { headerWorkOrderId } & log to message
		 * response 7. CALL - SaveForDetailsWithCF after updating data from
		 * <workOrderRequest> & l 8. return work order response
		 */
	}

	/**
	 * @param workOrderRequest
	 * @param operation
	 * @param templateName
	 * @param rqType
	 * @return String
	 * @throws Exception
	 */
	public String callToTechone(WorkOrder workOrderRequest, Operation operation, String templateName, String rqType)
			throws Exception {
		String response = null;
		String levelType = null;
		String orderId = null;
		if (rqType.equalsIgnoreCase(Constant.HEADER_WORKORDER)) {
			levelType = "H";
			orderId = workOrderRequest.getOrderId();
		} else if (rqType.equalsIgnoreCase(Constant.SUBORDER_WORKORDER)) {
			levelType = "S";
			orderId = operation.getOperationId();
		}
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(System.getProperty("techonecreatefromtemplateurl"));
			String input = "{\"Source\":\"3P\"," + "\"ExternalReference\":\"SAP\","
					+ "\"ExternalReferenceEntityKey1\":\"" + orderId + "\"," + "\"TemplateWorkOrderNumber\":\""
					+ templateName + "\"," + "\"WorkSystemName\":\"ACNZ_WORKS\"," + "\"IsTemplate\":\"FALSE\","
					+ "\"LevelType\":\"" + levelType + "\"," + "\"Status\":\"" + "I" + "\"," + "\"Type\":\"O\","
					+ "\"WorkTypeCode\":\"" + new TechOneCommon().mapOrderType(workOrderRequest.getOrderType()) + " \","
					+ "\"Description\":\"" + workOrderRequest.getWorkDescription() + "\"," + "\"ActivityCode\":\""
					+ new TechOneCommon().mapActivityType(workOrderRequest.getActivityType(),
							workOrderRequest.getOrderType())
					+ addAdditionalProperties(rqType, workOrderRequest.getWorkOrderNumber()) + "\"}";
			ClientResponse responseClient = webResource.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, input);

			new DbUtil().insertIntoMessageTable(esbId, "SAP", rqType, "Create", input, workOrderRequest.getOrderId(),
					"Valid", rqType == Constant.HEADER_WORKORDER ? "" : operation.getOperationId(), muleContext);

			logger.info(new UMSTimeDateLog().getTimeDateLog() + " " + responseClient.getStatus());
			response = responseClient.getEntity(String.class);
			logger.info(new UMSTimeDateLog().getTimeDateLog() + " Input " + rqType
					+ " CreateTemplate to Techone via UMSESB \t" + input);
			logger.info(new UMSTimeDateLog().getTimeDateLog() + " Response " + rqType
					+ " CreateTemplate from TechOne end" + response);

			ObjectMapper mapper = new ObjectMapper();
			Response resobjest = mapper.readValue(response.toString(), Response.class);

			workOrderRequest.setWorkSystemName(resobjest.getWorkSystemName());
			if (rqType.equalsIgnoreCase(Constant.HEADER_WORKORDER)) {
				workOrderRequest.setWorkOrderNumber(resobjest.getWorkOrderNumber());
			} else if (rqType.equalsIgnoreCase(Constant.SUBORDER_WORKORDER)) {
				workOrderRequest.setSubworkOrderNumber(resobjest.getWorkOrderNumber());
			}

			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

			if (errorMessageList.size() > 0) {
				logger.info(new UMSTimeDateLog().getTimeDateLog() + " Errors Found in " + rqType
						+ " CreateTemplate Response");
				new DbUtil().insertInMessageResponseTable(esbId, resobjest.getWorkOrderNumber(), "FAIL", response,
						"Create", rqType, muleContext);
				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				new DbUtil().insertInMessageResponseTable(esbId, resobjest.getWorkOrderNumber(), "SUCCESS", response,
						"Create", rqType, muleContext);
				logger.info(new UMSTimeDateLog().getTimeDateLog() + " No Errors Found in " + rqType
						+ " CreateTemplate  Response " + errorMessageList.toString());
			}
		} catch (Exception e) {
			throw e;
		}

		return response;
	}

	/**
	 * @param workOrderRequest
	 * @param operation
	 * @param rqType
	 * @return String
	 * @throws Exception
	 */
	public JSONObject callToTechoneReadForDetail(WorkOrder workOrderRequest, Operation operation, String rqType)
			throws Exception {
		String response = null;
		JSONObject jsonObject = new JSONObject();

		try {
			String urlOrderNo = rqType.equalsIgnoreCase(Constant.HEADER_WORKORDER)
					? workOrderRequest.getWorkOrderNumber() : workOrderRequest.getSubworkOrderNumber();

			String input = "{\"WorkSystemName\":\"" + workOrderRequest.getWorkSystemName() + "\","
					+ "\"KeyedWorkOrderNumber\":\"" + urlOrderNo + "\"}";

			logger.info(new UMSTimeDateLog().getTimeDateLog() + " Input to " + rqType + " Read Detail Techone API \t"
					+ System.getProperty("techonereadfordetailurl") + workOrderRequest.getWorkSystemName() + "/"
					+ urlOrderNo);

			Client client = Client.create();
			WebResource webResource = client.resource(System.getProperty("techonereadfordetailurl")
					+ workOrderRequest.getWorkSystemName() + "/" + urlOrderNo);

			new DbUtil().insertIntoMessageTable(esbId, "SAP", rqType, "Read", input, workOrderRequest.getOrderId(),
					"Valid", rqType == Constant.HEADER_WORKORDER ? "" : operation.getOperationId(), muleContext);

			ClientResponse responseClient = webResource.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization")).get(ClientResponse.class);
			response = responseClient.getEntity(String.class);
			JSONParser parser = new JSONParser();
			jsonObject = (JSONObject) parser.parse(response.toString());
			workOrderRequest.setReadForDetailJson(jsonObject);
			workOrderRequest.setWorkSystemName(workOrderRequest.getWorkSystemName());
			logger.info(new UMSTimeDateLog().getTimeDateLog() + " Response " + rqType + " read from TechOne READ end"
					+ response);

			ObjectMapper mapper = new ObjectMapper();
			Response resobjest = mapper.readValue(response.toString(), Response.class);

			if (rqType.equalsIgnoreCase(Constant.HEADER_WORKORDER)) {
				workOrderRequest.setWorkOrderNumber(resobjest.getWorkOrderNumber());
			} else if (rqType.equalsIgnoreCase(Constant.SUBORDER_WORKORDER)) {
				workOrderRequest.setSubworkOrderNumber(resobjest.getWorkOrderNumber());
			}

			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

			if (errorMessageList.size() > 0) {
				logger.info(new UMSTimeDateLog().getTimeDateLog() + " Errors Found in " + rqType + " Read Response"
						+ errorMessageList.toString());
				new DbUtil().insertInMessageResponseTable(esbId, resobjest.getWorkOrderNumber(), "FAIL", response,
						"Read", rqType, muleContext);
				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				new DbUtil().insertInMessageResponseTable(esbId, resobjest.getWorkOrderNumber(), "SUCCESS", response,
						"Read", rqType, muleContext);
				logger.info(
						new UMSTimeDateLog().getTimeDateLog() + " No Errors Found in " + rqType + " Read Response ");
			}
		} catch (Exception e) {
			throw e;
		}

		return jsonObject;
	}

	/**
	 * @param value
	 * @return Object
	 */
	private Object isNull(Object value) {
		if (value == null)
			return "NA";
		return value;
	}

	/**
	 * @param value
	 * @return Object
	 */
	private Object isEmpty(Object value) {
		if (value == null)
			return "";
		return value;
	}

	/**
	 * @param value
	 * @return Object
	 */
	private Object isNullDate(Object value) {
		/* if found null return "" */
		if (value == null)
			return "";
		return value;
	}

	/**
	 * @param value
	 * @return Object
	 */
	private Object isNullTime(Object value) {
		/* if found null return 0 */
		if (value == null)
			return 0;
		return value;
	}

	public static <T> Optional<T> resolve(Supplier<T> resolver) {
		try {
			T result = resolver.get();
			return Optional.ofNullable(result);
		} catch (NullPointerException e) {
			return Optional.empty();
		}
	}

	/**
	 * @param workOrderRequest
	 * @param operation
	 * @param rqType
	 * @return String
	 * @throws Exception
	 */
	public String saveForDetailWithCF(WorkOrder workOrderRequest, String detail, Operation operation, String rqType)
			throws Exception {
		String response = null;
		try {
			String orderId = rqType == Constant.HEADER_WORKORDER ? workOrderRequest.getOrderId()
					: operation.getOperationId();
			JSONObject jsonObject = workOrderRequest.getReadForDetailJson();
			String estimatedStartDateTime = (String) jsonObject.get("EstimatedStartDateTime");
			if (estimatedStartDateTime == "" || estimatedStartDateTime == null) {
				SimpleDateFormat st = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				st.setTimeZone(TimeZone.getTimeZone("GMT"));
				estimatedStartDateTime = new String(st.format(new Date())).toString();
			}
			String estimatedStartDate = (String) jsonObject.get("EstimatedStartDate");
			if (estimatedStartDate == "" || estimatedStartDate == null) {
				SimpleDateFormat st = new SimpleDateFormat("yyyy-MM-dd");
				st.setTimeZone(TimeZone.getTimeZone("GMT"));
				estimatedStartDate = new String(st.format(new Date())).toString();
			}
			String input = "{\"AvailableForBudgeting\":false," + "\"WorkSystemName\":\""
					+ workOrderRequest.getWorkSystemName() + "\"," + "\"WorkOrderNumber\":\""
					+ jsonObject.get("WorkOrderNumber") + "\"," + "\"KeyedWorkOrderNumber\":\""
					+ jsonObject.get("KeyedWorkOrderNumber") + "\"," + "\"IsTemplate\":" + jsonObject.get("IsTemplate")
					+ "," + "\"Vers\":" + jsonObject.get("Vers") + "," + "\"Description\":\""
					+ workOrderRequest.getWorkDescription() + "\"," + "\"Details\":\"" + detail + "\"," + "\"PriorityCode\":\""
					+ new TechOneCommon().mapPriority(workOrderRequest.getPriority()) + "\"," + "\"WorkTypeCode\":\""
					+ jsonObject.get("WorkTypeCode") + "\"," + "\"Type\":\"" + jsonObject.get("Type") + "\","
					+ "\"Source\":\"" + jsonObject.get("Source") + "\"," + "\"Status\":\"" + jsonObject.get("Status")
					+ "\"," + "\"Stage\":\"" + jsonObject.get("Stage") + "\"," + "\"ApprovalStatus\":\""
					+ jsonObject.get("ApprovalStatus") + "\"," + "\"ResponsiblePerson\":\""
					+ jsonObject.get("ResponsiblePerson") + "\"," + "\"ProjectCode\":\"" + jsonObject.get("ProjectCode")
					+ "\"," + "\"ActivityCode\":\"" + jsonObject.get("ActivityCode") + "\","
					+ "\"HeaderWorkOrderNumberInternal\":\"" + jsonObject.get("HeaderWorkOrderNumberInternal") + "\","
					+ "\"HeaderWorkOrderNumber\":\"" + jsonObject.get("HeaderWorkOrderNumber") + "\","
					+ "\"IsBudgetWorkOrder\":" + jsonObject.get("IsBudgetWorkOrder") + "," + "\"LevelType\":\""
					+ jsonObject.get("LevelType") + "\"," + "\"RequiredByDate\":\""
					+ isEmpty(jsonObject.get("RequiredByDate")) + "\"," + "\"RequiredByTime\":\""
					+ isEmpty(jsonObject.get("RequiredByTime")) + "\"," + "\"RequiredByDateTime\":\""
					+ isEmpty(jsonObject.get("RequiredByDateTime")) + "\"," + "\"EstimatedStartDate\":\""
					+ estimatedStartDate + "\"," + "\"EstimatedStartTime\":\""
					+ isEmpty(jsonObject.get("EstimatedStartTime")) + "\"," + "\"EstimatedStartDateTime\":\""
					+ estimatedStartDateTime + "\"," + "\"EstimatedFinishDate\":\""
					+ isEmpty(workOrderRequest.getFinishDate()) + "\"," + "\"EstimatedFinishTime\":\""
					+ isEmpty(jsonObject.get("EstimatedFinishTime")) + "\"," + "\"EstimatedFinishDateTime\":\""
					+ isEmpty(jsonObject.get("EstimatedFinishDateTime")) + "\"," + "\"ActualStartDate\":\""
					+ isEmpty(jsonObject.get("ActualStartDate")) + "\"," + "\"ActualStartTime\":\""
					+ isEmpty(jsonObject.get("ActualStartTime")) + "\"," + "\"ActualStartDateTime\":\""
					+ isEmpty(jsonObject.get("ActualStartDateTime")) + "\"," + "\"ActualFinishDate\":\""
					+ isEmpty(jsonObject.get("ActualFinishDate")) + "\"," + "\"ActualFinishTime\":\""
					+ isEmpty(jsonObject.get("ActualFinishTime")) + "\"," + "\"ActualFinishDateTime\":\""
					+ isEmpty(jsonObject.get("ActualFinishDateTime")) + "\"," + "\"PercentComplete\":0,"
					+ "\"ExpectedDuration\":0," + "\"BillingMethod\":\"" + jsonObject.get("BillingMethod") + "\","
					+ "\"BillingType\":\"" + jsonObject.get("BillingType") + "\"," + "\"BillingLedgerCode\":\""
					+ jsonObject.get("BillingLedgerCode") + "\"," + "\"Comment\":\"" + jsonObject.get("Comment") + "\","
					+ "\"BillingAccountNumberInternal\":\"" + jsonObject.get("BillingAccountNumberInternal") + "\","
					+ "\"WorkOrderUserField1\":0," + "\"WorkOrderUserField2\":\"NA\","
					+ "\"WorkOrderUserField3\":\"NA\"," + "\"WorkOrderUserField4\":\"NA\","
					+ "\"WorkOrderUserField5\":\"NA\"," + "\"WorkOrderUserField6\":\"NA\","
					+ "\"WorkOrderUserField7\":\"NA\"," + "\"WorkOrderUserField8\":\"" + orderId + "\","
					+ "\"WorkOrderUserField9\":\"" + workOrderRequest.getAddress().trim() + "\","
					+ "\"WorkOrderUserField10\":\"" + isNull(workOrderRequest.getEquipmentId()) + "\","
					+ "\"WorkOrderUserField11\":\"" + isNull(workOrderRequest.getLocationId()) + "\","
					+ "\"WorkOrderUserField12\":\"NA\"," + "\"WorkOrderUserField13\":\""
					+ isNull(workOrderRequest.getStatus()) + "\"," + "\"WorkOrderUserField14\":\""
					+ isNull(workOrderRequest.getPlannerGroup()) + "\"," + "\"WorkOrderUserField15\":\""
					+ isNull(workOrderRequest.getParentOrder()) + "\"," + "\"WorkOrderUserField16\":\""
					+ isNull(workOrderRequest.getDuplicateOf()) + "\"," + "\"WorkOrderUserField17\":\"NA\","
					+ "\"WorkOrderUserField18\":\"" + isNullDate(workOrderRequest.getReleaseDate()) + "\","

					+ "\"WorkOrderUserfield18_Time\":" + isNullTime(jsonObject.get("WorkOrderUserfield18_Time")) + ","
					+ "\"WorkTypeUserfield13_Time\":" + isNullTime(jsonObject.get("WorkTypeUserfield13_Time")) + ","
					+ "\"WorkTypeUserfield14_Time\":" + isNullTime(jsonObject.get("WorkTypeUserfield14_Time")) + ",";

			if (rqType.equalsIgnoreCase(Constant.HEADER_WORKORDER)) {
				input += "\"WorkOrderUserField19\":\"" + isNull(jsonObject.get("WorkOrderUserField19")) + "\","
						+ "\"WorkOrderUserField20\":\"" + isNull(jsonObject.get("WorkOrderUserField20")) + "\","
						+ "\"WorkTypeUserfield1\":\"" + "NA"

						+ "\"," + "\"WorkTypeUserfield2\":\"" + "NA"

						+ "\"," + "\"WorkTypeUserfield3\":\"NA\"," + "\"WorkTypeUserfield4\":\"NA\","
						+ "\"WorkTypeUserfield5\":\"" + "NA"

						+ "\",";
			} else if (rqType.equalsIgnoreCase(Constant.SUBORDER_WORKORDER)) {
				input += "\"WorkOrderUserField19\":\"NA\"," + "\"WorkOrderUserField20\":\"NA\","
						+ "\"WorkTypeUserfield1\":\"NA\","
						/*
						 * Commented for Hot Fix needs to put the Summary object
						 * null check
						 * isNull(workOrderRequest.getSummary().getFault() ==
						 * null ? "NA" :
						 * workOrderRequest.getSummary().getFault().getCode())
						 */
						+ "\"WorkTypeUserfield2\":\"NA\","
						/*
						 * Commented for Hot Fix needs to put the Summary object
						 * null check +
						 * isNull(workOrderRequest.getSummary().getProblem() ==
						 * null ? "NA" - :
						 * workOrderRequest.getSummary().getProblem().getCode())
						 */
						+ "\"WorkTypeUserfield3\":\"NA\"," + "\"WorkTypeUserfield4\":\"NA\","
						+ "\"WorkTypeUserfield5\":\"NA\",";
				/*
				 * Commented for Hot Fix needs to put the Summary object null
				 * check isNull(workOrderRequest.getSummary().getCause() == null
				 * ? "NA" - :
				 * workOrderRequest.getSummary().getCause().getCode())
				 */
			}

			input += "\"WorkTypeUserfield6\":\"" + isNull(workOrderRequest.getPurchaseOrderId()) + "\","
					+ "\"WorkTypeUserfield7\":\"" + isNull(workOrderRequest.getPurchaseOrderItemId()) + "\","
					+ "\"WorkTypeUserfield8\":\"" + isNull(workOrderRequest.getLoggedXCoordinate()) + "\","
					+ "\"WorkTypeUserfield9\":\"" + isNull(workOrderRequest.getLoggedYCoordinate()) + "\","
					+ "\"WorkTypeUserfield10\":\"" + isNull(workOrderRequest.getActualXCoordinate()) + "\","
					+ "\"WorkTypeUserfield11\":\"" + isNull(workOrderRequest.getActualYCoordinate()) + "\","
					+ "\"WorkTypeUserfield12\":\"NA\"," + "\"WorkTypeUserfield13\":\""
					+ isNullDate(workOrderRequest.getRequiredStart()) + "\"," + "\"WorkTypeUserfield14\":\""
					+ isNullDate(workOrderRequest.getRequiredFinish()) + "\",";

			if (rqType.equalsIgnoreCase(Constant.HEADER_WORKORDER)) {

				input += "\"WorkTypeUserfield15\":\"NA\"," +

						"\"WorkTypeUserfield16\":\"NA\"," + "\"WorkTypeUserfield17\":\""
						+ "NA"/*
								 * Commented for Hot Fix needs to put the
								 * Summary object null check +
								 * isNull(workOrderRequest.getSummary().getFault
								 * () == null ? "NA" - :
								 * workOrderRequest.getSummary().getFault().
								 * getCodeGroup())
								 */

						+ "\"," + "\"WorkTypeUserfield18\":\""
						+ "NA" /*
								 * Commented for Hot Fix needs to put the
								 * Summary object null check
								 * isNull(workOrderRequest.getSummary().getFault
								 * () == null ? "NA" :
								 * workOrderRequest.getSummary().getFault().
								 * getCode())
								 */

						+ "\",";

			} else if (rqType.equalsIgnoreCase(Constant.SUBORDER_WORKORDER)) {
				input += "\"WorkTypeUserfield15\":\"NA\"," + "\"WorkTypeUserfield16\":\"NA\","
						+ "\"WorkTypeUserfield17\":\"NA\"," + "\"WorkTypeUserfield18\":\"NA\",";

			}

			input += "\"WorkTypeUserfield19\":\"NA\"," + "\"WorkTypeUserfield20\":\"NA\","
					+ "\"WorkOrderSelectionType1\":\"" + isNull(jsonObject.get("WorkOrderSelectionType1")) + "\","
					+ "\"WorkOrderSelectionType2\":\"" + isNull(jsonObject.get("WorkOrderSelectionType2")) + "\","
					+ "\"WorkOrderSelectionType3\":\"" + isNull(jsonObject.get("WorkOrderSelectionType3")) + "\","
					+ "\"WorkOrderSelectionType4\":\"" + isNull(jsonObject.get("WorkOrderSelectionType4")) + "\","
					+ "\"WorkOrderSelectionType5\":\"" + isNull(jsonObject.get("WorkOrderSelectionType5")) + "\","
					+ "\"WorkOrderSelectionType6\":\"" + isNull(jsonObject.get("WorkOrderSelectionType6")) + "\","
					+ "\"WorkOrderSelectionType7\":\"" + isNull(jsonObject.get("WorkOrderSelectionType7")) + "\","
					+ "\"WorkOrderSelectionType8\":\"" + isNull(jsonObject.get("WorkOrderSelectionType8")) + "\","
					+ "\"WorkOrderSelectionType9\":\"" + isNull(jsonObject.get("WorkOrderSelectionType9")) + "\","
					+ "\"WorkOrderSelectionType10\":\"" + isNull(jsonObject.get("WorkOrderSelectionType10")) + "\","
					+ "\"WorkOrderSelectionType11\":\"" + isNull(jsonObject.get("WorkOrderSelectionType11")) + "\","
					+ "\"WorkOrderSelectionType12\":\"" + isNull(jsonObject.get("WorkOrderSelectionType12")) + "\","
					+ "\"WorkOrderSelectionType13\":\"" + isNull(jsonObject.get("WorkOrderSelectionType13")) + "\","
					+ "\"WorkOrderSelectionType14\":\"" + isNull(jsonObject.get("WorkOrderSelectionType14")) + "\","
					+ "\"WorkOrderSelectionType15\":\"" + isNull(jsonObject.get("WorkOrderSelectionType15")) + "\","
					+ "\"WorkOrderSelectionType16\":\"" + isNull(jsonObject.get("WorkOrderSelectionType16")) + "\","
					+ "\"WorkOrderSelectionType17\":\"" + isNull(jsonObject.get("WorkOrderSelectionType17")) + "\","
					+ "\"WorkOrderSelectionType18\":\"" + isNull(jsonObject.get("WorkOrderSelectionType18")) + "\","
					+ "\"WorkOrderSelectionType19\":\"" + isNull(jsonObject.get("WorkOrderSelectionType19")) + "\","
					+ "\"WorkOrderSelectionType20\":\"" + isNull(jsonObject.get("WorkOrderSelectionType20")) + "\"}";

			Client client = Client.create();
			WebResource webResource = client.resource(System.getProperty("techonesavefordetailurl"));

			new DbUtil().insertIntoMessageTable(esbId, "SAP", rqType, "Save", input, workOrderRequest.getOrderId(),
					"Valid", rqType == Constant.HEADER_WORKORDER ? "" : operation.getOperationId(), muleContext);

			ClientResponse responseClient = webResource.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, input);
			response = responseClient.getEntity(String.class);
			logger.info(new UMSTimeDateLog().getTimeDateLog() + " Input to " + rqType
					+ " save Techone for Saving the Details via UMSESB\t" + input);
			logger.info("Response " + rqType + " save from TechOne end" + response);

			ObjectMapper mapper = new ObjectMapper();
			Response resobjest = mapper.readValue(response.toString(), Response.class);

			if (rqType.equalsIgnoreCase(Constant.HEADER_WORKORDER)) {
				workOrderRequest.setWorkOrderNumber(resobjest.getKeyedWorkOrderNumber());
			} else if (rqType.equalsIgnoreCase(Constant.SUBORDER_WORKORDER)) {
				workOrderRequest.setSubworkOrderNumber(resobjest.getKeyedWorkOrderNumber());
			}

			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

			if (errorMessageList.size() > 0) {
				logger.info(new UMSTimeDateLog().getTimeDateLog() + " Errors Found in " + rqType + " Save Response"
						+ errorMessageList.toString());
				new DbUtil().insertInMessageResponseTable(esbId, resobjest.getKeyedWorkOrderNumber(), "FAIL", response,
						"Save", rqType, muleContext);
				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				logger.info(new UMSTimeDateLog().getTimeDateLog() + " No Errors Found in " + rqType + " Save Response");
				new DbUtil().insertInMessageResponseTable(esbId, resobjest.getKeyedWorkOrderNumber(), "SUCCESS",
						response, "Save", rqType, muleContext);
			}

		} catch (Exception e) {
			logger.error(new UMSTimeDateLog().getTimeDateLog() + " error while save operation" + e.getMessage());

			throw e;
		}
		return response;
	}

	/**
	 * @param workOrderRequest
	 * @param operation
	 * @param rqType
	 * @return String
	 * @throws Exception
	 */
	public String approveWorkOrderWithCF(WorkOrder workOrderRequest, Operation operation, String rqType)
			throws Exception {
		String response = null;
		Response resobjest;

		try {
			String urlOrderNo = rqType == Constant.HEADER_WORKORDER ? workOrderRequest.getWorkOrderNumber()
					: workOrderRequest.getSubworkOrderNumber();
			Client client = Client.create();
			WebResource webResource = client.resource(System.getProperty("techoneapproveworkorderurl"));
			String input = "{\"WorkSystemName\":\"" + workOrderRequest.getWorkSystemName() + "\","
					+ "\"KeyedWorkOrderNumber\":\"" + urlOrderNo + "\"}";

			new DbUtil().insertIntoMessageTable(esbId, "SAP", rqType, "Approve", input, workOrderRequest.getOrderId(),
					"Valid", rqType == Constant.HEADER_WORKORDER ? "" : operation.getOperationId(), muleContext);

			ClientResponse responseClient = webResource.type("application/json").accept("application/json")
					.header("Authorization", System.getProperty("techoneauthorization"))
					.post(ClientResponse.class, input);
			response = responseClient.getEntity(String.class);
			logger.info(new UMSTimeDateLog().getTimeDateLog() + " Input to " + rqType
					+ " Approve Techone for the WorkOrder via UMSESB \t" + input);
			logger.info(new UMSTimeDateLog().getTimeDateLog() + " Response " + rqType
					+ " Approve from TechOne end for Workorder \t" + response);

			ObjectMapper mapper = new ObjectMapper();
			resobjest = mapper.readValue(response.toString(), Response.class);

			ErrorMessages errorMessage = new ErrorMessages();
			ArrayList<ErrorMessages> errorMessageList = errorMessage.filterErrorMessages(resobjest);

			if (errorMessageList.size() > 0) {
				logger.info(new UMSTimeDateLog().getTimeDateLog() + " Errors Found in " + rqType + " Approve Response"
						+ errorMessageList.toString());
				new DbUtil().insertInMessageResponseTable(esbId, resobjest.getKeyedWorkOrderNumber(), "FAIL", response,
						"Approve", rqType, muleContext);
				throw new UnprocessedMessageFault_Exception(errorMessageList.toString());
			} else {
				logger.info(
						new UMSTimeDateLog().getTimeDateLog() + " No Errors Found in " + rqType + " Approve Response");
				new DbUtil().insertInMessageResponseTable(esbId, resobjest.getKeyedWorkOrderNumber(), "SUCCESS",
						response, "Approve", rqType, muleContext);
			}
		} catch (Exception e) {

			throw e;
		}
		return resobjest.getWorkOrderNumber();
	}

	/**
	 * @param rqType
	 * @param workOrder
	 * @return String
	 */
	private String addAdditionalProperties(String rqType, String workOrder) {
		if (rqType.equalsIgnoreCase("SUBORDER_WORKORDER")) {

			return "\"," + "\"HeaderWorkOrderNumber\":\"" + workOrder + "\"," + "\"HeaderWorkOrderNumberInternal\":\""
					+ workOrder;
		} else {
			return "";
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.mule.api.lifecycle.Callable#onCall(org.mule.api.MuleEventContext)
	 */
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		return eventContext.getMessage().getPayload();
	}

}