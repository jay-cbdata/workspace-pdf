
package com.service.workorder;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.service.workorder package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InvalidMessageFault_QNAME = new QName("urn:aucklandcouncil.govt.nz:service:supplier:operations:componentmodel:order", "invalidMessageFault");
    private final static QName _UnprocessedMessageFault_QNAME = new QName("urn:aucklandcouncil.govt.nz:service:supplier:operations:componentmodel:order", "unprocessedMessageFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.service.workorder
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Operation }
     * 
     */
    public Operation createOperation() {
        return new Operation();
    }

    /**
     * Create an instance of {@link Operation.Service }
     * 
     */
    public Operation.Service createOperationService() {
        return new Operation.Service();
    }

    /**
     * Create an instance of {@link CreateWorkOrderResponse }
     * 
     */
    public CreateWorkOrderResponse createCreateWorkOrderResponse() {
        return new CreateWorkOrderResponse();
    }

    /**
     * Create an instance of {@link WorkOrderResponse }
     * 
     */
    public WorkOrderResponse createWorkOrderResponse() {
        return new WorkOrderResponse();
    }

    /**
     * Create an instance of {@link InvalidMessageFault }
     * 
     */
    public InvalidMessageFault createInvalidMessageFault() {
        return new InvalidMessageFault();
    }

    /**
     * Create an instance of {@link CreateWorkOrder }
     * 
     */
    public CreateWorkOrder createCreateWorkOrder() {
        return new CreateWorkOrder();
    }

    /**
     * Create an instance of {@link WorkOrder }
     * 
     */
    public WorkOrder createWorkOrder() {
        return new WorkOrder();
    }

    /**
     * Create an instance of {@link UpdateWorkOrder }
     * 
     */
    public UpdateWorkOrder createUpdateWorkOrder() {
        return new UpdateWorkOrder();
    }

    /**
     * Create an instance of {@link UpdateWorkOrderResponse }
     * 
     */
    public UpdateWorkOrderResponse createUpdateWorkOrderResponse() {
        return new UpdateWorkOrderResponse();
    }

    /**
     * Create an instance of {@link UnprocessedMessageFault }
     * 
     */
    public UnprocessedMessageFault createUnprocessedMessageFault() {
        return new UnprocessedMessageFault();
    }

    /**
     * Create an instance of {@link Order }
     * 
     */
    public Order createOrder() {
        return new Order();
    }

    /**
     * Create an instance of {@link Summary }
     * 
     */
    public Summary createSummary() {
        return new Summary();
    }

    /**
     * Create an instance of {@link Activity }
     * 
     */
    public Activity createActivity() {
        return new Activity();
    }

    /**
     * Create an instance of {@link Code }
     * 
     */
    public Code createCode() {
        return new Code();
    }

    /**
     * Create an instance of {@link Operation.Service.EstimatedCostDetail }
     * 
     */
    public Operation.Service.EstimatedCostDetail createOperationServiceEstimatedCostDetail() {
        return new Operation.Service.EstimatedCostDetail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidMessageFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:aucklandcouncil.govt.nz:service:supplier:operations:componentmodel:order", name = "invalidMessageFault")
    public JAXBElement<InvalidMessageFault> createInvalidMessageFault(InvalidMessageFault value) {
        return new JAXBElement<InvalidMessageFault>(_InvalidMessageFault_QNAME, InvalidMessageFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnprocessedMessageFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:aucklandcouncil.govt.nz:service:supplier:operations:componentmodel:order", name = "unprocessedMessageFault")
    public JAXBElement<UnprocessedMessageFault> createUnprocessedMessageFault(UnprocessedMessageFault value) {
        return new JAXBElement<UnprocessedMessageFault>(_UnprocessedMessageFault_QNAME, UnprocessedMessageFault.class, null, value);
    }

}
