package com.service.workorder.validation;

import java.io.File;
import java.io.StringReader;
import java.net.URL;

import javax.sql.DataSource;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.mule.api.MuleContext;
import org.mule.api.MuleMessage;
import org.mule.api.annotations.expressions.Lookup;
import org.mule.api.context.MuleContextAware;
import org.mule.api.routing.filter.Filter;
import org.mule.module.db.internal.resolver.database.DbConfigResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;


public class SchemaValidator implements Filter, MuleContextAware {
	public final static Logger logger = LoggerFactory.getLogger(SchemaValidator.class.getName());
	@Lookup
	private MuleContext muleContext;
	private String esbId = org.mule.util.UUID.getUUID().toString();
	
	public void setMuleContext(MuleContext muleContext) {
		this.muleContext = muleContext;
	}
    private String schemaLocation;

	public String getSchemaLocation() {
		return schemaLocation;
	}


	public void setSchemaLocation(String schemaLocation) {
		this.schemaLocation = schemaLocation;
	}

	/**
     * Accepts the message if schema validation passes.
     * 
     * @param message The message.
     * @return Whether the message passes schema validation.
     */
    @Override
    public boolean accept(MuleMessage message)
    {
    	String payload = null; 
    	try{
    		payload = message.getPayloadAsString();
    		
    		String uripath= message.getInboundProperty("http.request.uri");
    		logger.info("Filter- Request received for xsd validation. SchemaLocation " + getSchemaLocation() + " uripath "+uripath +" payload "+ payload);
    		
    		
    		if(uripath.toString().toLowerCase().endsWith("?wsdl") ){
    			return true;
    		}
    		
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI); 
			
			logger.info("path" +this.getClass().getClassLoader().getResource(getSchemaLocation()).getPath());
			//URL url = getClass().getResource(getSchemaLocation());

			Schema schema = sf.newSchema(new File(this.getClass().getClassLoader().getResource(getSchemaLocation()).getPath())); 
			
			
			Validator validator = schema.newValidator();
			Source xmlFile = new StreamSource(new StringReader(payload));
			validator.validate(xmlFile);
			logger.debug("validation successful. returning true");
			 return true;
		
		} catch (Exception e) {
			e.printStackTrace();
			//return false;
			insertIntoMessageTable(esbId, "SOAP", "WorkOrder", "Create", payload , null ,"InValid");

			throw new InvalidSchemaException(e.getMessage());
			
		}
       
    }
	void insertIntoMessageTable(String id, String SourceSys, String MessageType, String ActionType, String Payload, String sapId, String valid) {
		try {
			DbConfigResolver dbConfigResolver = muleContext.getRegistry().get("MySQL_Configuration");
			DataSource dataSource = dbConfigResolver.resolve(null).getDataSource();
			JdbcTemplate jdbcTemplateResponseTable = new JdbcTemplate(dataSource);

			String sqlInsertion = "INSERT INTO message(esbid,SourceSys,MessageType,ActionType,Payload,sapId,valid) VALUES ("
					+ "'" + id + "'," + "'" + SourceSys + "'," + "'" + MessageType + "'," + "'" + ActionType + "',"
					+ "'" + Payload.replace("'","''") + "'," + "'" + sapId + "'" +"," + "'" + valid + "'" + ")";
			logger.info("sqlInsertion \t" + sqlInsertion);
			jdbcTemplateResponseTable.execute(sqlInsertion);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
