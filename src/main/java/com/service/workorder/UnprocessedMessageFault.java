
package com.service.workorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * A fault representing a failure of a system to handle a message, even though that message may have been successfully received.  It is expected that the caller will retry the message.  An example is where a message was received but a critical back-end system was unavailable.
 * 
 * <p>Java class for UnprocessedMessageFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnprocessedMessageFault">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}AbstractRemoteFault">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnprocessedMessageFault")
public class UnprocessedMessageFault
    extends AbstractRemoteFault
{


}
