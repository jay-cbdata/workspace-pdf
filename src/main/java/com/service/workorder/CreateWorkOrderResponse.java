
package com.service.workorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workOrderResponse" type="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}WorkOrderResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "workOrderResponse"
})
@XmlRootElement(name = "createWorkOrderResponse", namespace = "urn:aucklandcouncil.govt.nz:service:supplier:operations:componentmodel:order")
public class CreateWorkOrderResponse {

    @XmlElement(required = true)
    protected WorkOrderResponse workOrderResponse;

    /**
     * Gets the value of the workOrderResponse property.
     * 
     * @return
     *     possible object is
     *     {@link WorkOrderResponse }
     *     
     */
    public WorkOrderResponse getWorkOrderResponse() {
        return workOrderResponse;
    }

    /**
     * Sets the value of the workOrderResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkOrderResponse }
     *     
     */
    public void setWorkOrderResponse(WorkOrderResponse value) {
        this.workOrderResponse = value;
    }

}
