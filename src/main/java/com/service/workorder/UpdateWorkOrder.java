
package com.service.workorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workOrderRequest" type="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}WorkOrder"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "workOrderRequest"
})
@XmlRootElement(name = "updateWorkOrder", namespace = "urn:aucklandcouncil.govt.nz:service:supplier:operations:componentmodel:order")
public class UpdateWorkOrder {

    @XmlElement(required = true)
    protected WorkOrder workOrderRequest;

    /**
     * Gets the value of the workOrderRequest property.
     * 
     * @return
     *     possible object is
     *     {@link WorkOrder }
     *     
     */
    public WorkOrder getWorkOrderRequest() {
        return workOrderRequest;
    }

    /**
     * Sets the value of the workOrderRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkOrder }
     *     
     */
    public void setWorkOrderRequest(WorkOrder value) {
        this.workOrderRequest = value;
    }

}
