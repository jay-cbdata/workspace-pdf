
package com.service.workorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * A fault indicating that the recipient could not process the message due to problems with the message format or content.  This represents a fatal failure in processing and the message should not be resubmitted by the caller.
 * 
 * <p>Java class for InvalidMessageFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvalidMessageFault">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}AbstractRemoteFault">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvalidMessageFault")
public class InvalidMessageFault
    extends AbstractRemoteFault
{


}
