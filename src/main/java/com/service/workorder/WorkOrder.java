
package com.service.workorder;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.json.simple.JSONObject;

/**
 * <p>Java class for WorkOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WorkOrder">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}Order">
 *       &lt;sequence>
 *         &lt;element name="operation" type="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}Operation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="activity" type="{urn:aucklandcouncil.govt.nz:service:supplier:operations:datamodel:order}Activity" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkOrder", propOrder = {
    "operation",
    "activity"
})
public class WorkOrder
    extends Order
{

    protected List<Operation> operation;
    protected List<Activity> activity;
    
    protected transient String workSystemName;
    protected transient String workOrderNumber;
    protected transient String subworkOrderNumber;
    protected transient JSONObject readForDetailJson;
    
    /**
     * Gets the value of the operation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the operation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOperation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Operation }
     * 
     * 
     */
    public List<Operation> getOperation() {
        if (operation == null) {
            operation = new ArrayList<Operation>();
        }
        return this.operation;
    }

    /**
     * Gets the value of the activity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Activity }
     * 
     * 
     */
    public List<Activity> getActivity() {
        if (activity == null) {
            activity = new ArrayList<Activity>();
        }
        return this.activity;
    }

    /**
     * Sets the value of the operation property.
     * 
     * @param operation
     *     allowed object is
     *     {@link Operation }
     *     
     */
    public void setOperation(List<Operation> operation) {
        this.operation = operation;
    }

    /**
     * Sets the value of the activity property.
     * 
     * @param activity
     *     allowed object is
     *     {@link Activity }
     *     
     */
    public void setActivity(List<Activity> activity) {
        this.activity = activity;
    }
    @XmlTransient
	public String getWorkSystemName() {
		return workSystemName;
	}

	public void setWorkSystemName(String workSystemName) {
		this.workSystemName = workSystemName;
	}
	@XmlTransient
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}

	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	@XmlTransient
	public JSONObject getReadForDetailJson() {
		return readForDetailJson;
	}

	public void setReadForDetailJson(JSONObject readForDetailJson) {
		this.readForDetailJson = readForDetailJson;
	}

	@XmlTransient
	public String getSubworkOrderNumber() {
		return subworkOrderNumber;
	}

	public void setSubworkOrderNumber(String subworkOrderNumber) {
		this.subworkOrderNumber = subworkOrderNumber;
	}

	

}
